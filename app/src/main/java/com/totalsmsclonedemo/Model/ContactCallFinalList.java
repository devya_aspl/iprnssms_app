package com.totalsmsclonedemo.Model;

import com.totalsmsclonedemo.dbo.CallAndSmsListDataDbo;
import com.totalsmsclonedemo.dbo.CallListDataDbo;

import java.util.ArrayList;

public class ContactCallFinalList {

    String country_name;
    String name;
    ArrayList<CallListDataDbo> childList;
    ArrayList<ContactCallFinalList> tier_list;
    boolean selected_tier;

    public ContactCallFinalList(String name, ArrayList<CallListDataDbo> childList, boolean selected_tier) {
        this.name = name;
        this.childList = childList;
        this.selected_tier = selected_tier;
    }

    public ContactCallFinalList(ArrayList<ContactCallFinalList> tier_list, String country_name) {
        this.tier_list = tier_list;
        this.country_name = country_name;
    }

    public boolean isSelected_tier() {
        return selected_tier;
    }

    public void setSelected_tier(boolean selected_tier) {
        this.selected_tier = selected_tier;
    }

    public ArrayList<ContactCallFinalList> getTier_list() {
        return tier_list;
    }

    public void setTier_list(ArrayList<ContactCallFinalList> tier_list) {
        this.tier_list = tier_list;
    }

    public String getCountry_name() {
        return country_name;
    }

    public void setCountry_name(String country_name) {
        this.country_name = country_name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<CallListDataDbo> getChildList() {
        return childList;
    }

    public void setChildList(ArrayList<CallListDataDbo> childList) {
        this.childList = childList;
    }
}
