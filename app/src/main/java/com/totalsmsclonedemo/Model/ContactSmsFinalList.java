package com.totalsmsclonedemo.Model;

import com.totalsmsclonedemo.dbo.CallListDataDbo;
import com.totalsmsclonedemo.dbo.SmsListDataDbo;

import java.util.ArrayList;

public class ContactSmsFinalList {

    String country_name;
   String name;
    ArrayList<SmsListDataDbo> childList;
    ArrayList<ContactSmsFinalList> tier_list;
    boolean selected_tier;

    public ContactSmsFinalList(String name, ArrayList<SmsListDataDbo> childList, boolean selected_tier) {
        this.name = name;
        this.childList = childList;
        this.selected_tier = selected_tier;
    }

    public ContactSmsFinalList(String name, ArrayList<SmsListDataDbo> childList) {
        this.name = name;
        this.childList = childList;
    }

    public ContactSmsFinalList(ArrayList<ContactSmsFinalList> tier_list, String country_name) {
        this.tier_list = tier_list;
        this.country_name = country_name;
    }

    public String getCountry_name() {
        return country_name;
    }

    public void setCountry_name(String country_name) {
        this.country_name = country_name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<SmsListDataDbo> getChildList() {
        return childList;
    }

    public void setChildList(ArrayList<SmsListDataDbo> childList) {
        this.childList = childList;
    }

    public boolean isSelected_tier() {
        return selected_tier;
    }

    public void setSelected_tier(boolean selected_tier) {
        this.selected_tier = selected_tier;
    }

    public ArrayList<ContactSmsFinalList> getTier_list() {
        return tier_list;
    }

    public void setTier_list(ArrayList<ContactSmsFinalList> tier_list) {
        this.tier_list = tier_list;
    }
}
