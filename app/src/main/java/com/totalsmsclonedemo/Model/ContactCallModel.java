package com.totalsmsclonedemo.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class ContactCallModel implements Parcelable {

    public static final Creator CREATOR = new Creator() {
        public ContactCallModel createFromParcel(Parcel in) {
            return new ContactCallModel(in);
        }

        public ContactCallModel[] newArray(int size) {
            return new ContactCallModel[size];
        }
    };

    int id,image;
    String contactNumber,contactCountry,contactTier;

    public ContactCallModel(int id, String contactNumber, String contactCountry) {
        this.id = id;
        this.contactNumber = contactNumber;
        this.contactCountry = contactCountry;
    }

    public ContactCallModel(int id, int image, String contactNumber, String contactCountry, String contactTier) {
        this.id = id;
        this.image = image;
        this.contactNumber = contactNumber;
        this.contactCountry = contactCountry;
        this.contactTier = contactTier;
    }

    public ContactCallModel(int id, String contactNumber, String contactCountry, String contactTier) {
        this.id = id;
        this.contactNumber = contactNumber;
        this.contactCountry = contactCountry;
        this.contactTier = contactTier;
    }

    public String getContactCountry() {
        return contactCountry;
    }

    public void setContactCountry(String contactCountry) {
        this.contactCountry = contactCountry;
    }

    public ContactCallModel(int id, String contactNumber) {
        this.id = id;
        this.contactNumber = contactNumber;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getContactTier() {
        return contactTier;
    }

    public void setContactTier(String contactTier) {
        this.contactTier = contactTier;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    //    @Override
//    public int describeContents() {
//        return 0;
//    }
//
//    @Override
//    public void writeToParcel(Parcel parcel, int i) {
//
//    }

    // Parcelling part
    public ContactCallModel(Parcel in){
        this.id = in.readInt();
        this.contactCountry = in.readString();
        this.contactNumber =  in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeString(this.contactCountry);
        dest.writeString(this.contactNumber);
    }

    @Override
    public String toString() {
        return "ContactCallModel{" +
                "id='" + id + '\'' +
                ", contactCountry='" + contactCountry + '\'' +
                ", contactNumber='" + contactNumber + '\'' +
                '}';
    }
}
