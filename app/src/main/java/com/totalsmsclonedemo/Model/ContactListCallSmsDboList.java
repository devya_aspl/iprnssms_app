package com.totalsmsclonedemo.Model;

import com.totalsmsclonedemo.dbo.CallAndSmsListDataDbo;

import java.util.ArrayList;

public class ContactListCallSmsDboList {

   String name;
   ArrayList<CallAndSmsListDataDbo> childList;

    public ContactListCallSmsDboList(String name, ArrayList<CallAndSmsListDataDbo> childList) {
        this.name = name;
        this.childList = childList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<CallAndSmsListDataDbo> getChildList() {
        return childList;
    }

    public void setChildList(ArrayList<CallAndSmsListDataDbo> childList) {
        this.childList = childList;
    }
}
