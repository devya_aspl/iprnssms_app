package com.totalsmsclonedemo;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.widget.Toast;

@SuppressLint("OverrideAbstract")
@RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
public class NotificationListener extends NotificationListenerService {

    private String TAG = this.getClass().getSimpleName();

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onNotificationPosted(StatusBarNotification sbn) {
        Log.i(TAG, "Notification Posted");
        Log.i(TAG, sbn.getPackageName() +
                "\t" + sbn.getNotification().tickerText +
                "\t" + sbn.getNotification().extras.getString(Notification.EXTRA_TEXT));

        Bundle extras = sbn.getNotification().extras;

        if ("Ongoing call".equals(extras.getString(Notification.EXTRA_TEXT))) {
            Toast.makeText(this, "ongoing", Toast.LENGTH_SHORT).show();
//            startService(new Intent(this, ZajilService.class).setAction(ZajilService.ACTION_CALL_ANSWERED));
        } else if ("Dialing".equals(extras.getString(Notification.EXTRA_TEXT))) {
            Toast.makeText(this, "Dialing", Toast.LENGTH_SHORT).show();
//            startService(new Intent(this, ZajilService.class).setAction(ZajilService.ACTION_CALL_DIALING));
        }
    }

    @Override
    public void onNotificationRemoved(StatusBarNotification sbn) {
        Log.i(TAG, "********** onNotificationRemoved");
        Log.i(TAG, "ID :" + sbn.getId() + "\t" + sbn.getNotification().tickerText + "\t" + sbn.getPackageName());
    }
}
