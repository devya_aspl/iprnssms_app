package com.totalsmsclonedemo.activities;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.SmsManager;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.totalsmsclonedemo.R;
import com.totalsmsclonedemo.dialogFragment.Alert_Dialog_Fragment;
import com.totalsmsclonedemo.dialogFragment.More_Dialog_Fragment;
import com.totalsmsclonedemo.fragments.CallFragment;
import com.totalsmsclonedemo.fragments.CallSummaryFragment;
import com.totalsmsclonedemo.fragments.ConfigurationFragment;
import com.totalsmsclonedemo.fragments.DashboardFragment;
import com.totalsmsclonedemo.fragments.SMSFragment;
import com.totalsmsclonedemo.fragments.SmsSummaryFragment;
import com.totalsmsclonedemo.fragments.SplashFragment;
import com.totalsmsclonedemo.interfaces.MainInterfaces;
import com.totalsmsclonedemo.services.CallDataStoreService;
import com.totalsmsclonedemo.services.SmsDataStoreService;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public abstract class BaseActivity extends AppCompatActivity implements MainInterfaces {

    private static final String TAG = "BaseActivity";

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tv_title)
    TextView tv_title;

    @BindView(R.id.tabs)
    TabLayout tabLayout;

    @BindView(R.id.ll_view_tab)
    LinearLayout ll_view_tab;

//    @BindView(R.id.viewPager)
//    ViewPager viewPager;

    @BindView(R.id.ll_toolbar)
    LinearLayout ll_toolbar;

    @BindView(R.id.imgToolBarBack)
    ImageView imgToolBarBack;

    @BindView(R.id.ll_tab_home)
    LinearLayout ll_tab_home;

    @BindView(R.id.iv_nav_home)
    ImageView iv_nav_home;

    @BindView(R.id.tv_nav_home)
    TextView tv_nav_home;

    @BindView(R.id.ll_tab_call)
    LinearLayout ll_tab_call;

    @BindView(R.id.iv_nav_call)
    ImageView iv_nav_call;

    @BindView(R.id.tv_nav_call)
    TextView tv_nav_call;

    @BindView(R.id.ll_tab_sms)
    LinearLayout ll_tab_sms;

    @BindView(R.id.iv_nav_sms)
    ImageView iv_nav_sms;

    @BindView(R.id.tv_nav_sms)
    TextView tv_nav_sms;

    @BindView(R.id.ll_tab_config)
    LinearLayout ll_tab_config;

    @BindView(R.id.iv_nav_config)
    ImageView iv_nav_config;

    @BindView(R.id.tv_nav_config)
    TextView tv_nav_config;

    @BindView(R.id.ll_tab_more)
    LinearLayout ll_tab_more;

    @BindView(R.id.iv_nav_more)
    ImageView iv_nav_more;

    @BindView(R.id.tv_nav_more)
    TextView tv_nav_more;

    private int[] navIcons = {
            R.drawable.selector_nav_call,
            R.drawable.selector_nav_sms,
            R.drawable.selector_nav_config,
            R.drawable.selector_nav_more
    };
    private int[] navLabels = {
            R.string.nav_call,
            R.string.nav_sms,
            R.string.nav_config,
            R.string.nav_more
    };


    BroadcastReceiver sendBroadcastReceiver = new SentReceiver();
    BroadcastReceiver deliveryBroadcastReciever = new DeliverReceiver();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(false);

//        generateTabLayout();
//        setupTabIcons();


    }

//    private void setupTabIcons() {
//        tabLayout.setSelected(false);
//        TabLayout.Tab firstTab = tabLayout.newTab(); // Create a new Tab names "First Tab"
//        firstTab.setText("Call"); // set the Text for the first Tab
//        firstTab.setIcon(R.drawable.selector_nav_call); // set an icon for the first tab
//        tabLayout.addTab(firstTab,false); // add  the tab to the TabLayout
//
//        TabLayout.Tab secondTab = tabLayout.newTab(); // Create a new Tab names "First Tab"
//        secondTab.setText("SMS"); // set the Text for the first Tab
//        secondTab.setIcon(R.drawable.selector_nav_sms); // set an icon for the first tab
//        tabLayout.addTab(secondTab,false); // add  the tab to the TabLayout
//
//        TabLayout.Tab thirdTab = tabLayout.newTab(); // Create a new Tab names "First Tab"
//        thirdTab.setText("Configuration"); // set the Text for the first Tab
//        thirdTab.setIcon(R.drawable.selector_nav_config); // set an icon for the first tab
//        tabLayout.addTab(thirdTab,false); // add  the tab to the TabLayout
//        TabLayout.Tab forthTab = tabLayout.newTab(); // Create a new Tab names "First Tab"
//        forthTab.setText("More"); // set the Text for the first Tab
//        forthTab.setIcon(R.drawable.selector_nav_more); // set an icon for the first tab
//        tabLayout.addTab(forthTab,false); // add  the tab to the TabLayout
//
//        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
//            @Override
//            public void onTabSelected(TabLayout.Tab tab) {
//                // get the current selected tab's position and replace the fragment accordingly
//                Fragment fragment = null;
//                String name = null;
//                switch (tab.getPosition()) {
//                    case 0:
//                        name = CallFragment.class.getName();
//                        fragment = new CallFragment();
//                        break;
//                    case 1:
//                        name = SMSFragment.class.getName();
//                        fragment = new SMSFragment();
//                        break;
//                    case 2:
//                        name = ConfigurationFragment.class.getName();
//                        fragment = new ConfigurationFragment();
//                        break;
//                    case 3:
//                        name = ConfigurationFragment.class.getName();
//                        fragment = new ConfigurationFragment();
//                        break;
//                }
////                FragmentManager fm = getSupportFragmentManager();
////                FragmentTransaction ft = fm.beginTransaction();
////                ft.replace(R.id.fragment_container, fragment);
////                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
////                ft.commit();
//                replaceFragment(fragment, R.id.fragment_container, name, false);
//            }
//
//            @Override
//            public void onTabUnselected(TabLayout.Tab tab) {
//
//            }
//
//            @Override
//            public void onTabReselected(TabLayout.Tab tab) {
//
//            }
//        });
//    }

//    private void setupTabIcons() {
//        tabLayout.addTab(tabLayout.newTab().setText("Call"));
//        tabLayout.addTab(tabLayout.newTab().setText("SMS"));
//        tabLayout.addTab(tabLayout.newTab().setText("Configuration"));
//        tabLayout.addTab(tabLayout.newTab().setText("More"));
//    }

//    private void setupTabIcons() {
//        LinearLayout tabOne = (LinearLayout) LayoutInflater.from(this).inflate(R.layout.nav_tab, null);
//        TextView tab_label = (TextView) tabOne.findViewById(R.id.nav_label);
//        ImageView tab_icon = (ImageView) tabOne.findViewById(R.id.nav_icon);
//        tab_label.setText(getResources().getString(R.string.nav_call));
//        tab_icon.setImageResource(R.drawable.selector_nav_call);
//        tabLayout.getTabAt(0).setCustomView(tabOne);
//
//        LinearLayout tabTwo = (LinearLayout) LayoutInflater.from(this).inflate(R.layout.nav_tab, null);
//        TextView tab_labe2 = (TextView) tabTwo.findViewById(R.id.nav_label);
//        ImageView tab_icon2 = (ImageView) tabTwo.findViewById(R.id.nav_icon);
//        tab_labe2.setText(getResources().getString(R.string.nav_sms));
//        tab_icon2.setImageResource(R.drawable.selector_nav_sms);
//        tabLayout.getTabAt(1).setCustomView(tabTwo);
//
//        LinearLayout tabThree = (LinearLayout) LayoutInflater.from(this).inflate(R.layout.nav_tab, null);
//        TextView tab_labe3 = (TextView) tabThree.findViewById(R.id.nav_label);
//        ImageView tab_icon3 = (ImageView) tabThree.findViewById(R.id.nav_icon);
//        tab_labe3.setText(getResources().getString(R.string.nav_config));
//        tab_icon3.setImageResource(R.drawable.selector_nav_config);
//        tabLayout.getTabAt(2).setCustomView(tabThree);
//
//        LinearLayout tabFour = (LinearLayout) LayoutInflater.from(this).inflate(R.layout.nav_tab, null);
//        TextView tab_labe4 = (TextView) tabFour.findViewById(R.id.nav_label);
//        ImageView tab_icon4 = (ImageView) tabFour.findViewById(R.id.nav_icon);
//        tab_labe4.setText(getResources().getString(R.string.nav_more));
//        tab_icon4.setImageResource(R.drawable.selector_nav_more);
//        tabLayout.getTabAt(3).setCustomView(tabFour);
//
//    }

//    private void generateTabLayout() {
////        tabLayout.setupWithViewPager(viewPager);
////
////        // loop through all navigation tabs
//        for (int i = 0; i < tabLayout.getTabCount(); i++) {
//            // inflate the Parent LinearLayout Container for the tab
//            // from the layout nav_tab.xml file that we created 'R.layout.nav_tab
//            LinearLayout tab = (LinearLayout) LayoutInflater.from(this).inflate(R.layout.nav_tab, null);
//
//            // get child TextView and ImageView from this layout for the icon and label
//            TextView tab_label = (TextView) tab.findViewById(R.id.nav_label);
//            ImageView tab_icon = (ImageView) tab.findViewById(R.id.nav_icon);
//
//            // set the label text by getting the actual string value by its id
//            // by getting the actual resource value `getResources().getString(string_id)`
//            tab_label.setText(getResources().getString(navLabels[i]));
//            tab_icon.setImageResource(navIcons[i]);
//
//            // set the home to be active at first
////            if(i == 0) {
////                tab_label.setTextColor(getResources().getColor(R.color.efent_color));
////                tab_icon.setImageResource(navIconsActive[i]);
////            } else {
////                tab_icon.setImageResource(navIcons[i]);
////            }
//
//            // finally publish this custom view to navigation tab
//            tabLayout.getTabAt(i).setCustomView(tab);
//
//        }
//    }

    @Override
    public void OpenSplash() {
//        tabLayout.setVisibility(View.GONE);
        replaceFragment(new SplashFragment(), R.id.fragment_container, SplashFragment.class.getName(), false);
    }

    @Override
    public void OpenDashBoard() {
//        tabLayout.setVisibility(View.VISIBLE);
        setHomeSelected();
        setUnselectOtherThenHome();
        replaceFragment(new DashboardFragment(), R.id.fragment_container, DashboardFragment.class.getName(), false);
    }

    @Override
    public void OpenCall() {
//        tabLayout.setVisibility(View.VISIBLE);
        setCallSelected();
        setUnselectOtherThenCall();
        replaceFragment(new CallFragment(), R.id.fragment_container, CallFragment.class.getName(), false);
    }

    @Override
    public void OpenSms() {
//        tabLayout.setVisibility(View.VISIBLE);
        setSMSSelected();
        setUnselectOtherThenSMS();
        replaceFragment(new SMSFragment(), R.id.fragment_container, SMSFragment.class.getName(), false);
    }



    @Override
    public void OpenConfiguration() {
//        tabLayout.setVisibility(View.VISIBLE);
        setConfigSelected();
        setUnselectOtherThenConfig();
        replaceFragment(new ConfigurationFragment(), R.id.fragment_container, ConfigurationFragment.class.getName(), false);
    }

    @Override
    public void OpenMore() {
        setMoreSelected();
        setUnselectOtherThenMore();
        More_Dialog_Fragment cdd = new More_Dialog_Fragment(this);
        cdd.show();
    }

    @Override
    public void OpenSmsSummary() {
        setMoreSelected();
        setUnselectOtherThenMore();
        replaceFragment(new SmsSummaryFragment(), R.id.fragment_container, SmsSummaryFragment.class.getName(), false);
    }

    @Override
    public void OpenCallSummary() {
        replaceFragment(new CallSummaryFragment(), R.id.fragment_container, CallSummaryFragment.class.getName(), false);
        setMoreSelected();
        setUnselectOtherThenMore();

    }

    public void replaceFragment(Fragment mFragment, int id, String tag, boolean addToStack) {
        FragmentTransaction mTransaction = getSupportFragmentManager().beginTransaction();
        mTransaction.replace(id, mFragment);
        hideKeyboard();
        if (addToStack) {
            mTransaction.addToBackStack(tag);
        }
        mTransaction.commitAllowingStateLoss();
    }

    public void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public Toolbar getToolbar() {
        return toolbar;
    }

    public void setToolbar(Toolbar toolbar) {
        this.toolbar = toolbar;
    }

    public LinearLayout getLl_toolbar() {
        return ll_toolbar;
    }

    public void setLl_toolbar(LinearLayout ll_toolbar) {
        this.ll_toolbar = ll_toolbar;
    }

    public LinearLayout getLl_view_tab() {
        return ll_view_tab;
    }

    public void setLl_view_tab(LinearLayout ll_view_tab) {
        this.ll_view_tab = ll_view_tab;
    }

    public TabLayout getTabLayout() {
        return tabLayout;
    }

    public void setTabLayout(TabLayout tabLayout)
    {
        this.tabLayout = tabLayout;
    }

    public TextView getToolbartext() {
        return tv_title;
    }

    public void setToolbartext(TextView toolbartext) {
        this.tv_title = toolbartext;
    }

    public ImageView getImgToolBarBack() {
        return imgToolBarBack;
    }

    public void setImgToolBarBack(ImageView imgToolBarBack) {
        this.imgToolBarBack = imgToolBarBack;
    }

    public void setHomeSelected()
    {
        iv_nav_home.setImageResource(R.drawable.nav_home_selected);
        tv_nav_home.setTextColor(getResources().getColor(R.color.selectionAndMenuColor));
    }

//    public void setMoreSelected()
//    {
//        iv.setImageResource(R.drawable.nav_home_selected);
//        tv_nav_home.setTextColor(getResources().getColor(R.color.selectionAndMenuColor));
//    }

    public void setUnselectOtherThenHome()
    {
        iv_nav_call.setImageResource(R.drawable.nav_call);
        tv_nav_call.setTextColor(getResources().getColor(R.color.normalTab));

        iv_nav_sms.setImageResource(R.drawable.nav_sms);
        tv_nav_sms.setTextColor(getResources().getColor(R.color.normalTab));

        iv_nav_config.setImageResource(R.drawable.nav_config);
        tv_nav_config.setTextColor(getResources().getColor(R.color.normalTab));

        iv_nav_more.setImageResource(R.drawable.more_grey);
        tv_nav_more.setTextColor(getResources().getColor(R.color.normalTab));
    }

    public void setUnselectOtherThenCall()
    {
        iv_nav_home.setImageResource(R.drawable.nav_home);
        tv_nav_home.setTextColor(getResources().getColor(R.color.normalTab));

        iv_nav_sms.setImageResource(R.drawable.nav_sms);
        tv_nav_sms.setTextColor(getResources().getColor(R.color.normalTab));

        iv_nav_config.setImageResource(R.drawable.nav_config);
        tv_nav_config.setTextColor(getResources().getColor(R.color.normalTab));

        iv_nav_more.setImageResource(R.drawable.more_grey);
        tv_nav_more.setTextColor(getResources().getColor(R.color.normalTab));
    }

    public void setUnselectOtherThenSMS()
    {
        iv_nav_home.setImageResource(R.drawable.nav_home);
        tv_nav_home.setTextColor(getResources().getColor(R.color.normalTab));

        iv_nav_call.setImageResource(R.drawable.nav_call);
        tv_nav_call.setTextColor(getResources().getColor(R.color.normalTab));

        iv_nav_config.setImageResource(R.drawable.nav_config);
        tv_nav_config.setTextColor(getResources().getColor(R.color.normalTab));

        iv_nav_more.setImageResource(R.drawable.more_grey);
        tv_nav_more.setTextColor(getResources().getColor(R.color.normalTab));
    }

    public void setUnselectOtherThenConfig()
    {
        iv_nav_home.setImageResource(R.drawable.nav_home);
        tv_nav_home.setTextColor(getResources().getColor(R.color.normalTab));

        iv_nav_call.setImageResource(R.drawable.nav_call);
        tv_nav_call.setTextColor(getResources().getColor(R.color.normalTab));

        iv_nav_sms.setImageResource(R.drawable.nav_sms);
        tv_nav_sms.setTextColor(getResources().getColor(R.color.normalTab));

        iv_nav_more.setImageResource(R.drawable.more_grey);
        tv_nav_more.setTextColor(getResources().getColor(R.color.normalTab));
    }

    public void setUnselectOtherThenMore()
    {
        iv_nav_home.setImageResource(R.drawable.nav_home);
        tv_nav_home.setTextColor(getResources().getColor(R.color.normalTab));

        iv_nav_call.setImageResource(R.drawable.nav_call);
        tv_nav_call.setTextColor(getResources().getColor(R.color.normalTab));

        iv_nav_config.setImageResource(R.drawable.nav_config);
        tv_nav_config.setTextColor(getResources().getColor(R.color.normalTab));

        iv_nav_sms.setImageResource(R.drawable.nav_sms);
        tv_nav_sms.setTextColor(getResources().getColor(R.color.normalTab));
    }

    public void setCallSelected()
    {
        iv_nav_call.setImageResource(R.drawable.nav_call_selected);
        tv_nav_call.setTextColor(getResources().getColor(R.color.selectionAndMenuColor));
    }
    public void setSMSSelected()
    {
        iv_nav_sms.setImageResource(R.drawable.nav_sms_selected);
        tv_nav_sms.setTextColor(getResources().getColor(R.color.selectionAndMenuColor));
    }

    public void setConfigSelected()
    {
        iv_nav_config.setImageResource(R.drawable.nav_config_selected);
        tv_nav_config.setTextColor(getResources().getColor(R.color.selectionAndMenuColor));
    }

    public void setMoreSelected()
    {
        iv_nav_more.setImageResource(R.drawable.more_selected);
        tv_nav_more.setTextColor(getResources().getColor(R.color.selectionAndMenuColor));
    }

    @OnClick(R.id.ll_tab_call)
    public void onCallClicked(View v)
    {
        OpenCall();
    }

    @OnClick(R.id.ll_tab_home)
    public void onHomeClicked(View v)
    {
        OpenDashBoard();
    }

    @OnClick(R.id.ll_tab_sms)
    public void onSMSClicked(View v)
    {
        OpenSms();
    }

    @OnClick(R.id.ll_tab_config)
    public void onConfigClicked(View v)
    {
        OpenConfiguration();
    }

    @OnClick(R.id.ll_tab_more)
    public void onMoreClicked(View v)
    {
        OpenMore();
    }

//    @OnClick(R.id.ll)
//    public void onConfigClicked(View v)
//    {
//        OpenConfiguration();
//    }


    @Override
    public void onBackPressed() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        if (fragment instanceof DashboardFragment) {
            Alert_Dialog_Fragment cdd = new Alert_Dialog_Fragment(this);
            cdd.show();
        }else if (fragment instanceof CallFragment) {
            OpenDashBoard();
        } else if (fragment instanceof SMSFragment) {
            OpenDashBoard();
        } else if (fragment instanceof ConfigurationFragment) {
            OpenDashBoard();
        } else if (fragment instanceof SplashFragment) {
            finish();
        } else {
            super.onBackPressed();
            hideKeyboard();
        }
//        else if (fragment instanceof DashboardFragment) {
//            finish();
//        }
//        else if (fragment instanceof TermsAndConditionsFragment) {
//            OpenLogin_Screen();
//        }


    }

    public void sendSMS(String phoneNumber, String message, final String actualPhoneNumber, final String image) {
        String SENT = "SMS_SENT";
        String DELIVERED = "SMS_DELIVERED";

        PendingIntent sentPI = PendingIntent.getBroadcast(this, 0, new Intent(
                SENT), 0);

        PendingIntent deliveredPI = PendingIntent.getBroadcast(this, 0,
                new Intent(DELIVERED), 0);

        registerReceiver(sendBroadcastReceiver, new IntentFilter(SENT));

        registerReceiver(deliveryBroadcastReciever, new IntentFilter(DELIVERED));
        SmsManager sms = SmsManager.getDefault();
        sms.sendTextMessage(phoneNumber, null, message, sentPI, deliveredPI);

    }


   public class DeliverReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent arg1) {
            switch (getResultCode()) {
                case Activity.RESULT_OK:
                    Toast.makeText(getBaseContext(), "Delivered",
                            Toast.LENGTH_SHORT).show();

                    break;
                case Activity.RESULT_CANCELED:
                    Toast.makeText(getBaseContext(), "Failed",
                            Toast.LENGTH_SHORT).show();
                    break;
            }

        }
    }

   public class SentReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent arg1) {
            switch (getResultCode()) {
                case Activity.RESULT_OK:
                    Toast.makeText(getBaseContext(), "SMS sent", Toast.LENGTH_SHORT)
                            .show();
//                    startActivity(new Intent(SendSMS.this, ChooseOption.class));
//                    overridePendingTransition(R.anim.animation, R.anim.animation2);
                    break;
                case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                    Toast.makeText(getBaseContext(), "Generic failure",
                            Toast.LENGTH_SHORT).show();
                    break;
                case SmsManager.RESULT_ERROR_NO_SERVICE:
                    Toast.makeText(getBaseContext(), "No service",
                            Toast.LENGTH_SHORT).show();
                    break;
                case SmsManager.RESULT_ERROR_NULL_PDU:
                    Toast.makeText(getBaseContext(), "Null PDU", Toast.LENGTH_SHORT)
                            .show();
                    break;
                case SmsManager.RESULT_ERROR_RADIO_OFF:
                    Toast.makeText(getBaseContext(), "Radio off",
                            Toast.LENGTH_SHORT).show();
                    break;
            }

        }
    }

    public void callStoreServiceCall()
    {
        Intent i = new Intent(BaseActivity.this, CallDataStoreService.class);
        this.startService(i);
    }

    public void smsStoreServiceCall()
    {
        Intent i = new Intent(BaseActivity.this, SmsDataStoreService.class);
        this.startService(i);
    }


//    //---sends an SMS message to another device---
//    private void sendSMS(String phoneNumber, String message, final String actualPhoneNumber, final String image)
//    {
//        try {
//
//
////            int i=0;
////            int gotSmsNumber = Integer.parseInt(Utilities.getNoSmsSendEachTime(context));
////                Log.e("adapter", "Got sms Number: " + gotSmsNumber);
////            while (i < gotSmsNumber)
////            {
////
////            }
////
////            for (int j=1;j <= gotSmsNumber; j++)
////            {
////                Log.e("adapter","index: "+j);
//
//
//            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
////        Date currentFromApi = simpleDateFormat.parse();
////        Log.e("SMSAdapter", "Api Date: " + currentFromApi);
//            Long system_timestamp = System.currentTimeMillis();
//            final String system_str_date = getDate(system_timestamp);
//            try {
//                Date current_system_date = simpleDateFormat.parse(system_str_date);
//            } catch (ParseException e) {
//                e.printStackTrace();
//            }
//
//            DateFormat dateFormat = new SimpleDateFormat("hh.mm aa");
//            final String timeString = dateFormat.format(new Date()).toString();
//            System.out.println("Current time in AM/PM: " + timeString);
//            Log.e("SMSAdapter", "Current Timestamp: " + system_timestamp);
//            Log.e("SMSAdapter", "Current date: " + system_str_date);
//            Log.e("SMSAdapter", "Current time: " + timeString);
////
////        differenceTime = printDifference(currentFromApi, current_system_date);
////
////        Log.e("SMSAdapter", "difference Time " + differenceTime);
//
//
//            String SENT = "SMS_SENT";
//            String DELIVERED = "SMS_DELIVERED";
//
//            PendingIntent sentPI = PendingIntent.getBroadcast(context, 0,
//                    new Intent(SENT), 0);
//
//            PendingIntent deliveredPI = PendingIntent.getBroadcast(context, 0,
//                    new Intent(DELIVERED), 0);
//
//            //---when the SMS has been sent---
//            registerReceiver(new BroadcastReceiver() {
//                @Override
//                public void onReceive(Context arg0, Intent arg1) {
//                    switch (getResultCode()) {
//                        case Activity.RESULT_OK:
//                            Toast.makeText(context, "SMS sent",
//                                    Toast.LENGTH_SHORT).show();
//                            break;
//                        case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
//                            Toast.makeText(context, "Generic failure",
//                                    Toast.LENGTH_SHORT).show();
//                            break;
//                        case SmsManager.RESULT_ERROR_NO_SERVICE:
//                            Toast.makeText(context, "No service",
//                                    Toast.LENGTH_SHORT).show();
//                            break;
//                        case SmsManager.RESULT_ERROR_NULL_PDU:
//                            Toast.makeText(context, "Null PDU",
//                                    Toast.LENGTH_SHORT).show();
//                            break;
//                        case SmsManager.RESULT_ERROR_RADIO_OFF:
//                            Toast.makeText(context, "Radio off",
//                                    Toast.LENGTH_SHORT).show();
//                            break;
//                    }
//                }
//            }, new IntentFilter(SENT));
//
//            //---when the SMS has been delivered---
//            registerReceiver(new BroadcastReceiver() {
//                @Override
//                public void onReceive(Context arg0, Intent arg1) {
//                    switch (getResultCode()) {
//                        case Activity.RESULT_OK:
//                            Toast.makeText(context, "SMS delivered",
//                                    Toast.LENGTH_SHORT).show();
//                            status = "delivered";
//                            Log.e("adapter","status is: "+status);
////                                storeData(image, actualPhoneNumber, "delivered", system_str_date, timeString);
//                            dbHelperForSmsSummary.insertSmsSummaryDetails(new SMSSummaryDbo(image,actualPhoneNumber,status,timeString,system_str_date));
//                            break;
//                        case Activity.RESULT_CANCELED:
//                            Toast.makeText(context, "SMS not delivered",
//                                    Toast.LENGTH_SHORT).show();
//                            status = "failed";
//                            Log.e("adapter","status is: "+status);
////                                storeData(image, actualPhoneNumber, "Undelivered", system_str_date, timeString);
//                            dbHelperForSmsSummary.insertSmsSummaryDetails(new SMSSummaryDbo(image,actualPhoneNumber,status,timeString,system_str_date));
//                            break;
//                    }
//                }
//            }, new IntentFilter(DELIVERED));
////                Log.e("adapter","status is: "+status);
////            dbHelperForSmsSummary.insertSmsSummaryDetails(new SMSSummaryDbo(image,actualPhoneNumber,status,timeString,system_str_date));
//
////        PendingIntent pi = PendingIntent.getActivity(this, 0,
////                new Intent(context, SMS.class), 0);
//
//            SmsManager sms = SmsManager.getDefault();
//            sms.sendTextMessage(phoneNumber, null, message, sentPI, deliveredPI);
//
////            }
//
//
////            if (!Utilities.getNoSmsSendEachTime(context).equalsIgnoreCase("")) {
////
////                int gotSmsNumber = Integer.parseInt(Utilities.getNoSmsSendEachTime(context));
////                Log.e("adapter", "Got sms Number: " + gotSmsNumber);
////                for (int i = 0; i < gotSmsNumber; i++) {
////                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
//////        Date currentFromApi = simpleDateFormat.parse();
//////        Log.e("SMSAdapter", "Api Date: " + currentFromApi);
////                    Long system_timestamp = System.currentTimeMillis();
////                    final String system_str_date = getDate(system_timestamp);
////                    try {
////                        Date current_system_date = simpleDateFormat.parse(system_str_date);
////                    } catch (ParseException e) {
////                        e.printStackTrace();
////                    }
////
////                    DateFormat dateFormat = new SimpleDateFormat("hh.mm aa");
////                    final String timeString = dateFormat.format(new Date()).toString();
////                    System.out.println("Current time in AM/PM: " + timeString);
////                    Log.e("SMSAdapter", "Current Timestamp: " + system_timestamp);
////                    Log.e("SMSAdapter", "Current date: " + system_str_date);
////                    Log.e("SMSAdapter", "Current time: " + timeString);
//////
//////        differenceTime = printDifference(currentFromApi, current_system_date);
//////
//////        Log.e("SMSAdapter", "difference Time " + differenceTime);
////
////
////                    String SENT = "SMS_SENT";
////                    String DELIVERED = "SMS_DELIVERED";
////
////                    PendingIntent sentPI = PendingIntent.getBroadcast(context, 0,
////                            new Intent(SENT), 0);
////
////                    PendingIntent deliveredPI = PendingIntent.getBroadcast(context, 0,
////                            new Intent(DELIVERED), 0);
////
////                    //---when the SMS has been sent---
////                    context.registerReceiver(new BroadcastReceiver() {
////                        @Override
////                        public void onReceive(Context arg0, Intent arg1) {
////                            switch (getResultCode()) {
////                                case Activity.RESULT_OK:
////                                    Toast.makeText(context, "SMS sent",
////                                            Toast.LENGTH_SHORT).show();
////                                    break;
////                                case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
////                                    Toast.makeText(context, "Generic failure",
////                                            Toast.LENGTH_SHORT).show();
////                                    break;
////                                case SmsManager.RESULT_ERROR_NO_SERVICE:
////                                    Toast.makeText(context, "No service",
////                                            Toast.LENGTH_SHORT).show();
////                                    break;
////                                case SmsManager.RESULT_ERROR_NULL_PDU:
////                                    Toast.makeText(context, "Null PDU",
////                                            Toast.LENGTH_SHORT).show();
////                                    break;
////                                case SmsManager.RESULT_ERROR_RADIO_OFF:
////                                    Toast.makeText(context, "Radio off",
////                                            Toast.LENGTH_SHORT).show();
////                                    break;
////                            }
////                        }
////                    }, new IntentFilter(SENT));
////
////                    //---when the SMS has been delivered---
////                    context.registerReceiver(new BroadcastReceiver() {
////                        @Override
////                        public void onReceive(Context arg0, Intent arg1) {
////                            switch (getResultCode()) {
////                                case Activity.RESULT_OK:
////                                    Toast.makeText(context, "SMS delivered",
////                                            Toast.LENGTH_SHORT).show();
////                                    storeData(image, actualPhoneNumber, "delivered", system_str_date, timeString);
////
////                                    break;
////                                case Activity.RESULT_CANCELED:
////                                    Toast.makeText(context, "SMS not delivered",
////                                            Toast.LENGTH_SHORT).show();
////                                    storeData(image, actualPhoneNumber, "Undelivered", system_str_date, timeString);
////                                    break;
////                            }
////                        }
////                    }, new IntentFilter(DELIVERED));
////
//////        PendingIntent pi = PendingIntent.getActivity(this, 0,
//////                new Intent(context, SMS.class), 0);
////
////                    SmsManager sms = SmsManager.getDefault();
////                    sms.sendTextMessage(phoneNumber, null, message, sentPI, deliveredPI);
//////                }
//////
//////            }
//////            else
//////            {
//////                SmsManager sms = SmsManager.getDefault();
//////                sms.sendTextMessage(phoneNumber, null, message, sentPI, deliveredPI);
////                }
////
////            } else {
////
//////                int gotSmsNumber = Integer.parseInt(Utilities.getNoSmsSendEachTime(context));
//////                Log.e("adapter", "Got sms Number: " + gotSmsNumber);
//////                for (int i = 0; i < gotSmsNumber; i++) {
////                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
//////        Date currentFromApi = simpleDateFormat.parse();
//////        Log.e("SMSAdapter", "Api Date: " + currentFromApi);
////                    Long system_timestamp = System.currentTimeMillis();
////                    final String system_str_date = getDate(system_timestamp);
////                    try {
////                        Date current_system_date = simpleDateFormat.parse(system_str_date);
////                    } catch (ParseException e) {
////                        e.printStackTrace();
////                    }
////
////                    DateFormat dateFormat = new SimpleDateFormat("hh.mm aa");
////                    final String timeString = dateFormat.format(new Date()).toString();
////                    System.out.println("Current time in AM/PM: " + timeString);
////                    Log.e("SMSAdapter", "Current Timestamp: " + system_timestamp);
////                    Log.e("SMSAdapter", "Current date: " + system_str_date);
////                    Log.e("SMSAdapter", "Current time: " + timeString);
//////
//////        differenceTime = printDifference(currentFromApi, current_system_date);
//////
//////        Log.e("SMSAdapter", "difference Time " + differenceTime);
////
////
////                    String SENT = "SMS_SENT";
////                    String DELIVERED = "SMS_DELIVERED";
////
////                    PendingIntent sentPI = PendingIntent.getBroadcast(context, 0,
////                            new Intent(SENT), 0);
////
////                    PendingIntent deliveredPI = PendingIntent.getBroadcast(context, 0,
////                            new Intent(DELIVERED), 0);
////
////                    //---when the SMS has been sent---
////                    context.registerReceiver(new BroadcastReceiver() {
////                        @Override
////                        public void onReceive(Context arg0, Intent arg1) {
////                            switch (getResultCode()) {
////                                case Activity.RESULT_OK:
////                                    Toast.makeText(context, "SMS sent",
////                                            Toast.LENGTH_SHORT).show();
////                                    break;
////                                case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
////                                    Toast.makeText(context, "Generic failure",
////                                            Toast.LENGTH_SHORT).show();
////                                    break;
////                                case SmsManager.RESULT_ERROR_NO_SERVICE:
////                                    Toast.makeText(context, "No service",
////                                            Toast.LENGTH_SHORT).show();
////                                    break;
////                                case SmsManager.RESULT_ERROR_NULL_PDU:
////                                    Toast.makeText(context, "Null PDU",
////                                            Toast.LENGTH_SHORT).show();
////                                    break;
////                                case SmsManager.RESULT_ERROR_RADIO_OFF:
////                                    Toast.makeText(context, "Radio off",
////                                            Toast.LENGTH_SHORT).show();
////                                    break;
////                            }
////                        }
////                    }, new IntentFilter(SENT));
////
////                    //---when the SMS has been delivered---
////                    context.registerReceiver(new BroadcastReceiver() {
////                        @Override
////                        public void onReceive(Context arg0, Intent arg1) {
////                            switch (getResultCode()) {
////                                case Activity.RESULT_OK:
////                                    Toast.makeText(context, "SMS delivered",
////                                            Toast.LENGTH_SHORT).show();
////                                    storeData(image, actualPhoneNumber, "delivered", system_str_date, timeString);
////
////                                    break;
////                                case Activity.RESULT_CANCELED:
////                                    Toast.makeText(context, "SMS not delivered",
////                                            Toast.LENGTH_SHORT).show();
////                                    storeData(image, actualPhoneNumber, "Undelivered", system_str_date, timeString);
////                                    break;
////                            }
////                        }
////                    }, new IntentFilter(DELIVERED));
////
//////        PendingIntent pi = PendingIntent.getActivity(this, 0,
//////                new Intent(context, SMS.class), 0);
////
////                    SmsManager sms = SmsManager.getDefault();
////                    sms.sendTextMessage(phoneNumber, null, message, sentPI, deliveredPI);
//////                }
////            }
//        }
//        catch (Exception e)
//        {
//            e.printStackTrace();
//        }
//    }


}
