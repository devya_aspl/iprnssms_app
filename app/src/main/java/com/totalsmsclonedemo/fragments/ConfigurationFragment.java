package com.totalsmsclonedemo.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.totalsmsclonedemo.R;
import com.totalsmsclonedemo.Utilities;
import com.totalsmsclonedemo.activities.BaseActivity;
import com.totalsmsclonedemo.databases.DBHelperForConfigurationDetails;
import com.totalsmsclonedemo.dbo.ProjectConfigurationDbo;
import com.totalsmsclonedemo.events.ApiErrorEvent;
import com.totalsmsclonedemo.events.ApiErrorWithMessageEvent;

import org.greenrobot.eventbus.Subscribe;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class ConfigurationFragment extends BaseFragment {

    View v;
    Unbinder unbinder;

    @BindView(R.id.edt_call_seconds)
    EditText edt_call_seconds;

    @BindView(R.id.edt_send_numbers)
    EditText edt_send_numbers;

    @BindView(R.id.btn_save)
    Button btn_save;

    @BindView(R.id.sp_call_disconnect_time)
    Spinner sp_call_disconnect_time;

    @BindView(R.id.sp_no_time_send_sms)
    Spinner sp_no_time_send_sms;

    DBHelperForConfigurationDetails dbHelperConfig;
    public static final String TAG = ConfigurationFragment.class.getSimpleName();

    ArrayAdapter<String> adapterChooseCallDisconnectTime,adapterChooseNoSmsSend;

    List<ProjectConfigurationDbo> getConfigData;

    String selectedDisconnectTime="",selectedSmsTimes="";


    @Override
    public void setToolbarForFragment() {
        ((BaseActivity) getActivity()).getToolbar().setVisibility(View.VISIBLE);
        ((BaseActivity) getActivity()).getLl_toolbar().setVisibility(View.VISIBLE);
        ((BaseActivity) getActivity()).getToolbartext().setVisibility(View.VISIBLE);
        ((BaseActivity) getActivity()).getToolbartext().setText("Configuration");

        ((BaseActivity)getActivity()).getImgToolBarBack().setVisibility(View.VISIBLE);
        ((BaseActivity)getActivity()).getImgToolBarBack().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validationNew()) {
                    SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
//            Date currentFromApi = simpleDateFormat1.parse(Utilities.getGotCurrentDate((BaseActivity) getActivity()));
//            Log.e(TAG, "Api Date: " + currentFromApi);
                    Long system_timestamp = System.currentTimeMillis();
                    String system_str_date = getDate(system_timestamp);
                    try {
                        Date current_system_date = simpleDateFormat1.parse(system_str_date);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    Log.e(TAG, "Configuration Current Timestamp: " + system_timestamp);
                    Log.e(TAG, "Configuration Current date: " + system_str_date);

                    Random r = new Random();
                    int randomNumber = r.nextInt(5);

                    String random_timerId = randomNumber + "_" + system_timestamp;
//            timeId,call_disconnect_time,no_sms_send_each_time
                    dbHelperConfig.insertConfigurationDetails(new ProjectConfigurationDbo(random_timerId,sp_call_disconnect_time.getSelectedItem().toString(),sp_no_time_send_sms.getSelectedItem().toString()));
                    hideKeyboard();
//                    String firstWord = "Magic Word";
                    String disconnectTime=sp_call_disconnect_time.getSelectedItem().toString();
                    if(disconnectTime.contains(" ")){
                        disconnectTime= disconnectTime.substring(0, disconnectTime.indexOf(" "));
                        System.out.println(disconnectTime);
                    }
                    String smsTime=sp_no_time_send_sms.getSelectedItem().toString();
                    if(smsTime.contains(" ")){
                        smsTime= smsTime.substring(0, smsTime.indexOf(" "));
                        System.out.println(smsTime);
                    }


                    Utilities.storeValue((BaseActivity)getActivity(),Utilities.DISCONNECT_DURATION_TIME,disconnectTime);
                    Utilities.storeValue((BaseActivity)getActivity(),Utilities.NO_SMS_SEND_EACH_TIME,smsTime);
                    mainInterfaces.OpenDashBoard();
                }
                else
                {
                    Toast.makeText((BaseActivity)getActivity(),"Something wrong",Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    @Override
    public void setTabLayout() {
        ((BaseActivity) getActivity()).getLl_view_tab().setVisibility(View.VISIBLE);
//        ((BaseActivity) getActivity()).getTabLayout().setVisibility(View.VISIBLE);
    }

    public ConfigurationFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_configuration, container, false);
        unbinder = ButterKnife.bind(this, v);
        dbHelperConfig = new DBHelperForConfigurationDetails((BaseActivity)getActivity());
        setUpSpinner();

        try {

            int configCount = dbHelperConfig.getConfigurationCount();
            Log.e(TAG, "Config Count is: " + configCount);

            if (configCount > 0) {
                getConfigData = new ArrayList<>();
                getConfigData = dbHelperConfig.getConfigurationDbo();
//                for (int i = 0; i < getConfigData.size(); i++) {
//                    selectedDisconnectTime = getConfigData.get(i).getCall_disconnect_time();
//                    selectedSmsTimes = getConfigData.get(i).getNo_sms_send_each_time();
//
//                    Log.e("Config getdata :",
//                            "timer id is:  "+ getConfigData.get(i).getTimeId()
//                                    + "selected disconnect time is: "+ selectedDisconnectTime
//                                    + "selected sms times: "+ selectedSmsTimes
//                    );
//
//                }

                selectedDisconnectTime = getConfigData.get(getConfigData.size()-1).getCall_disconnect_time();
                selectedSmsTimes = getConfigData.get(getConfigData.size()-1).getNo_sms_send_each_time();

                if (selectedDisconnectTime != null) {
                    int spinnerPosition = adapterChooseCallDisconnectTime.getPosition(selectedDisconnectTime);
                    sp_call_disconnect_time.setSelection(spinnerPosition);
                }
                if (selectedSmsTimes != null) {
                    int spinnerPosition = adapterChooseNoSmsSend.getPosition(selectedSmsTimes);
                    sp_no_time_send_sms.setSelection(spinnerPosition);
                }
            }
            else
            {
                Log.e(TAG,"Count is 0");
                String noTime= "1 Time";
                String disconnectTime= "1 Minute";
                dbHelperConfig.insertConfigurationDetails(new ProjectConfigurationDbo("1",disconnectTime,noTime));
//                if (selectedDisconnectTime != null) {
                int spinnerPosition = adapterChooseCallDisconnectTime.getPosition(disconnectTime);
                sp_call_disconnect_time.setSelection(spinnerPosition);
//                }
//                if (selectedSmsTimes != null) {
                int spinnerPosition2 = adapterChooseNoSmsSend.getPosition(noTime);
                sp_no_time_send_sms.setSelection(spinnerPosition2);
//                }

                Utilities.storeValue((BaseActivity)getActivity(),Utilities.DISCONNECT_DURATION_TIME,"1");
                Utilities.storeValue((BaseActivity)getActivity(),Utilities.NO_SMS_SEND_EACH_TIME,"1");
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return v;
    }

    private void setUpSpinner() {
        List<String> spinnerChooseCallDisconnect=  new ArrayList<String>();
        spinnerChooseCallDisconnect.add("Select");
        spinnerChooseCallDisconnect.add("1 Minute");
        spinnerChooseCallDisconnect.add("2 Minutes");
        spinnerChooseCallDisconnect.add("5 Minutes");
        spinnerChooseCallDisconnect.add("10 Minutes");
        spinnerChooseCallDisconnect.add("15 Minutes");

        adapterChooseCallDisconnectTime = new ArrayAdapter<String>((BaseActivity)getActivity(), android.R.layout.simple_spinner_item, spinnerChooseCallDisconnect);

        adapterChooseCallDisconnectTime.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        sp_call_disconnect_time.setAdapter(adapterChooseCallDisconnectTime);

//        sp_sort_country.setOnItemSelectedListener(this);

        List<String> spinnerChooseNoSmsSend=  new ArrayList<String>();
        spinnerChooseNoSmsSend.add("Select");
        spinnerChooseNoSmsSend.add("1 Time");
        spinnerChooseNoSmsSend.add("2 Times");
        spinnerChooseNoSmsSend.add("3 Times");
        spinnerChooseNoSmsSend.add("4 Times");
        spinnerChooseNoSmsSend.add("5 Times");

        adapterChooseNoSmsSend = new ArrayAdapter<String>((BaseActivity)getActivity(), android.R.layout.simple_spinner_item, spinnerChooseNoSmsSend);

        adapterChooseNoSmsSend.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        sp_no_time_send_sms.setAdapter(adapterChooseNoSmsSend);
    }

    @OnClick(R.id.btn_save)
    public void onClickSave(View v)  {

        if (validationNew()) {
            SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
//            Date currentFromApi = simpleDateFormat1.parse(Utilities.getGotCurrentDate((BaseActivity) getActivity()));
//            Log.e(TAG, "Api Date: " + currentFromApi);
            Long system_timestamp = System.currentTimeMillis();
            String system_str_date = getDate(system_timestamp);
            try {
                Date current_system_date = simpleDateFormat1.parse(system_str_date);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Log.e(TAG, "Configuration Current Timestamp: " + system_timestamp);
            Log.e(TAG, "Configuration Current date: " + system_str_date);

            Random r = new Random();
            int randomNumber = r.nextInt(5);

            String random_timerId = randomNumber + "_" + system_timestamp;
//            timeId,call_disconnect_time,no_sms_send_each_time
            dbHelperConfig.insertConfigurationDetails(new ProjectConfigurationDbo(random_timerId,sp_call_disconnect_time.getSelectedItem().toString(),sp_no_time_send_sms.getSelectedItem().toString()));
            hideKeyboard();
            Utilities.storeValue((BaseActivity)getActivity(),Utilities.DISCONNECT_DURATION_TIME,sp_call_disconnect_time.getSelectedItem().toString());
            Utilities.storeValue((BaseActivity)getActivity(),Utilities.NO_SMS_SEND_EACH_TIME,sp_no_time_send_sms.getSelectedItem().toString());
        }
//        else
//        {
//            Toast.makeText(this,"Please Enter Some Value",Toast.LENGTH_SHORT).show();
//        }
    }

    private boolean validationNew() {
        if(sp_call_disconnect_time.getSelectedItem().toString().equalsIgnoreCase("Select")){
            Toast.makeText((BaseActivity)getActivity(),"Please select call duration time",Toast.LENGTH_SHORT).show();
           return false;
        }
            else if (sp_no_time_send_sms.getSelectedItem().toString().equalsIgnoreCase("Select"))
        {
            Toast.makeText((BaseActivity)getActivity(),"Please select number of sms send",Toast.LENGTH_SHORT).show();
            return false;
        }
        else
        {
            return true;
        }
    }

    private String getDate(long time) {
        Date date = new Date(time); // *1000 is to convert seconds to milliseconds
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH); // the format of your date
        return sdf.format(date);
    }

    private boolean validation() {
        if(edt_call_seconds.getText().toString().trim().length() == 0){
            edt_call_seconds.setError("Please Enter Call disconnect time");
            edt_call_seconds.requestFocus();
//            Toast.makeText(this,)
//            Utilities.showSnackBarToast(lv_main,"Please Enter Licence Key");
//            edt_licence_id.setError("Please Enter Licence Key");
//            Utili.requestFocus();
            return false;
        }
        else if (edt_send_numbers.getText().toString().trim().length() == 0)
        {
            edt_send_numbers.setError("Please Enter Number of SMS send to each number");
            edt_send_numbers.requestFocus();
//            Utilities.showSnackBarToast(lv_main,"Please Enter Licence Code");
//            edt_licence_code.setError("Please Enter Licence Code");
//            edt_licence_code.requestFocus();
            return false;
        }
        else
        {
            return true;
        }
    }



    public void hideKeyboard() {
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Subscribe
    public void onEventMainThread(ApiErrorEvent event) {
        switch (event.getRequestTag()) {
//            case LOGTAG:
//                Log.e(":::","Error"+event.getRequestTag());
//                dismissProgress();
//                break;
            default:
                break;
        }
    }


    @Subscribe
    public void onEventMainThread(ApiErrorWithMessageEvent event) {
        switch (event.getRequestTag()) {
//            case LOGTAG:
//                dismissProgress();
//                Log.e(":::",""+event.getResultMsgUser());
//                break;
            default:
                break;
        }
    }


}
