package com.totalsmsclonedemo.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.totalsmsclonedemo.R;
import com.totalsmsclonedemo.Utilities;
import com.totalsmsclonedemo.activities.BaseActivity;
import com.totalsmsclonedemo.databases.DBHelperForConfigurationDetails;
import com.totalsmsclonedemo.dbo.ProjectConfigurationDbo;
import com.totalsmsclonedemo.events.ApiErrorEvent;
import com.totalsmsclonedemo.events.ApiErrorWithMessageEvent;
import com.totalsmsclonedemo.services.CallDataStoreService;
import com.totalsmsclonedemo.services.SmsDataStoreService;

import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


public class DashboardFragment extends BaseFragment {

    View v;
    Unbinder unbinder;

    @BindView(R.id.lv_sms)
    LinearLayout lv_sms;

    @BindView(R.id.lv_call)
    LinearLayout lv_call;

    @BindView(R.id.btn_configuration)
    Button btn_configuration;

    public static final String CALL_LIST_TAG = "call_list_tag";

    DBHelperForConfigurationDetails dbHelperConfig;

    Context context;
    public static final String TAG = DashboardFragment.class.getSimpleName();



    @Override
    public void setToolbarForFragment() {
        ((BaseActivity) getActivity()).getToolbar().setVisibility(View.GONE);
        ((BaseActivity) getActivity()).getLl_toolbar().setVisibility(View.GONE);
        ((BaseActivity) getActivity()).getToolbartext().setVisibility(View.GONE);
    }

    @Override
    public void setTabLayout() {
        ((BaseActivity) getActivity()).getLl_view_tab().setVisibility(View.VISIBLE);
//        ((BaseActivity)getActivity()).setHomeSelected();
//        ((BaseActivity) getActivity()).getTabLayout().setVisibility(View.VISIBLE);
    }

    public DashboardFragment() {
        // Required empty public constructor
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v =  inflater.inflate(R.layout.fragment_dashboard, container, false);
        unbinder = ButterKnife.bind(this, v);
        dbHelperConfig = new DBHelperForConfigurationDetails((BaseActivity)getActivity());
        context = ((BaseActivity)getActivity());
//        if (Utilities.isNetworkAvailable(context)) {
//            if (!mApiClient.isRequestRunning(CALL_LIST_TAG)) {
////                showProgress();
//                mApiClient.get_call_list(CALL_LIST_TAG);
//            }
//        }
//        else
//        {
//            showMsg("No Internet Connection");
//        }

//        ((BaseActivity)getActivity()).callStoreServiceCall();
//        ((BaseActivity)getActivity()).smsStoreServiceCall();

        Intent i = new Intent(context, CallDataStoreService.class);
        context.startService(i);
           Intent i2 = new Intent(context, SmsDataStoreService.class);
        context.startService(i2);

        int configCount = dbHelperConfig.getConfigurationCount();
        Log.e(TAG, "Config Count is: " + configCount);
        if (configCount == 0) {

            Log.e(TAG,"Count is 0");
            String noTime= "1 Time";
            String disconnectTime= "1 Minute";
            dbHelperConfig.insertConfigurationDetails(new ProjectConfigurationDbo("1",disconnectTime,noTime));
//                if (selectedDisconnectTime != null) {
//            int spinnerPosition = adapterChooseCallDisconnectTime.getPosition(disconnectTime);
//            sp_call_disconnect_time.setSelection(spinnerPosition);
////                }
////                if (selectedSmsTimes != null) {
//            int spinnerPosition2 = adapterChooseNoSmsSend.getPosition(noTime);
//            sp_no_time_send_sms.setSelection(spinnerPosition2);
//                }

            Utilities.storeValue((BaseActivity)getActivity(),Utilities.DISCONNECT_DURATION_TIME,"1");
            Utilities.storeValue((BaseActivity)getActivity(),Utilities.NO_SMS_SEND_EACH_TIME,"1");

//            Utilities.storeValue((BaseActivity)getActivity(),Utilities.DISCONNECT_DURATION_TIME,"1");
//            Utilities.storeValue((BaseActivity)getActivity(),Utilities.NO_SMS_SEND_EACH_TIME,"1");
        }

        return v;
    }

    @OnClick(R.id.lv_call)
    public void onCallClicked(View v)
    {
        mainInterfaces.OpenCall();
    }

    @OnClick(R.id.lv_sms)
    public void onSMSClicked(View v)
    {
        mainInterfaces.OpenSms();
    }

    @OnClick(R.id.btn_configuration)
    public void onConfigurationClicked(View v)
    {
        mainInterfaces.OpenConfiguration();
    }

    @Subscribe
    public void onEventMainThread(ApiErrorEvent event) {
        switch (event.getRequestTag()) {
//            case LOGTAG:
//                Log.e(":::","Error"+event.getRequestTag());
//                dismissProgress();
//                break;
            default:
                break;
        }
    }


    @Subscribe
    public void onEventMainThread(ApiErrorWithMessageEvent event) {
        switch (event.getRequestTag()) {
//            case LOGTAG:
//                dismissProgress();
//                Log.e(":::",""+event.getResultMsgUser());
//                break;
            default:
                break;
        }
    }
}
