package com.totalsmsclonedemo.fragments;


import android.Manifest;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.totalsmsclonedemo.Model.ContactCallFinalList;
import com.totalsmsclonedemo.Model.ContactCallModel;
import com.totalsmsclonedemo.Model.ContactListCallSmsDboList;
import com.totalsmsclonedemo.PermissionResult;
import com.totalsmsclonedemo.PhoneListener;
import com.totalsmsclonedemo.R;
import com.totalsmsclonedemo.Utilities;
import com.totalsmsclonedemo.activities.BaseActivity;
import com.totalsmsclonedemo.adapters.ExpandableCallListAdapter;
import com.totalsmsclonedemo.adapters.MyExpandableCallListAdapter;
import com.totalsmsclonedemo.databases.DBHelperForCallCountryNameDetails;
import com.totalsmsclonedemo.databases.DBHelperForCallSummary;
import com.totalsmsclonedemo.databases.DBHelperForCallSummaryForPhoneTime;
import com.totalsmsclonedemo.databases.DBHelperForCallTierNameDetails;
import com.totalsmsclonedemo.databases.DBHelperForListForCall;
import com.totalsmsclonedemo.databases.DBHelperForListForCallAndSms;
import com.totalsmsclonedemo.dbo.CallAndSmsListDataDbo;
import com.totalsmsclonedemo.dbo.CallListDataDbo;
import com.totalsmsclonedemo.dbo.CallSummaryDbo;
import com.totalsmsclonedemo.dialog.CustomDialog;
import com.totalsmsclonedemo.events.ApiErrorEvent;
import com.totalsmsclonedemo.events.ApiErrorWithMessageEvent;
import com.totalsmsclonedemo.network.response.CallListResponse;
import com.totalsmsclonedemo.network.response.CallListResponseData;
import com.totalsmsclonedemo.recyclerview_selection.ContactItemDetails;
import com.totalsmsclonedemo.recyclerview_selection.ContactItemDetailsLookup;
import com.totalsmsclonedemo.recyclerview_selection.ContactItemKeyProvider;
import com.totalsmsclonedemo.services.MyPhoneListenerService;

import org.greenrobot.eventbus.Subscribe;

import java.lang.reflect.Method;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import androidx.recyclerview.selection.SelectionPredicates;
import androidx.recyclerview.selection.SelectionTracker;
import androidx.recyclerview.selection.StorageStrategy;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class CallFragment extends BaseFragment implements Switch.OnCheckedChangeListener, Spinner.OnItemSelectedListener, MyExpandableCallListAdapter.Add_remove_child {

    View v;
    Unbinder unbinder;

    RecyclerView rv_contact_list, rv_display;
    ArrayList<ContactCallModel> data;
    ArrayList<ContactCallModel> selectedData;
    TextView tvSelectionCount, tv_select_all;
    List<String> countryNames_all;
    Context context;
    CountDownTimer timer;
    ContactListAdapter contactListAdapter;
    boolean call_start = false;
    private static ExpandableCallListAdapter expandableCallListAdapter;
    Switch switch_select_all;
    private SelectionTracker mSelectionTracker;
    ArrayAdapter<String> adapterChooseSortBy, adapterChooseSortByTier;
    public static Integer pos;
    public static ArrayList<ContactCallFinalList> global_selected_child_list;

    public static ArrayList<CallListDataDbo> selected_child_list;
    int call_index = 0;

    public static ArrayList<CallListDataDbo> selected_phone_num;

    public static final String TAG = CallFragment.class.getSimpleName();

    DBHelperForCallSummaryForPhoneTime dbHelperForCallSummaryForPhoneTime;

    @BindView(R.id.sp_sort_country)
    Spinner sp_sort_country;

    int i=0;

    @BindView(R.id.floating_action_btn)
    FloatingActionButton floating_action_btn;

    public static final String CALL_LIST_TAG = "call_list_tag";

    @BindView(R.id.sp_sort_tier)
    Spinner sp_sort_tier;

    @BindView(R.id.el_contact_list_new)
    ExpandableListView el_contact_list_new;

    HashMap<String, List<ContactCallModel>> hashMap;

    DBHelperForListForCallAndSms dbHelper;
    DBHelperForListForCall dbHelperForListForCall;
    DBHelperForCallTierNameDetails dbHelperForCallTierNameDetails;
    DBHelperForCallCountryNameDetails dbHelperForCallCountryNameDetails;
    DBHelperForCallSummary dbHelperForCallSummary;

    ArrayList<ContactListCallSmsDboList> newData;
    MyExpandableCallListAdapter newExpandeddapter;

    ArrayList<ContactCallFinalList> finalData;
    ArrayList<String> finalHeaderData;


    ArrayList<ContactCallFinalList> global_data_list_country_wise;
    ArrayList<ContactCallFinalList> global_data_list_all_country;

    static CustomDialog dialog;

    public static String current_selected_country = "All Countries";
    public static String current_selected_tier = "All Tier";


    @Override
    public void setToolbarForFragment() {
        ((BaseActivity) getActivity()).getToolbar().setVisibility(View.VISIBLE);
        ((BaseActivity) getActivity()).getLl_toolbar().setVisibility(View.VISIBLE);
        ((BaseActivity) getActivity()).getToolbartext().setVisibility(View.VISIBLE);
        ((BaseActivity) getActivity()).getToolbartext().setText("Test Call");

        ((BaseActivity) getActivity()).getImgToolBarBack().setVisibility(View.VISIBLE);
        ((BaseActivity) getActivity()).getImgToolBarBack().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mainInterfaces.OpenDashBoard();
            }
        });
    }


    @Override
    public void setTabLayout() {
        ((BaseActivity) getActivity()).getLl_view_tab().setVisibility(View.VISIBLE);
//        ((BaseActivity) getActivity()).getTabLayout().setVisibility(View.VISIBLE);
    }

    public CallFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_call, container, false);
        context = (BaseActivity) getActivity();
        dbHelper = new DBHelperForListForCallAndSms((BaseActivity) getActivity());
        dbHelperForCallSummaryForPhoneTime = new DBHelperForCallSummaryForPhoneTime((BaseActivity) getActivity());
        dbHelperForCallSummary = new DBHelperForCallSummary((BaseActivity) getActivity());
        dbHelperForListForCall = new DBHelperForListForCall((BaseActivity) getActivity());
        dbHelperForCallTierNameDetails = new DBHelperForCallTierNameDetails((BaseActivity) getActivity());
        dbHelperForCallCountryNameDetails = new DBHelperForCallCountryNameDetails((BaseActivity) getActivity());
        unbinder = ButterKnife.bind(this, v);
//        initDialog();
        allowPermissions();

//        Intent i = new Intent(context, CallDataStoreService.class);
//        context.startService(i);
//        showProgress();
//        Intent i = new Intent(context, CallDataStoreService.class);
//        context.startService(i);
//        dismissProgress();
//        if (Utilities.isNetworkAvailable(context)) {
//            if (!mApiClient.isRequestRunning(CALL_LIST_TAG)) {
//                showProgress();
//                mApiClient.get_call_list(CALL_LIST_TAG);
//            }
//        }
//        else
//        {
//            showMsg("No Internet Connection");
//        }
        el_contact_list_new.setGroupIndicator(null);
        rv_contact_list = v.findViewById(R.id.rv_contact_list);
        tvSelectionCount = v.findViewById(R.id.tvSelectionCount);
        tv_select_all = v.findViewById(R.id.tv_select_all);
        switch_select_all = v.findViewById(R.id.switch_select_all);
        rv_display = v.findViewById(R.id.rv_display);
        switch_select_all.setEnabled(false);
//        expandableListView.setItemChecked(index,true)
//        if (mSelectionTracker.hasSelection())
//        {
        switch_select_all.setOnCheckedChangeListener(this);

        countryNames_all = dbHelperForCallCountryNameDetails.getAllCallCountryName();

        global_selected_child_list = new ArrayList<>();

        /*TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        PhoneStateListener phoneStateListener = new PhoneStateListener() {
            @Override
            public void onCallStateChanged(int state, String phoneNumber) {
                super.onCallStateChanged(state, phoneNumber);
                if (state == TelephonyManager.CALL_STATE_RINGING) {
                    if (phoneNumber == null) {
                        //outgoing call
//                        Toast.makeText(context.getApplicationContext(), "Phone Is Riging", Toast.LENGTH_LONG).show();
                    } else {
                        //incoming call
                    }
                }

                if (state == TelephonyManager.CALL_STATE_OFFHOOK) {
                    if (phoneNumber == null) {
                        //outgoing call
//                        Toast.makeText(context.getApplicationContext(), "Phone is Currently in A call", Toast.LENGTH_LONG).show();
                    } else {
                        //incoming call
                    }
                }

                if (state == TelephonyManager.CALL_STATE_IDLE) {
//                    Toast.makeText(context.getApplicationContext(), "phone is neither ringing nor in a call", Toast.LENGTH_LONG).show();
                }
            }
        };

        tm.listen(phoneStateListener, PhoneStateListener.LISTEN_CALL_STATE);

        OutgoingReceiver outgoingReceiver = new OutgoingReceiver();
        IntentFilter intentFilter = new IntentFilter(Intent.ACTION_NEW_OUTGOING_CALL);
        context.registerReceiver(outgoingReceiver, intentFilter);*/


//        }

        OutgoingReceiver outgoingReceiver = new OutgoingReceiver();
        IntentFilter intentFilter = new IntentFilter(Intent.ACTION_NEW_OUTGOING_CALL);
        context.registerReceiver(outgoingReceiver, intentFilter);
        setUpList();
        setUpSpinners();
        setClickListenerSpinner();


        selected_child_list = new ArrayList<>();

        floating_action_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Collect_phone_number();
                if (selected_phone_num != null && selected_phone_num.size() != 0) {
                    Log.e(TAG,"size is:"+selected_phone_num.size());
                    confirmCallDialog();
                } else {
                    Toast.makeText(context, "First select multiple contacts trough long press on Tier", Toast.LENGTH_SHORT).show();
                }
            }
        });
        try {
            mSelectionTracker = new SelectionTracker.Builder<>(
                    "string-items-selection",
                    rv_contact_list,
                    new ContactItemKeyProvider(1, data),
                    new ContactItemDetailsLookup(rv_contact_list),
                    StorageStrategy.createParcelableStorage(ContactCallModel.class)
            ).withSelectionPredicate(SelectionPredicates.<ContactCallModel>createSelectAnything())
                    .build();

            contactListAdapter.setSelectionTracker(mSelectionTracker);

            mSelectionTracker.addObserver(new SelectionTracker.SelectionObserver() {
                @Override
                public void onItemStateChanged(@NonNull Object key, boolean selected) {
                    super.onItemStateChanged(key, selected);
//                Log.e("ItemStateChanged %s to %b", key, selected);
                }

                @Override
                public void onSelectionRefresh() {
                    super.onSelectionRefresh();
                    tvSelectionCount.setText("Selection Count: 0");
                }

                @Override
                public void onSelectionChanged() {
                    super.onSelectionChanged();
                    switch_select_all.setEnabled(true);
//                    mSelectionTracker.select()
                    if (mSelectionTracker.hasSelection()) {
                        Log.e("activity", "selected Element: " + mSelectionTracker.getSelection().toString());
                        tvSelectionCount.setText(String.format("Selection Count: %d", mSelectionTracker.getSelection().size()));
                    } else {
                        tvSelectionCount.setText("Selection Count: 0");
                    }

                    Iterator<ContactCallModel> itemIterable = mSelectionTracker.getSelection().iterator();
                    List<ContactCallModel> mySelectedList = copyIterator(itemIterable);
                    Log.e("activity", "Selected Arraylist: " + mySelectedList.size());
                    while (itemIterable.hasNext()) {
                        try {
                            Log.e("activity", itemIterable.next().getContactNumber());
                            Log.e("activity", itemIterable.next().getContactCountry());
                        } catch (Exception e) {

                        }
                    }

                }

                @Override
                public void onSelectionRestored() {
                    super.onSelectionRestored();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

        tv_select_all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectAll();
            }
        });

        return v;
    }

    private void Collect_phone_number() {
        selected_phone_num = new ArrayList<>();
        if (global_data_list_country_wise.size() != 0) {
            for (int i = 0; i < global_data_list_country_wise.size(); i++) {
                ContactCallFinalList contactCallFinalList = global_data_list_country_wise.get(i);
                if (contactCallFinalList.getTier_list().size() != 0) {
                    for (int j = 0; j < contactCallFinalList.getTier_list().size(); j++) {
                        ContactCallFinalList contactCallFinalList1 = contactCallFinalList.getTier_list().get(j);
                        if (contactCallFinalList1.getChildList().size() != 0) {
                            for (int k = 0; k < contactCallFinalList1.getChildList().size(); k++) {
                                CallListDataDbo callListDataDbo = contactCallFinalList1.getChildList().get(k);
                                if (callListDataDbo.isSelected()) {
//                                    String image, String phoneNumber, String country, String tier, boolean selected
                                    selected_phone_num.add(new CallListDataDbo(callListDataDbo.getImage(),callListDataDbo.getPhoneNumber(),callListDataDbo.getCountry(),callListDataDbo.getTier()));
                                }
                            }
                        }
                    }
                }
            }
        }
    }


    private void setClickListenerSpinner() {
        sp_sort_country.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                try {
                    filterData(sp_sort_country.getSelectedItem().toString(), sp_sort_tier.getSelectedItem().toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        sp_sort_tier.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                try {
                    filterData(sp_sort_country.getSelectedItem().toString(), sp_sort_tier.getSelectedItem().toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
//                if (!sp_sort_tier.getSelectedItem().toString().equalsIgnoreCase("All Tier") && sp_sort_tier.getSelectedItem().toString().equalsIgnoreCase("Tier1"))
//                {
////                    ArrayList<CallAndSmsListDataDbo> newfirstTier;
////                    newfirstTier = new ArrayList<>();
////                    newfirstTier.add(new CallAndSmsListDataDbo(R.drawable.f1,"+91 7043169995","India","Tier1"));
////                    newfirstTier.add(new CallAndSmsListDataDbo(R.drawable.f2,"+91 8154936235","India","Tier1"));
////                    newfirstTier.add(new CallAndSmsListDataDbo(R.drawable.f3,"+91 8160058122","America","Tier1"));
////                    newfirstTier.add(new CallAndSmsListDataDbo(R.drawable.f4,"+91 8154936235","Ukraine","Tier1"));
////                    newfirstTier.add(new CallAndSmsListDataDbo(R.drawable.f5,"+91 8160058122","USA","Tier1"));
////
////                    newData = new ArrayList<>();
////                    newData.add(new ContactListCallSmsDboList("Tier1",newfirstTier));
////
////                    newExpandeddapter = new MyExpandableCallListAdapter(context,newData);
////
////                    el_contact_list_new.setAdapter(newExpandeddapter);
////                    expandAll();
//                }
//                else if (!sp_sort_tier.getSelectedItem().toString().equalsIgnoreCase("All Tier") && sp_sort_tier.getSelectedItem().toString().equalsIgnoreCase("Tier2"))
//                {
////                    ArrayList<CallAndSmsListDataDbo> newSecondTier;
////                    newSecondTier = new ArrayList<>();
////                    newSecondTier.add(new CallAndSmsListDataDbo(R.drawable.f1,"+91 7043169995","India","Tier2"));
////                    newSecondTier.add(new CallAndSmsListDataDbo(R.drawable.f2,"+91 8154936235","India","Tier2"));
////                    newSecondTier.add(new CallAndSmsListDataDbo(R.drawable.f3,"+91 8160058122","America","Tier2"));
////                    newSecondTier.add(new CallAndSmsListDataDbo(R.drawable.f4,"+91 8154936235","Ukraine","Tier2"));
////                    newSecondTier.add(new CallAndSmsListDataDbo(R.drawable.f5,"+91 8160058122","USA","Tier2"));
////
////                    newData = new ArrayList<>();
////                    newData.add(new ContactListCallSmsDboList("Tier2",newSecondTier));
////
////                    newExpandeddapter = new MyExpandableCallListAdapter(context,newData);
////
////                    el_contact_list_new.setAdapter(newExpandeddapter);
////                    expandAll();
//                }
//                else if  (!sp_sort_tier.getSelectedItem().toString().equalsIgnoreCase("All Tier") && sp_sort_tier.getSelectedItem().toString().equalsIgnoreCase("Tier3"))
//                {
////                    ArrayList<CallAndSmsListDataDbo> newThirdTier;
////                    newThirdTier = new ArrayList<>();
////                    newThirdTier.add(new CallAndSmsListDataDbo(R.drawable.f1,"+91 7043169995","India","Tier3"));
////                    newThirdTier.add(new CallAndSmsListDataDbo(R.drawable.f2,"+91 8154936235","India","Tier3"));
////                    newThirdTier.add(new CallAndSmsListDataDbo(R.drawable.f3,"+91 8160058122","America","Tier3"));
////                    newThirdTier.add(new CallAndSmsListDataDbo(R.drawable.f4,"+91 8154936235","Ukraine","Tier3"));
////                    newThirdTier.add(new CallAndSmsListDataDbo(R.drawable.f5,"+91 8160058122","USA","Tier3"));
////
////                    newData = new ArrayList<>();
////                    newData.add(new ContactListCallSmsDboList("Tier3",newThirdTier));
////
////                    newExpandeddapter = new MyExpandableCallListAdapter(context,newData);
////
////                    el_contact_list_new.setAdapter(newExpandeddapter);
////                    expandAll();
//                }
//                else if  (!sp_sort_tier.getSelectedItem().toString().equalsIgnoreCase("All Tier") && sp_sort_tier.getSelectedItem().toString().equalsIgnoreCase("Tier4")) {
////                    ArrayList<CallAndSmsListDataDbo> newFourTier;
////                    newFourTier = new ArrayList<>();
////                    newFourTier.add(new CallAndSmsListDataDbo(R.drawable.f1, "+91 7043169995", "India", "Tier4"));
////                    newFourTier.add(new CallAndSmsListDataDbo(R.drawable.f2, "+91 8154936235", "India", "Tier4"));
////                    newFourTier.add(new CallAndSmsListDataDbo(R.drawable.f3, "+91 8160058122", "America", "Tier4"));
////                    newFourTier.add(new CallAndSmsListDataDbo(R.drawable.f4, "+91 8154936235", "Ukraine", "Tier4"));
////                    newFourTier.add(new CallAndSmsListDataDbo(R.drawable.f5, "+91 8160058122", "USA", "Tier4"));
////
////                    newData = new ArrayList<>();
////                    newData.add(new ContactListCallSmsDboList("Tier4",newFourTier));
////
////                    newExpandeddapter = new MyExpandableCallListAdapter(context,newData);
////
////                    el_contact_list_new.setAdapter(newExpandeddapter);
////                    expandAll();
//                }
//                if (!sp_sort_tier.getSelectedItem().toString().equalsIgnoreCase("All Tier"))
//                {
//                    finalData = new ArrayList<>();
//                    finalData.add(new ContactCallFinalList(sp_sort_tier.getSelectedItem().toString(),dbHelperForListForCall.getCallDataListByTier(sp_sort_tier.getSelectedItem().toString())));
////
//                    newExpandeddapter = new MyExpandableCallListAdapter(context,finalData,el_contact_list_new);
//
//                    el_contact_list_new.setAdapter(newExpandeddapter);
//                    expandAll();
//                }
//                else
//                {
//                    setUpList();
//                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    private void filterData(String countryData, String tierData) {
        current_selected_country = countryData;
        current_selected_tier = tierData;
        Set_filtered_data(countryData, tierData);

       /* try {
            if (countryData.equalsIgnoreCase("All Countries") && tierData.equalsIgnoreCase("All Tier")) {
                setUpList();
            } else if (!(countryData.equalsIgnoreCase("All Countries")) && tierData.equalsIgnoreCase("All Tier")) {
                setUpList();
//                filter(sp_sort_country.getSelectedItem().toString());
                newExpandeddapter.filterData(sp_sort_country.getSelectedItem().toString());
                expandAll();
            } else if (!(countryData.equalsIgnoreCase("All Countries")) && !(tierData.equalsIgnoreCase("All Tier"))) {
                finalData = new ArrayList<>();
                finalData.add(new ContactCallFinalList(sp_sort_tier.getSelectedItem().toString(), dbHelperForListForCall.getCallDataListByTier(sp_sort_tier.getSelectedItem().toString())));
//
                pos = null;
                newExpandeddapter = new MyExpandableCallListAdapter(context, finalData, el_contact_list_new, CallFragment.this);
//                filter(sp_sort_country.getSelectedItem().toString());
                newExpandeddapter.filterData(sp_sort_country.getSelectedItem().toString());
                el_contact_list_new.setAdapter(newExpandeddapter);

                expandAll();
            } else if ((countryData.equalsIgnoreCase("All Countries")) && !(tierData.equalsIgnoreCase("All Tier"))) {
                finalData = new ArrayList<>();
                finalData.add(new ContactCallFinalList(sp_sort_tier.getSelectedItem().toString(), dbHelperForListForCall.getCallDataListByTier(sp_sort_tier.getSelectedItem().toString())));
                pos = null;
                newExpandeddapter = new MyExpandableCallListAdapter(context, finalData, el_contact_list_new, CallFragment.this);
                el_contact_list_new.setAdapter(newExpandeddapter);
//                filter(sp_sort_country.getSelectedItem().toString());
//                newExpandeddapter.filterData(sp_sort_country.getSelectedItem().toString());
                expandAll();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }*/
    }

    private void Set_filtered_data(String countryData, String tierData) {
        if (countryData.equals("All Countries") && tierData.equals("All Tier")) {
            el_contact_list_new.setAdapter(new MyExpandableCallListAdapter(context, global_data_list_all_country.get(0).getTier_list(), el_contact_list_new, CallFragment.this));
        } else {
            ArrayList<ContactCallFinalList> temp_data = new ArrayList<>();

            if (countryData.equals("All Countries") && !tierData.equals("All Tier")) {
                if (global_data_list_all_country.size() != 0) {
                    for (int i = 0; i < global_data_list_all_country.size(); i++) {
                        ContactCallFinalList contactCallFinalList = global_data_list_all_country.get(i);
                        if (contactCallFinalList.getTier_list().size() != 0) {
                            for (int j = 0; j < contactCallFinalList.getTier_list().size(); j++) {
                                ContactCallFinalList contactCallFinalList1 = contactCallFinalList.getTier_list().get(j);
                                if (contactCallFinalList1.getName().equals(tierData)) {
                                    temp_data.add(contactCallFinalList1);
                                    break;
                                }
                            }
                        }
                    }
                }
            }

            if (!countryData.equals("All Countries") && tierData.equals("All Tier")) {
                if (global_data_list_country_wise.size() != 0) {
                    for (int i = 0; i < global_data_list_country_wise.size(); i++) {
                        ContactCallFinalList contactCallFinalList = global_data_list_country_wise.get(i);
                        if (contactCallFinalList.getCountry_name().equals(countryData)) {
                            temp_data.addAll(contactCallFinalList.getTier_list());
                        }
                    }
                }
            }

            if (!countryData.equals("All Countries") && !tierData.equals("All Tier")) {
                if (global_data_list_country_wise.size() != 0) {
                    for (int i = 0; i < global_data_list_country_wise.size(); i++) {
                        ContactCallFinalList contactCallFinalList = global_data_list_country_wise.get(i);
                        if (contactCallFinalList.getCountry_name().equals(countryData)) {
                            if (contactCallFinalList.getTier_list().size() != 0) {
                                for (int j = 0; j < contactCallFinalList.getTier_list().size(); j++) {
                                    ContactCallFinalList contactCallFinalList1 = contactCallFinalList.getTier_list().get(j);
                                    if (contactCallFinalList1.getName().equals(tierData)) {
                                        temp_data.add(contactCallFinalList1);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }


            if (temp_data.size() != 0) {
                newExpandeddapter = new MyExpandableCallListAdapter(context, temp_data, el_contact_list_new, CallFragment.this);
//                newExpandeddapter.filterData(sp_sort_country.getSelectedItem().toString());
                el_contact_list_new.setAdapter(newExpandeddapter);
            } else {
                temp_data = new ArrayList<>();
                el_contact_list_new.setAdapter(new MyExpandableCallListAdapter(context, temp_data, el_contact_list_new, CallFragment.this));
            }
//            expandAll();
        }
    }

    private void setUpSpinners() {
        try {
            List<String> countryNames = new ArrayList<>();
            countryNames = dbHelperForCallCountryNameDetails.getAllCallCountryName();
            List<String> spinnerChooseSortBy = new ArrayList<>();
            spinnerChooseSortBy.add("All Countries");
            spinnerChooseSortBy.addAll(countryNames);

            adapterChooseSortBy = new ArrayAdapter<String>((BaseActivity) getActivity(), android.R.layout.simple_spinner_item, spinnerChooseSortBy);

            adapterChooseSortBy.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//       Spinner sItems = (Spinner) findViewById(R.id.spinner1);
            sp_sort_country.setAdapter(adapterChooseSortBy);

//        sp_sort_country.setOnItemSelectedListener(this);

//        List<String> tierNames=new ArrayList<>();
//        tierNames = dbHelperForCallTierNameDetails.getAllCallTierName();
            Log.e(TAG, "size of database tier list: " + finalHeaderData.size());
            List<String> spinnerChooseSortByTier = new ArrayList<>();
            spinnerChooseSortByTier.add("All Tier");
//        spinnerChooseSortByTier = dbHelperForCallTierNameDetails.getAllCallTierName();
//        for (int j=0;j < finalHeaderData.size(); j++)
//        {
            spinnerChooseSortByTier.addAll(finalHeaderData);
//        }
            Log.e(TAG, "size of tier list: " + spinnerChooseSortByTier.size());

            adapterChooseSortByTier = new ArrayAdapter<String>((BaseActivity) getActivity(), android.R.layout.simple_spinner_item, spinnerChooseSortByTier);

            adapterChooseSortByTier.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//       Spinner sItems = (Spinner) findViewById(R.id.spinner1);
            sp_sort_tier.setAdapter(adapterChooseSortByTier);
//        sp_sort_tier.setOnItemSelectedListener(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void Set_all_data_of_tiers(ArrayList<ContactCallFinalList> finalData) {
        if (finalData.size() != 0) {
            global_data_list_country_wise = new ArrayList<>();
            global_data_list_all_country = new ArrayList<>();
            for (int i = 0; i < finalData.size(); i++) {
                ContactCallFinalList contactCallFinalList = finalData.get(i);
                if (contactCallFinalList.getChildList().size() != 0) {
                    for (int j = 0; j < contactCallFinalList.getChildList().size(); j++) {
                        CallListDataDbo child_object = contactCallFinalList.getChildList().get(j);
                        Add_to_list(child_object);
                        Add_to_all_data_list(child_object);
                    }
                }
            }
        }
    }

    private void Add_to_all_data_list(CallListDataDbo child_object) {
        if (global_data_list_all_country.size() != 0) {
            for (int i = 0; i < global_data_list_all_country.size(); i++) {
                ContactCallFinalList contactCallFinalList = global_data_list_all_country.get(i);
                if (contactCallFinalList.getTier_list().size() != 0) {
                    boolean match_tier = false;
                    for (int j = 0; j < contactCallFinalList.getTier_list().size(); j++) {
                        ContactCallFinalList contactCallFinalList1 = contactCallFinalList.getTier_list().get(j);
                        if (contactCallFinalList1.getName().equals(child_object.getTier())) {
                            if (contactCallFinalList1.getChildList().size() != 0) {
                                boolean match_child_objejct = false;
                                for (int k = 0; k < contactCallFinalList1.getChildList().size(); k++) {
                                    CallListDataDbo callListDataDbo = contactCallFinalList1.getChildList().get(k);
                                    if (callListDataDbo.getPhoneNumber().equals(child_object.getPhoneNumber())) {
                                        match_child_objejct = true;
                                        break;
                                    }
                                }
                                if (!match_child_objejct) {
                                    child_object.setSelected(false);
                                    contactCallFinalList1.getChildList().add(child_object);
                                }
                            }
                            match_tier = true;
                            break;
                        }
                    }
                    if (!match_tier) {
                        ArrayList<CallListDataDbo> childList = new ArrayList<>();
                        child_object.setSelected(false);
                        childList.add(child_object);
                        ContactCallFinalList contactCallFinalList1 = new ContactCallFinalList(child_object.getTier(), childList, false);
                        contactCallFinalList.getTier_list().add(contactCallFinalList1);
                    }
                }
            }
        } else {
            ArrayList<CallListDataDbo> childList = new ArrayList<>();
            child_object.setSelected(false);
            childList.add(child_object);
            ContactCallFinalList contactCallFinalList = new ContactCallFinalList(child_object.getTier(), childList, false);
            ArrayList<ContactCallFinalList> tier_list = new ArrayList<>();
            tier_list.add(contactCallFinalList);
            global_data_list_all_country.add(new ContactCallFinalList(tier_list, "All Countries"));
        }
    }

    private void Add_to_list(CallListDataDbo child_object) {
        if (global_data_list_country_wise.size() != 0) {
            boolean match_country = false;
            for (int i = 0; i < global_data_list_country_wise.size(); i++) {
                ContactCallFinalList contactCallFinalList = global_data_list_country_wise.get(i);
                if (contactCallFinalList.getCountry_name().equals(child_object.getCountry())) {
                    if (contactCallFinalList.getTier_list().size() != 0) {
                        boolean match_tier = false;
                        for (int j = 0; j < contactCallFinalList.getTier_list().size(); j++) {
                            ContactCallFinalList contactCallFinalList1 = contactCallFinalList.getTier_list().get(j);
                            if (contactCallFinalList1.getName().equals(child_object.getTier())) {
                                if (contactCallFinalList1.getChildList().size() != 0) {
                                    boolean match_child_objejct = false;
                                    for (int k = 0; k < contactCallFinalList1.getChildList().size(); k++) {
                                        CallListDataDbo callListDataDbo = contactCallFinalList1.getChildList().get(k);
                                        if (callListDataDbo.getPhoneNumber().equals(child_object.getPhoneNumber())) {
                                            match_child_objejct = true;
                                            break;
                                        }
                                    }
                                    if (!match_child_objejct) {
                                        child_object.setSelected(false);
                                        contactCallFinalList1.getChildList().add(child_object);
                                    }
                                }
                                match_tier = true;
                                break;
                            }
                        }
                        if (!match_tier) {
                            ArrayList<CallListDataDbo> childList = new ArrayList<>();
                            child_object.setSelected(false);
                            childList.add(child_object);
                            ContactCallFinalList contactCallFinalList1 = new ContactCallFinalList(child_object.getTier(), childList, false);
                            contactCallFinalList.getTier_list().add(contactCallFinalList1);
                        }
                    }
                    match_country = true;
                    break;
                }
            }
            if (!match_country) {
                ArrayList<CallListDataDbo> childList = new ArrayList<>();
                child_object.setSelected(false);
                childList.add(child_object);
                ContactCallFinalList contactCallFinalList = new ContactCallFinalList(child_object.getTier(), childList, false);
                ArrayList<ContactCallFinalList> tier_list = new ArrayList<>();
                tier_list.add(contactCallFinalList);
                global_data_list_country_wise.add(new ContactCallFinalList(tier_list, child_object.getCountry()));
            }
        } else {
            ArrayList<CallListDataDbo> childList = new ArrayList<>();
            child_object.setSelected(false);
            childList.add(child_object);
            ContactCallFinalList contactCallFinalList = new ContactCallFinalList(child_object.getTier(), childList, false);
            ArrayList<ContactCallFinalList> tier_list = new ArrayList<>();
            tier_list.add(contactCallFinalList);
            global_data_list_country_wise.add(new ContactCallFinalList(tier_list, child_object.getCountry()));
        }
    }

    public static <
            ContactModel> List<ContactModel> copyIterator(Iterator<ContactModel> iter) {
        List<ContactModel> copy = new ArrayList<ContactModel>();
        while (iter.hasNext())
            copy.add(iter.next());
        return copy;
    }

    private void setUpList() {
        rv_contact_list.setLayoutManager(new LinearLayoutManager(context));
//        rv_contact_list_new.setLayoutManager(new LinearLayoutManager(context));

        hashMap = new HashMap<String, List<ContactCallModel>>();


        final List<String> header;
        header = new ArrayList<>();
        header.add("Tier1");
        header.add("Tier2");
        header.add("Tier3");
        header.add("Tier4");

        List<ContactCallModel> firstTier;
        firstTier = new ArrayList<>();
        firstTier.add(new ContactCallModel(1, R.drawable.f1, "+91 7043169995", "India", "Tier1"));
        firstTier.add(new ContactCallModel(2, R.drawable.f2, "+91 8154936235", "India", "Tier1"));
        firstTier.add(new ContactCallModel(3, R.drawable.f3, "+91 8160058122", "America", "Tier1"));
        firstTier.add(new ContactCallModel(4, R.drawable.f4, "+91 8154936235", "Ukraine", "Tier1"));
        firstTier.add(new ContactCallModel(5, R.drawable.f5, "+91 8160058122", "USA", "Tier1"));

        List<ContactCallModel> secondTier;
        secondTier = new ArrayList<>();
        secondTier.add(new ContactCallModel(6, R.drawable.f1, "+91 8154936235", "India", "Tier2"));
        secondTier.add(new ContactCallModel(7, R.drawable.f2, "+91 7043169995", "India", "Tier2"));
        secondTier.add(new ContactCallModel(8, R.drawable.f3, "+91 8154936235", "Ukraine", "Tier2"));
        secondTier.add(new ContactCallModel(9, R.drawable.f4, "+91 8160058122", "USA", "Tier2"));
        secondTier.add(new ContactCallModel(10, R.drawable.f5, "+91 8154936235", "India", "Tier2"));

        ArrayList<CallAndSmsListDataDbo> newfirstTier;
        newfirstTier = new ArrayList<>();
        newfirstTier.add(new CallAndSmsListDataDbo(R.drawable.f1, "+91 7043169995", "India", "Tier1"));
        newfirstTier.add(new CallAndSmsListDataDbo(R.drawable.f2, "+91 8154936235", "India", "Tier1"));
        newfirstTier.add(new CallAndSmsListDataDbo(R.drawable.f3, "+91 8160058122", "America", "Tier1"));
        newfirstTier.add(new CallAndSmsListDataDbo(R.drawable.f4, "+91 8154936235", "Ukraine", "Tier1"));
        newfirstTier.add(new CallAndSmsListDataDbo(R.drawable.f5, "+91 8160058122", "USA", "Tier1"));

        ArrayList<CallAndSmsListDataDbo> newSecondTier;
        newSecondTier = new ArrayList<>();
        newSecondTier.add(new CallAndSmsListDataDbo(R.drawable.f1, "+91 7043169995", "India", "Tier2"));
        newSecondTier.add(new CallAndSmsListDataDbo(R.drawable.f2, "+91 8154936235", "India", "Tier2"));
        newSecondTier.add(new CallAndSmsListDataDbo(R.drawable.f3, "+91 8160058122", "America", "Tier2"));
        newSecondTier.add(new CallAndSmsListDataDbo(R.drawable.f4, "+91 8154936235", "Ukraine", "Tier2"));
        newSecondTier.add(new CallAndSmsListDataDbo(R.drawable.f5, "+91 8160058122", "USA", "Tier2"));

        ArrayList<CallAndSmsListDataDbo> newThirdTier;
        newThirdTier = new ArrayList<>();
        newThirdTier.add(new CallAndSmsListDataDbo(R.drawable.f1, "+91 7043169995", "India", "Tier3"));
        newThirdTier.add(new CallAndSmsListDataDbo(R.drawable.f2, "+91 8154936235", "India", "Tier3"));
        newThirdTier.add(new CallAndSmsListDataDbo(R.drawable.f3, "+91 8160058122", "America", "Tier3"));
        newThirdTier.add(new CallAndSmsListDataDbo(R.drawable.f4, "+91 8154936235", "Ukraine", "Tier3"));
        newThirdTier.add(new CallAndSmsListDataDbo(R.drawable.f5, "+91 8160058122", "USA", "Tier3"));


        ArrayList<CallAndSmsListDataDbo> newFourTier;
        newFourTier = new ArrayList<>();
        newFourTier.add(new CallAndSmsListDataDbo(R.drawable.f1, "+91 7043169995", "India", "Tier4"));
        newFourTier.add(new CallAndSmsListDataDbo(R.drawable.f2, "+91 8154936235", "India", "Tier4"));
        newFourTier.add(new CallAndSmsListDataDbo(R.drawable.f3, "+91 8160058122", "America", "Tier4"));
        newFourTier.add(new CallAndSmsListDataDbo(R.drawable.f4, "+91 8154936235", "Ukraine", "Tier4"));
        newFourTier.add(new CallAndSmsListDataDbo(R.drawable.f5, "+91 8160058122", "USA", "Tier4"));

        newData = new ArrayList<>();
        newData.add(new ContactListCallSmsDboList("Tier1", newfirstTier));
        newData.add(new ContactListCallSmsDboList("Tier2", newSecondTier));
        newData.add(new ContactListCallSmsDboList("Tier3", newThirdTier));
        newData.add(new ContactListCallSmsDboList("Tier4", newFourTier));


        List<String> headerList;
        headerList = new ArrayList<>();
        headerList.add("Tier1");
        headerList.add("Tier2");
        headerList.add("Tier3");
        headerList.add("Tier4");

        List<CallAndSmsListDataDbo> dbData = new ArrayList<>();
        dbData.add(new CallAndSmsListDataDbo(R.drawable.f1, "+91 7043169995", "India", "Tier1"));
        dbData.add(new CallAndSmsListDataDbo(R.drawable.f2, "+91 8154936235", "India", "Tier1"));
        dbData.add(new CallAndSmsListDataDbo(R.drawable.f3, "+91 8160058122", "America", "Tier1"));
        dbData.add(new CallAndSmsListDataDbo(R.drawable.f4, "+91 8154936235", "Ukraine", "Tier1"));
        dbData.add(new CallAndSmsListDataDbo(R.drawable.f5, "+91 8160058122", "USA", "Tier1"));
        dbData.add(new CallAndSmsListDataDbo(R.drawable.f1, "+91 8154936235", "India", "Tier2"));
        dbData.add(new CallAndSmsListDataDbo(R.drawable.f2, "+91 7043169995", "India", "Tier2"));
        dbData.add(new CallAndSmsListDataDbo(R.drawable.f3, "+91 8154936235", "Ukraine", "Tier2"));
        dbData.add(new CallAndSmsListDataDbo(R.drawable.f4, "+91 8160058122", "USA", "Tier2"));
        dbData.add(new CallAndSmsListDataDbo(R.drawable.f5, "+91 8154936235", "India", "Tier2"));
        dbData.add(new CallAndSmsListDataDbo(R.drawable.f1, "+91 7043169995", "India", "Tier3"));
        dbData.add(new CallAndSmsListDataDbo(R.drawable.f2, "+91 8154936235", "India", "Tier3"));
        dbData.add(new CallAndSmsListDataDbo(R.drawable.f3, "+91 8160058122", "America", "Tier3"));
        dbData.add(new CallAndSmsListDataDbo(R.drawable.f1, "+91 7043169995", "India", "Tier4"));
        dbData.add(new CallAndSmsListDataDbo(R.drawable.f2, "+91 8154936235", "India", "Tier4"));
        dbData.add(new CallAndSmsListDataDbo(R.drawable.f3, "+91 8160058122", "America", "Tier4"));


        for (int i = 0; i < dbData.size(); i++) {
            dbHelper.insertCallSmsListDetails(dbData.get(i));
        }
//        newData = new ArrayList<>();
//        for (int j=0; j < headerList.size();j++)
//        {
//            newData.add(new ContactListCallSmsDboList(headerList.get(j).toString(),dbHelper.getCallSmsDataListByTier(headerList.get(j))));
//        }


//        newExpandeddapter = new MyExpandableCallListAdapter(context,newData);
//
//        el_contact_list_new.setAdapter(newExpandeddapter);

        hashMap.put("Tier1", firstTier);
        hashMap.put("Tier2", secondTier);
        hashMap.put("Tier3", firstTier);
        hashMap.put("Tier4", secondTier);
//        hashMap.put(header.get(1), child2);
//        hashMap.put(header.get(2), child3);
//        hashMap.put(header.get(3), child4);

//        expandableCallListAdapter = new ExpandableCallListAdapter(context, header, hashMap);
//        // Setting adpater for expandablelistview
//        el_contact_list_new.setAdapter(expandableCallListAdapter);
//
//          /*
//        You can add listeners for the item clicks
//         */
//        el_contact_list_new.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
//
//            @Override
//            public boolean onGroupClick(ExpandableListView parent, View v,
//                                        int groupPosition, long id) {
//                return false;
//            }
//        });
//
//        // Listview Group expanded listener
//        el_contact_list_new.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
//
//            @Override
//            public void onGroupExpand(int groupPosition) {
////                Toast.makeText(context,
////                        header.get(groupPosition) + " Expanded",
////                        Toast.LENGTH_SHORT).show();
//            }
//        });
//
//        // Listview Group collasped listener
//        el_contact_list_new.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {
//
//            @Override
//            public void onGroupCollapse(int groupPosition) {
////                Toast.makeText(context,
////                        header.get(groupPosition) + " Collapsed",
////                        Toast.LENGTH_SHORT).show();
//
//            }
//        });
//
//        // Listview on child click listener
//        el_contact_list_new.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
//
//            @Override
//            public boolean onChildClick(ExpandableListView parent, View v,
//                                        int groupPosition, int childPosition, long id) {
//
////                Toast.makeText(
////                        context,
////                        header.get(groupPosition)
////                                + " : "
////                                + hashMap.get(
////                                header.get(groupPosition)).get(
////                                childPosition), Toast.LENGTH_SHORT)
////                        .show();
//                return false;
//            }
//        });

        data = new ArrayList<ContactCallModel>();
        data.add(new ContactCallModel(1, "+91 7043169995", "India"));
        data.add(new ContactCallModel(2, "+91 8154936235", "India"));
        data.add(new ContactCallModel(3, "+91 8160058122", "America"));
        data.add(new ContactCallModel(4, "+91 8154936235", "Ukraine"));
        data.add(new ContactCallModel(5, "+91 8160058122", "USA"));
        data.add(new ContactCallModel(6, "+91 8154936235", "India"));
        data.add(new ContactCallModel(7, "+91 7043169995", "India"));
        data.add(new ContactCallModel(8, "+91 8154936235", "Ukraine"));
        data.add(new ContactCallModel(9, "+91 8160058122", "USA"));
        data.add(new ContactCallModel(10, "+91 8154936235", "India"));

        contactListAdapter = new ContactListAdapter(context, data);

        Log.e("Before Sorting:", data.get(0).getContactNumber());

        rv_contact_list.setAdapter(contactListAdapter);

        int count = dbHelperForListForCall.getCallCount();
        Log.d(TAG, "Call list count " + dbHelperForListForCall.getCallCount());

        if (count > 0) {
            finalData = new ArrayList<>();
            finalHeaderData = new ArrayList<>();
            finalHeaderData = dbHelperForCallTierNameDetails.getAllCallTierName();

            for (int i = 0; i < finalHeaderData.size(); i++) {
                finalData.add(new ContactCallFinalList(finalHeaderData.get(i), dbHelperForListForCall.getCallDataListByTier(finalHeaderData.get(i)), false));
            }
            Log.e(TAG, "final data size: " + finalData.size());
            pos = null;
            newExpandeddapter = new MyExpandableCallListAdapter(context, finalData, el_contact_list_new, CallFragment.this);
            el_contact_list_new.setAdapter(newExpandeddapter);

            Set_all_data_of_tiers(finalData);
        } else {

        }

//        el_contact_list_new
//                .setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
//
//                    public void onGroupExpand(int groupPosition) {
//
//                        if(groupPosition!=1){
//                            expandableIsThere = false;
//                        }
//
//                        for (int i = 0; i < len; i++) {
//                            if (i != groupPosition) {
//                                expandableListDetailsLevel.collapseGroup(i);
//                            }
//                        }
//                    }
//                });

        el_contact_list_new.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView expandableListView, View view, int groupPosition, int childPosition, long l) {
//                int index = view.getFlatListPosition(ExpandableListView.getPackedPositionForChild(groupPosition, childPosition));
//                view.setItemChecked(index, true);
//                int index = parent.getFlatListPosition(ExpandableListView.getPackedPositionForChild(groupPosition, childPosition));
//                parent.setItemChecked(index, true);
                view.setSelected(true);
                return false;
            }
        });


        el_contact_list_new.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView expandableListView, View view, int groupPosition, long l) {
                if (expandableListView.isGroupExpanded(groupPosition)) {
                    expandableListView.collapseGroup(groupPosition);
                } else {
                    expandableListView.expandGroup(groupPosition);
                }
                return false;
            }
        });

        el_contact_list_new.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                if (pos != null) {
                    if (pos == position) {
                        pos = null;
                        Toast.makeText(context, "All contacts are unselect of this Tier", Toast.LENGTH_SHORT).show();
                    } else {
                        pos = position;
                        Toast.makeText(context, "All contacts are select of this Tier", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    pos = position;
                    Toast.makeText(context, "All contacts are select of this Tier", Toast.LENGTH_SHORT).show();
                }
                return false;
            }
        });
//        Collections.sort(callModelList, new Comparator<CallModel>() {
//            public int compare(CallModel v1, CallModel v2) {
//                return v1.getCountry().compareTo(v2.getCountry());
//            }
//        });
//
//        Log.e("Sorting:",callModelList.get(0).getCountry());


//        callModelList.sort();


    }

    private void selectAll() {
        if (mSelectionTracker.hasSelection()) {
            mSelectionTracker.setItemsSelected(data, true);
        }
    }

    private void unSelectAll() {
        if (mSelectionTracker.hasSelection()) {
            mSelectionTracker.clearSelection();
        }
        switch_select_all.setEnabled(false);
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        if (mSelectionTracker.hasSelection()) {
            if (b) {
                selectAll();
            } else {
                unSelectAll();
            }
//            mSelectionTracker.setItemsSelected(data, true);
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {


    }

    private void expandAll() {
        int count = newExpandeddapter.getGroupCount();
        for (int i = 0; i < count; i++) {
            el_contact_list_new.expandGroup(0);
        }
    }

    private void filter(String text) {

        try {
            ArrayList<ContactCallModel> filteredList = new ArrayList<>();

            for (ContactCallModel item : data) {
                if (item.getContactCountry().toLowerCase().contains(text.toLowerCase())) {
                    filteredList.add(item);
                }
            }

            contactListAdapter.filterList(filteredList);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    private boolean allowPermissions() {
        boolean isGranted = isPermissionsGranted(getActivity(), new String[]{Manifest.permission.CALL_PHONE,
                Manifest.permission.READ_PHONE_STATE, Manifest.permission.READ_CALL_LOG,
                Manifest.permission.PROCESS_OUTGOING_CALLS});
        if (!isGranted) {
            askCompactPermissions(new String[]{Manifest.permission.CALL_PHONE,
                            Manifest.permission.READ_PHONE_STATE,
                            Manifest.permission.READ_CALL_LOG,
                            Manifest.permission.PROCESS_OUTGOING_CALLS},
                    new PermissionResult() {
                        @Override
                        public void permissionGranted() {
                        }

                        @Override
                        public void permissionDenied() {
                            Toast.makeText(getActivity(),
                                    getResources().getString(R.string.permission),
                                    Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void permissionForeverDenied() {
                            Toast.makeText(getActivity(), getResources().getString(R.string.permission),
                                    Toast.LENGTH_SHORT).show();
                        }
                    });
        }
        return isGranted;
    }

    public class ContactListAdapter extends RecyclerView.Adapter<ContactListAdapter.ViewHolder> {

        Context context;
        ArrayList<ContactCallModel> data;
        CountDownTimer timer;

        private SelectionTracker mSelectionTracker;

        public ContactListAdapter(Context context, ArrayList<ContactCallModel> data) {
            this.context = context;
            this.data = data;
        }

        public void setSelectionTracker(SelectionTracker mSelectionTracker) {
            this.mSelectionTracker = mSelectionTracker;
        }

        @NonNull
        @Override
        public ContactListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.call_list_item, parent, false);
            return new ContactListAdapter.ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull ContactListAdapter.ViewHolder holder, final int position) {
            try {
                final ContactCallModel item = data.get(position);
                holder.bind(item, mSelectionTracker.isSelected(item));
                holder.text_view_country.setText(data.get(position).getContactCountry());
                holder.tv_number.setText(data.get(position).getContactNumber());

                holder.ll_call_list_item.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String phoneNumber = item.getContactNumber();
                        phoneNumber = phoneNumber.replace(" ", "");
                        phoneNumber = phoneNumber.replace("+", "");
                        phoneNumber = phoneNumber.substring(2);
                        Log.e("callAdapter", "Phone Number is: " + phoneNumber);
                        makeCall(phoneNumber, position);
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        private void makeCall(String phoneNumber, int position) {

            try {

                if (!phoneNumber.equals("")) {

                    Log.e("adapter", "PhoneStateListener: " + PhoneListener.currentStatusOfPhone);


                    if (!Utilities.getDisconnectDurationTime(context).equalsIgnoreCase("")) {
                        long durationTime = Integer.parseInt(Utilities.getDisconnectDurationTime(context)) * 1000;
                        Log.e("adapter", "config duration: " + durationTime);


                        Bundle b = new Bundle();
                        b.putString("phoneNumber", phoneNumber);
                        b.putLong("duration", durationTime);
                        Intent serviceIntent = new Intent(context, MyPhoneListenerService.class);
                        context.startService(serviceIntent);

                    } else {
                        Bundle b = new Bundle();
                        b.putString("phoneNumber", phoneNumber);
                        b.putLong("duration", 00);
                        Intent serviceIntent = new Intent(context, MyPhoneListenerService.class);
                        context.startService(serviceIntent);

                        Log.e("adapter", "config duration not set");
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }


        }

        public void endCall(Context context) {
            TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            try {
                Class c = Class.forName(tm.getClass().getName());
                Method m = c.getDeclaredMethod("getITelephony");
                m.setAccessible(true);
                Object telephonyService = m.invoke(tm);

                c = Class.forName(telephonyService.getClass().getName());
                m = c.getDeclaredMethod("endCall");
                m.setAccessible(true);
                m.invoke(telephonyService);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        public void filterList(ArrayList<ContactCallModel> filteredData) {
            data = filteredData;
            notifyDataSetChanged();
        }


        @Override
        public int getItemCount() {
            return data.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {

            private LinearLayout ll_call_list_item;
            TextView tv_number, text_view_country;

            ViewHolder(@NonNull View v) {
                super(v);
                ll_call_list_item = (LinearLayout) v.findViewById(R.id.ll_call_list_item);
                tv_number = (TextView) v.findViewById(R.id.tv_number);
                text_view_country = (TextView) v.findViewById(R.id.text_view_country);

            }

            void bind(ContactCallModel item, boolean isSelected) {
//            textView.setText(item);
                // If the item is selected then we change its state to activated
                ll_call_list_item.setActivated(isSelected);
            }

            //        /**
//         * Create a new {@link StringItemDetails} for each string item, will be used later by {@link StringItemDetailsLookup#getItemDetails(MotionEvent)}
//         * @return {@link StringItemDetails} instance
//         */
            ContactItemDetails getItemDetails() {
                return new ContactItemDetails(getAdapterPosition(), data.get(getAdapterPosition()));
            }
        }
    }

    @Subscribe
    public void onEventMainThread(ApiErrorEvent event) {
        switch (event.getRequestTag()) {
            case CALL_LIST_TAG:
                Log.e(":::", "Error" + event.getRequestTag());
                dismissProgress();
                break;
            default:
                break;
        }
    }

    @Subscribe
    public void onEventMainThread(ApiErrorWithMessageEvent event) {
        switch (event.getRequestTag()) {
            case CALL_LIST_TAG:
                dismissProgress();
                Log.e(":::", "" + event.getResultMsgUser());
                break;
            default:
                break;
        }
    }

    @Subscribe
    public void onEventMainThread(CallListResponse response) {
        switch (response.getRequestTag()) {
            case CALL_LIST_TAG:
                Log.e(TAG, "Response is: " + response);
                Log.e(TAG, "Response status: " + response.getStatus());
                storeDataInDatabase(response.getData(), response.getPath());
                break;
            default:
                break;
        }
    }

    private void storeDataInDatabase(List<CallListResponseData> data, String path) {
        Log.e(TAG, "list data size: " + data.size());
        dbHelperForListForCall.deleteCallList();
        dbHelperForCallTierNameDetails.deleteCallTierNames();
        dbHelperForCallCountryNameDetails.deleteCallCountryNames();
        for (int i = 0; i < data.size(); i++) {
            Log.e(TAG, "list data size: " + data.size());
//            int image, String phoneNumber, String country, String tier
            Log.e(TAG, "call list database size: " + dbHelperForListForCall.getCallCount());
            String countryImage = path + data.get(i).getCountryImage();
            Log.e(TAG, "CountryImage: " + countryImage);
            dbHelperForListForCall.insertCallListDetails(new CallListDataDbo(countryImage, data.get(i).getNumber(), data.get(i).getCountry(), data.get(i).getTier(), false));
            dbHelperForCallCountryNameDetails.insertCallCountryName(data.get(i).getCountry());
            dbHelperForCallTierNameDetails.insertCallTierName(data.get(i).getTier());
            Log.e(TAG, "Size of Country names list: " + dbHelperForCallCountryNameDetails.getCallCountryNameCount());
            Log.e(TAG, "Size of tier names list: " + dbHelperForCallTierNameDetails.getCallTierNameCount());
            Log.e(TAG, "call list database size: " + dbHelperForListForCall.getCallCount());
            Log.e(TAG, "Size of " + data.get(0).getTier() + " is " + dbHelperForListForCall.getCallTierCount(data.get(0).getTier()));
        }
        setUpList();
        setUpSpinners();
        dismissProgress();
    }

    private void confirmCallDialog() {
        try {
            final Dialog dialog = new Dialog(context);
            dialog.setCancelable(true);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setContentView(R.layout.dialog_call_confirm_multiple);
            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            lp.copyFrom(dialog.getWindow().getAttributes());
            lp.width = WindowManager.LayoutParams.MATCH_PARENT;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

            if (!dialog.isShowing()) {
                dialog.show();
            }
            dialog.getWindow().setAttributes(lp);

            Button btn_yes = dialog.findViewById(R.id.btn_yes);
            btn_yes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Start_the_calling_process();
//                    StartCall();
                    dialog.dismiss();
                }
            });

            Button btn_no = dialog.findViewById(R.id.btn_no);
            btn_no.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void StartCall() {
//        for (int i=0; i < selected_phone_num.size(); i++)
//        {
//            String phoneNumber = selected_phone_num.get(i).getPhoneNumber();
//            phoneNumber = phoneNumber.replace(" ", "");
//            phoneNumber = phoneNumber.replace("+", "");
//            phoneNumber = phoneNumber.substring(2);
//            Log.e(TAG, "Phone Number is: " + phoneNumber);
//            MakePhoneCall(phoneNumber);
//        }

//        while (i < selected_phone_num.size())
//        {
//            Log.e(TAG,"Selected i is: "+i);
//            Log.e(TAG,"Selected phoneNumber is: "+selected_phone_num.get(i).getPhoneNumber());
//
//            MakePhoneCall(selected_phone_num.get(i).getPhoneNumber());
//        }
        i=0;
        if (i < selected_phone_num.size())
        {
            Log.e(TAG,"Selected i is: "+i);
            Log.e(TAG,"Selected phoneNumber is: "+selected_phone_num.get(i).getPhoneNumber());
            Log.e(TAG,"PhoneListener: "+ PhoneListener.currentStatusOfPhone);
            if(PhoneListener.currentStatusOfPhone == 0) {
                MakePhoneCall(selected_phone_num.get(i).getPhoneNumber(), selected_phone_num.get(i));
            }
        }

    }

    public void MakePhoneCall(String phoneNumber, final CallListDataDbo callListDataDbo)
    {
        if (!Utilities.getDisconnectDurationTime(context).equalsIgnoreCase("")) {

            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
//        Date currentFromApi = simpleDateFormat.parse();
//        Log.e("SMSAdapter", "Api Date: " + currentFromApi);
            final Long system_timestamp = System.currentTimeMillis();
            final String timestampDataStr = getTimeStampDate(system_timestamp);
            final String system_str_date = getDate(system_timestamp);
            try {
                Date current_system_date = simpleDateFormat.parse(system_str_date);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            DateFormat dateFormat = new SimpleDateFormat("hh.mm aa");
            final String timeString = dateFormat.format(new Date()).toString();
            System.out.println("Current time in AM/PM: " + timeString);
            Log.e(TAG, "Current Timestamp: " + system_timestamp);
            Log.e(TAG, "Current date: " + system_str_date);
            Log.e(TAG, "Current time: " + timeString);
//
//        differenceTime = printDifference(currentFromApi, current_system_date);
//
//        Log.e("SMSAdapter", "difference Time " + differenceTime);

            long durationTime = Integer.parseInt(Utilities.getDisconnectDurationTime(context)) * 60000;
            Log.e(TAG, "config duration: " + durationTime);
//            String phoneNumber = selected_phone_num.get(i).getPhoneNumber();
            phoneNumber = phoneNumber.replace(" ", "");
            phoneNumber = phoneNumber.replace("+", "");
            phoneNumber = phoneNumber.substring(2);
            Log.e(TAG, "Phone Number is: " + phoneNumber);
//            Intent intent = new Intent(context, MyReceiver.class);
//            PendingIntent pi = PendingIntent.getBroadcast(context, 0, intent, 0);
//            AlarmManager alarmManager = (AlarmManager)context.getSystemService(ALARM_SERVICE);
//            alarmManager.set(AlarmManager.RTC_WAKEUP, durationTime, pi);

//            dbHelperForCallSummary.insertCallSummaryDetails(new CallSummaryDbo(callListDataDbo.getImage(),callListDataDbo.getPhoneNumber(),"Successful",timeString,system_str_date,system_timestamp,timestampDataStr));
            dbHelperForCallSummary.insertCallSummaryDetails(new CallSummaryDbo(callListDataDbo.getImage(),callListDataDbo.getPhoneNumber(),"Successful",timeString,system_str_date,system_timestamp,timestampDataStr));
            Intent intent2 = new Intent(Intent.ACTION_CALL);
            intent2.setData(Uri.parse("tel:" + phoneNumber));
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            context.startActivity(intent2);
            Log.e(TAG,"PhoneListener: "+ PhoneListener.currentStatusOfPhone);

//            if (PhoneListener.LISTEN_CALL_STATE = TelephonyManager.CALL_STATE_OFFHOOK)
//            {
//
//            }

//            callImage,callNumber,status,callTime,callDate,callTimeStamp,callStrTimeStamp

//            dbHelperForSmsSummary.insertSmsSummaryDetails(new SMSSummaryDbo(image,actualPhoneNumber,status,timeString,system_str_date,timeStampDate,timestampDataStr));

            if (timer != null) {
                timer.cancel();
                timer = null;
            }

            timer = new CountDownTimer(durationTime, 1000) {
                @Override
                public void onTick(long l) {
                    Log.e(TAG, "seconds remaining : " + l / 1000);

                    Log.e("CountDownTimer","PhoneListener: "+ PhoneListener.currentStatusOfPhone);

//                    if (PhoneListener.currentStatusOfPhone == 0)
//                    {
////                        if(PhoneListener.currentStatusOfPhone == 0) {
//                            Log.e(TAG, "inside phoneListener");
//                            timer.onFinish();
//                            endCall(context);
//                            i++;
//                            if (i < selected_phone_num.size()) {
//                                MakePhoneCall(selected_phone_num.get(i).getPhoneNumber(), selected_phone_num.get(i));
//                            }
////                        }
//                    }
                }

                @Override
                public void onFinish() {
                    Log.e("done!", "done!");
                    endCall(context);
                    if(PhoneListener.currentStatusOfPhone == 0) {
                        i++;
                        if (i < selected_phone_num.size()) {
                            MakePhoneCall(selected_phone_num.get(i).getPhoneNumber(), selected_phone_num.get(i));
                        }
                    }
                }
            }.start();

//            i++;
        }
    }
    private void Start_the_calling_process() {
//        Toast.makeText(context, String.valueOf("call list count : " + call_index), Toast.LENGTH_SHORT).show();
        if (call_index < selected_phone_num.size()) {
            CallListDataDbo callListDataDbo = selected_phone_num.get(call_index);
            makeCall(callListDataDbo.getPhoneNumber(),callListDataDbo);

            Log.e(TAG,"Call done."+callListDataDbo.getPhoneNumber());

//            StringBuffer sb = new StringBuffer();
//            Cursor managedCursor = ((BaseActivity)getActivity()).managedQuery(CallLog.Calls.CONTENT_URI, null, null, null, null);
//            int number = managedCursor.getColumnIndex(CallLog.Calls.NUMBER);
//            int type = managedCursor.getColumnIndex(CallLog.Calls.TYPE);
//            int date = managedCursor.getColumnIndex(CallLog.Calls.DATE);
//            int duration = managedCursor.getColumnIndex(CallLog.Calls.DURATION);
//
//            while (managedCursor.moveToNext()) {
//                String phNumber = managedCursor.getString(number);
//
//                String callDate = managedCursor.getString(date);
//                Date callDayTime = new Date(Long.valueOf(callDate));
//                SimpleDateFormat sdf2 = new SimpleDateFormat("HH:mm:ss", Locale.ENGLISH); // the format of your date
//                String originalTimeString =  sdf2.format(callDayTime);
//                Log.e(TAG,"originalTimeString: "+originalTimeString);
//
//                if (managedCursor.getString(number).equalsIgnoreCase(phoneNumberCompare) && originalTimeString.equalsIgnoreCase(timeCompareStr)) {
//                    String callType = managedCursor.getString(type);
////                String callDate = managedCursor.getString(date);
////                Date callDayTime = new Date(Long.valueOf(callDate));
//                    String callDuration = managedCursor.getString(duration);
//                    String dir = null;
//                    int dircode = Integer.parseInt(callType);
//                    switch (dircode) {
//                        case CallLog.Calls.OUTGOING_TYPE:
//                            dir = "OUTGOING";
//                            break;
//                        case CallLog.Calls.INCOMING_TYPE:
//                            dir = "INCOMING";
//                            break;
//                        case CallLog.Calls.MISSED_TYPE:
//                            dir = "MISSED";
//                            break;
//                        case CallLog.Calls.REJECTED_TYPE:
//                            dir = "REJECTED";
//                            break;
//                    }
//                    sb.append("\nPhone Number:--- " + phNumber + " \nCall Type:--- " + dir + " \nCall Date:--- " + callDayTime + " \nCall duration in sec :--- " + callDuration);
//                    sb.append("\n----------------------------------");
//
//                    dbHelperForCallSummary.insertCallSummaryDetails(new CallSummaryDbo(callListDataDbo.getImage(),callListDataDbo.getPhoneNumber(),dir,timeString,system_str_date,system_timestamp,timestampDataStr));
//                }
//            }
//            Log.e(TAG,"Call Log: "+sb);
//            dbHelperForCallSummary.insertCallSummaryDetails(callListDataDbo);
//
//            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
////        Date currentFromApi = simpleDateFormat.parse();
////        Log.e("SMSAdapter", "Api Date: " + currentFromApi);
//            final Long system_timestamp = System.currentTimeMillis();
//            final String timestampDataStr = getTimeStampDate(system_timestamp);
//            final String system_str_date = getDate(system_timestamp);
//            try {
//                Date current_system_date = simpleDateFormat.parse(system_str_date);
//            } catch (ParseException e) {
//                e.printStackTrace();
//            }
//
//            DateFormat dateFormat = new SimpleDateFormat("hh.mm aa");
//            final String timeString = dateFormat.format(new Date()).toString();
//            System.out.println("Current time in AM/PM: " + timeString);
//            Log.e(TAG, "Current Timestamp: " + system_timestamp);
//            Log.e(TAG, "Current date: " + system_str_date);
//            Log.e(TAG, "Current time: " + timeString);
//            dbHelperForCallSummary.insertCallSummaryDetails(new CallSummaryDbo(callListDataDbo.getImage(),callListDataDbo.getPhoneNumber(),"Successful",timeString,system_str_date,system_timestamp,timestampDataStr));

        } else {
            Toast.makeText(context, "All calls completed", Toast.LENGTH_SHORT).show();
            Log.e(TAG,"DbHelperSummaryDemo Size is:"+dbHelperForCallSummaryForPhoneTime.getCallSummaryCountDemo());
//            Intent i = new Intent(context, CallSummaryStoreService.class);
//            context.startService(i);
            selected_phone_num = new ArrayList<>();
            setUpList();
            setUpSpinners();
            setClickListenerSpinner();
            pos = null;
            call_index = 0;
//            newExpandeddapter.notifyDataSetChanged();
        }
    }

    private void makeCall(String phoneNumber,CallListDataDbo callListDataDbo) {
        try {
            if (!phoneNumber.equals("")) {
                Intent intent2 = new Intent(Intent.ACTION_CALL);
                intent2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent2.setData(Uri.parse("tel:" + phoneNumber));
                if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                context.startActivity(intent2);

                String phoneNumberCompare = callListDataDbo.getPhoneNumber();
                phoneNumberCompare = phoneNumberCompare.replace(" ", "");

                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
//        Date currentFromApi = simpleDateFormat.parse();
//        Log.e("SMSAdapter", "Api Date: " + currentFromApi);
                final Long system_timestamp = System.currentTimeMillis();
                final String timestampDataStr = getTimeStampDate(system_timestamp);
                final String system_str_date = getDate(system_timestamp);
//                SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss", Locale.ENGLISH); // the format of your date
//                Date dateTime=sdf.parse(system_str_date);// converting String to date
//                System.out.println(sdf.format(dateTime));
               String timeCompareStr =  getDateTime(system_timestamp);
                Log.e(TAG,"timeCompareStr: "+timeCompareStr);

                try {
                    Date current_system_date = simpleDateFormat.parse(system_str_date);

                } catch (ParseException e) {
                    e.printStackTrace();
                }

                DateFormat dateFormat = new SimpleDateFormat("hh.mm aa");
                final String timeString = dateFormat.format(new Date()).toString();
                System.out.println("Current time in AM/PM: " + timeString);
                Log.e(TAG, "Current Timestamp: " + system_timestamp);
                Log.e(TAG, "Current date: " + system_str_date);
                Log.e(TAG, "Current time: " + timeString);


//                textView.setText(sb);

                dbHelperForCallSummaryForPhoneTime.insertCallSummaryDetailsDemo(new CallSummaryDbo(callListDataDbo.getImage(),callListDataDbo.getPhoneNumber(),"Successful",timeString,system_str_date,system_timestamp,timestampDataStr));

                /*if (!Utilities.getDisconnectDurationTime(context).equalsIgnoreCase("")) {

                } else {
                    Intent intent2 = new Intent(Intent.ACTION_CALL);
                    intent2.setData(Uri.parse("tel:" + phoneNumber));
                    if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {

                        return;
                    }
                    context.startActivity(intent2);
                }*/
            }

        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(context, "dasdasdas", Toast.LENGTH_SHORT).show();
        }
    }

    public void endCall(Context context) {
        TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        try {
            Class c = Class.forName(tm.getClass().getName());
            Method m = c.getDeclaredMethod("getITelephony");
            m.setAccessible(true);
            Object telephonyService = m.invoke(tm);

            c = Class.forName(telephonyService.getClass().getName());
            m = c.getDeclaredMethod("endCall");
            m.setAccessible(true);
            m.invoke(telephonyService);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//    public class OutgoingReceiver extends BroadcastReceiver {
//        TelephonyManager telManager;
//
//        public OutgoingReceiver() {
//        }
//
//        @Override
//        public void onReceive(Context context, Intent intent) {/*
//            String number = intent.getStringExtra(Intent.EXTRA_PHONE_NUMBER);
//            Toast.makeText(context, "Outgoing: " + number, Toast.LENGTH_LONG).show();*/
//            telManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
//            telManager.listen(phoneStateListener, PhoneStateListener.LISTEN_CALL_STATE);
//        }
//
//        PhoneStateListener phoneStateListener = new PhoneStateListener() {
//            @Override
//            public void onCallStateChanged(int state, String phoneNumber) {
//                super.onCallStateChanged(state, phoneNumber);
//                if (state == TelephonyManager.CALL_STATE_IDLE) {
////                    long durationTime = Integer.parseInt(Utilities.getDisconnectDurationTime(context)) * 60000;
//                    long durationTime = 30000;
//                    if (!call_start) {
//                        call_start = true;
//                        Start_timer(durationTime);
////                        Toast.makeText(context.getApplicationContext(), "Call start", Toast.LENGTH_LONG).show();
//                    } else {
//                        call_start = false;
////                        Stop_timer();
////                        Toast.makeText(context.getApplicationContext(), "Call end", Toast.LENGTH_LONG).show();
////                        endCall(context);
//                        Start_the_calling_process();
//                    }
//                }
//            }
//        };
//    }

    private void Start_timer(final long durationTime) {
        if (timer != null) {
            timer.cancel();
            timer = null;
        }

        timer = new CountDownTimer(durationTime, 1000) {
            @Override
            public void onTick(long l) {
//                Toast.makeText(context, String.valueOf(l / 1000), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFinish() {
                endCall(context);
                Start_the_calling_process();
            }
        }.start();
    }

    public class OutgoingReceiver extends BroadcastReceiver {
        TelephonyManager telManager;

        public OutgoingReceiver() {
        }

        @Override
        public void onReceive(Context context, Intent intent) {/*
            String number = intent.getStringExtra(Intent.EXTRA_PHONE_NUMBER);
            Toast.makeText(context, "Outgoing: " + number, Toast.LENGTH_LONG).show();*/
            telManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            telManager.listen(phoneStateListener, PhoneStateListener.LISTEN_CALL_STATE);
        }

        PhoneStateListener phoneStateListener = new PhoneStateListener() {
            @Override
            public void onCallStateChanged(int state, String phoneNumber) {
                super.onCallStateChanged(state, phoneNumber);
                if (state == TelephonyManager.CALL_STATE_IDLE) {
                    try {
                        long durationTime = Integer.parseInt(Utilities.getDisconnectDurationTime(context)) * 60000;
//                    long durationTime = 65000;
                        if (!call_start) {
                            call_start = true;
                            call_index++;
                            Start_timer(durationTime);
//                        Toast.makeText(context.getApplicationContext(), "Call start", Toast.LENGTH_LONG).show();
                        } else {
                            call_start = false;
//                        Stop_timer();
//                        Toast.makeText(context.getApplicationContext(), "Call end", Toast.LENGTH_LONG).show();
//                        endCall(context);
                            Start_the_calling_process();
                        }
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                }
            }
        };
    }

    @Override
    public void Add_remove(String current_selected_country, String tier_name, String
            action, String phoneNumber) {
        if (current_selected_country.equals("All Countries")) {
            if (countryNames_all.size() != 0) {
                Set_selected_in_country_data_array(current_selected_country, tier_name, action, phoneNumber, global_data_list_all_country, "ALL");
                for (int i = 0; i < countryNames_all.size(); i++) {
                    Set_selected_in_country_data_array(countryNames_all.get(i), tier_name, action, phoneNumber, global_data_list_country_wise, "COUNTRY");
                }
            }
        } else {
            Set_selected_in_country_data_array(current_selected_country, tier_name, action, phoneNumber, global_data_list_country_wise, "COUNTRY");
        }
    }

    private void Set_selected_in_country_data_array(String current_selected_country, String tier_name, String action, String phoneNumber, ArrayList<ContactCallFinalList> finalData, String from) {
        if (finalData.size() != 0) {
            for (int i = 0; i < finalData.size(); i++) {
                ContactCallFinalList contactCallFinalList = finalData.get(i);
                if (contactCallFinalList.getCountry_name().equals(current_selected_country)) {
                    if (contactCallFinalList.getTier_list().size() != 0) {
                        for (int j = 0; j < contactCallFinalList.getTier_list().size(); j++) {
                            ContactCallFinalList contactCallFinalList1 = contactCallFinalList.getTier_list().get(j);
                            if (contactCallFinalList1.getName().equals(tier_name)) {
                                if (contactCallFinalList1.getChildList().size() != 0) {
                                    if (!phoneNumber.equals("")) {
                                        for (int k = 0; k < contactCallFinalList1.getChildList().size(); k++) {
                                            CallListDataDbo callListDataDbo = contactCallFinalList1.getChildList().get(k);
                                            if (callListDataDbo.getPhoneNumber().equals(phoneNumber)) {
                                                if (action.equals("ADD")) {
                                                    callListDataDbo.setSelected(true);
                                                } else {
                                                    callListDataDbo.setSelected(false);
                                                }
                                                contactCallFinalList1.getChildList().remove(k);
                                                contactCallFinalList1.getChildList().add(k, callListDataDbo);
                                                break;
                                            }
                                        }
                                    } else {
                                        for (int k = 0; k < contactCallFinalList1.getChildList().size(); k++) {
                                            CallListDataDbo callListDataDbo = contactCallFinalList1.getChildList().get(k);
                                            if (action.equals("ADD")) {
                                                callListDataDbo.setSelected(true);
                                            } else {
                                                callListDataDbo.setSelected(false);
                                            }
                                            contactCallFinalList1.getChildList().remove(k);
                                            contactCallFinalList1.getChildList().add(k, callListDataDbo);
                                        }

                                        if (action.equals("ADD")) {
                                            contactCallFinalList1.setSelected_tier(true);
                                        } else {
                                            contactCallFinalList1.setSelected_tier(false);
                                        }
                                    }
                                }
                                break;
                            }
                        }
                    }
                    break;
                }
            }

            if (from.equals("ALL")) {
                global_data_list_all_country = finalData;
            } else {
                global_data_list_country_wise = finalData;
            }

            selected_phone_num = new ArrayList<>();
//            selected_phone_num.add("asda");
            newExpandeddapter.notifyDataSetChanged();
            Collect_phone_number();
        }
    }

    private String getDate(long time) {
        Date date = new Date(time); // *1000 is to convert seconds to milliseconds
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH); // the format of your date
        return sdf.format(date);
    }

    private String getDateTime(long time) {
        Date date = new Date(time); // *1000 is to convert seconds to milliseconds
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss", Locale.ENGLISH); // the format of your date
        return sdf.format(date);
    }

    private String getTimeStampDate(long time) {
        Date date = new Date(time); // *1000 is to convert seconds to milliseconds
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH); // the format of your date
        return sdf.format(date);
    }


}
