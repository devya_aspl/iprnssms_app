package com.totalsmsclonedemo.fragments;


import android.Manifest;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ExpandableListView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;

import com.totalsmsclonedemo.Model.ContactCallModel;
import com.totalsmsclonedemo.Model.ContactListCallSmsDboList;
import com.totalsmsclonedemo.Model.ContactSmsFinalList;
import com.totalsmsclonedemo.PermissionResult;
import com.totalsmsclonedemo.R;
import com.totalsmsclonedemo.Utilities;
import com.totalsmsclonedemo.activities.BaseActivity;
import com.totalsmsclonedemo.adapters.ExpandableSMSListAdapter;
import com.totalsmsclonedemo.adapters.MyExpandableSMSListAdapter;
import com.totalsmsclonedemo.databases.DBHelperForListForSms;
import com.totalsmsclonedemo.databases.DBHelperForSMSCountryNameDetails;
import com.totalsmsclonedemo.databases.DBHelperForSmsSummary;
import com.totalsmsclonedemo.databases.DBHelperForSmsTierNameDetails;
import com.totalsmsclonedemo.dbo.CallAndSmsListDataDbo;
import com.totalsmsclonedemo.dbo.SMSSummaryDbo;
import com.totalsmsclonedemo.dbo.SmsListDataDbo;
import com.totalsmsclonedemo.dialog.CustomDialog;
import com.totalsmsclonedemo.events.ApiErrorEvent;
import com.totalsmsclonedemo.events.ApiErrorWithMessageEvent;
import com.totalsmsclonedemo.network.response.SmsListResponse;
import com.totalsmsclonedemo.network.response.SmsListResponseData;

import org.greenrobot.eventbus.Subscribe;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class SMSFragment extends BaseFragment implements MyExpandableSMSListAdapter.Add_remove_child{

    View v;
    Unbinder unbinder;

    Context context;
    @BindView(R.id.sp_sort_country)
    Spinner sp_sort_country;

    @BindView(R.id.sp_sort_tier)
    Spinner sp_sort_tier;

    @BindView(R.id.el_contact_list_new)
    ExpandableListView el_contact_list_new;

    List<String> countryNames_all;

    HashMap<String, List<ContactCallModel>> hashMap;

    @BindView(R.id.switch_select_all)
    Switch switch_select_all;

    private static ExpandableSMSListAdapter expandableSMSListAdapter;
    ArrayAdapter<String> adapterChooseSortBy,adapterChooseSortByTier;

    ArrayList<ContactListCallSmsDboList> newData;
    MyExpandableSMSListAdapter newExpandeddapter;

    ArrayList<ContactSmsFinalList> finalData;
    ArrayList<String> finalHeaderData;

    public static final String SMS_LIST_TAG = "sms_list_tag";
    public static final String TAG = SMSFragment.class.getSimpleName();

    DBHelperForListForSms dbHelperForListForSms;
    DBHelperForSmsTierNameDetails dbHelperForSmsTierNameDetails;
    DBHelperForSMSCountryNameDetails dbHelperForSMSCountryNameDetails;

    DBHelperForSmsSummary dbHelperForSmsSummary;

    public static Integer pos;
    public static ArrayList<ContactSmsFinalList> global_selected_child_list;

    public static ArrayList<SmsListDataDbo> selected_child_list;
    int call_index = 0;

    public static ArrayList<SmsListDataDbo> selected_phone_num;

    ArrayList<ContactSmsFinalList> global_data_list_country_wise;
    ArrayList<ContactSmsFinalList> global_data_list_all_country;

    static CustomDialog dialog;

    public static String current_selected_country = "All Countries";
    public static String current_selected_tier = "All Tier";

    @BindView(R.id.fab_sms)
    FloatingActionButton fab_sms;

    String status="";

    @Override
    public void setToolbarForFragment() {
        ((BaseActivity) getActivity()).getToolbar().setVisibility(View.VISIBLE);
        ((BaseActivity) getActivity()).getLl_toolbar().setVisibility(View.VISIBLE);
        ((BaseActivity) getActivity()).getToolbartext().setVisibility(View.VISIBLE);
        ((BaseActivity) getActivity()).getToolbartext().setText("Test SMS");

        ((BaseActivity)getActivity()).getImgToolBarBack().setVisibility(View.VISIBLE);
        ((BaseActivity)getActivity()).getImgToolBarBack().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mainInterfaces.OpenDashBoard();
            }
        });
    }

    @Override
    public void setTabLayout() {
        ((BaseActivity) getActivity()).getLl_view_tab().setVisibility(View.VISIBLE);
//        ((BaseActivity) getActivity()).getTabLayout().setVisibility(View.VISIBLE);

    }

    public SMSFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_sms, container, false);
        context = (BaseActivity)getActivity();
        allowPermissions();
        dbHelperForListForSms = new DBHelperForListForSms(context);
        dbHelperForSmsTierNameDetails = new DBHelperForSmsTierNameDetails(context);
        dbHelperForSMSCountryNameDetails = new DBHelperForSMSCountryNameDetails(context);
        dbHelperForSmsSummary = new DBHelperForSmsSummary(context);
        unbinder = ButterKnife.bind(this, v);

//        Intent i2 = new Intent(context, SmsDataStoreService.class);
//        context.startService(i2);
//        initDialog();
        el_contact_list_new.setGroupIndicator(null);
//        if (!mApiClient.isRequestRunning(SMS_LIST_TAG)) {
//            showProgress();
//            mApiClient.get_sms_list(SMS_LIST_TAG);
//        }
        countryNames_all = dbHelperForSMSCountryNameDetails.getAllSmsCountryName();

        global_selected_child_list = new ArrayList<>();
        setUpList();
        setUpSpinners();
        setClickListener();

        selected_child_list = new ArrayList<>();

        fab_sms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Collect_phone_number();
                if (selected_phone_num != null && selected_phone_num.size() != 0) {
                    Log.e(TAG,"size is: "+selected_phone_num.size());
//                    confirmCallDialog();
                    sendMessageList(selected_phone_num);
                } else {
                    Toast.makeText(context, "First select multiple contacts trough long press on Tier", Toast.LENGTH_SHORT).show();
                }
            }
        });

        return v;
    }

    private void sendMessageList(ArrayList<SmsListDataDbo> selected_phone_num) {

        try {

            for (int i=0;i < selected_phone_num.size();i++) {

                String phoneNumber = selected_phone_num.get(i).getPhoneNumber();
                phoneNumber = phoneNumber.replace(" ", "");
                phoneNumber = phoneNumber.replace("+", "");
                phoneNumber = phoneNumber.substring(2);
                Log.e(TAG, "Phone Number is: " + phoneNumber);

                int gotSmsNumber = Integer.parseInt(Utilities.getNoSmsSendEachTime(context));
                Log.e("adapter", "Got sms Number: " + gotSmsNumber);
                for (int j = 1; j <= gotSmsNumber; j++) {
                    sendSMS(phoneNumber, "Test Message from IPRNSMS App", selected_phone_num.get(i).getPhoneNumber(), selected_phone_num.get(i).getImage());
                }
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private void sendSMS(String phoneNumber, String message, final String actualPhoneNumber, final String image) {
        try {
//            int i=0;
//            int gotSmsNumber = Integer.parseInt(Utilities.getNoSmsSendEachTime(context));
//                Log.e("adapter", "Got sms Number: " + gotSmsNumber);
//            while (i < gotSmsNumber)
//            {
//
//            }
//
//            for (int j=1;j <= gotSmsNumber; j++)
//            {
//                Log.e("adapter","index: "+j)
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
//        Date currentFromApi = simpleDateFormat.parse();
//        Log.e("SMSAdapter", "Api Date: " + currentFromApi);
            final Long system_timestamp = System.currentTimeMillis();
            final String timestampDataStr = getTimeStampDate(system_timestamp);
            final String system_str_date = getDate(system_timestamp);
            try {
                Date current_system_date = simpleDateFormat.parse(system_str_date);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            DateFormat dateFormat = new SimpleDateFormat("hh.mm aa");
            final String timeString = dateFormat.format(new Date()).toString();
            System.out.println("Current time in AM/PM: " + timeString);
            Log.e("SMSAdapter", "Current Timestamp: " + system_timestamp);
            Log.e("SMSAdapter", "Current date: " + system_str_date);
            Log.e("SMSAdapter", "Current time: " + timeString);
//
//        differenceTime = printDifference(currentFromApi, current_system_date);
//
//        Log.e("SMSAdapter", "difference Time " + differenceTime);


            String SENT = "SMS_SENT";
            String DELIVERED = "SMS_DELIVERED";

            PendingIntent sentPI = PendingIntent.getBroadcast(context, 0,
                    new Intent(SENT), 0);

            PendingIntent deliveredPI = PendingIntent.getBroadcast(context, 0,
                    new Intent(DELIVERED), 0);

            //---when the SMS has been sent---
            context.registerReceiver(new BroadcastReceiver() {
                @Override
                public void onReceive(Context arg0, Intent arg1) {
                    switch (getResultCode()) {
                        case Activity.RESULT_OK:
                            Toast.makeText(context, "SMS sent",
                                    Toast.LENGTH_SHORT).show();
                            break;
                        case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                            Toast.makeText(context, "Generic failure",
                                    Toast.LENGTH_SHORT).show();
                            break;
                        case SmsManager.RESULT_ERROR_NO_SERVICE:
                            Toast.makeText(context, "No service",
                                    Toast.LENGTH_SHORT).show();
                            break;
                        case SmsManager.RESULT_ERROR_NULL_PDU:
                            Toast.makeText(context, "Null PDU",
                                    Toast.LENGTH_SHORT).show();
                            break;
                        case SmsManager.RESULT_ERROR_RADIO_OFF:
                            Toast.makeText(context, "Radio off",
                                    Toast.LENGTH_SHORT).show();
                            break;
                    }
                }
            }, new IntentFilter(SENT));

            //---when the SMS has been delivered---
            context.registerReceiver(new BroadcastReceiver() {
                @Override
                public void onReceive(Context arg0, Intent arg1) {
                    switch (getResultCode()) {
                        case Activity.RESULT_OK:
                            Toast.makeText(context, "SMS delivered",
                                    Toast.LENGTH_SHORT).show();
                            status = "delivered";
                            Log.e("adapter","if status is: "+status);
                            storeData(image, actualPhoneNumber, "delivered", system_str_date, timeString,system_timestamp,timestampDataStr);
//                                dbHelperForSmsSummary.insertSmsSummaryDetails(new SMSSummaryDbo(image,actualPhoneNumber,status,timeString,system_str_date,timestampData));
                            break;
                        case Activity.RESULT_CANCELED:
                            Toast.makeText(context, "SMS not delivered",
                                    Toast.LENGTH_SHORT).show();
                            status = "failed";
                            Log.e("adapter","else status is: "+status);
                            storeData(image, actualPhoneNumber, "Undelivered", system_str_date, timeString,system_timestamp,timestampDataStr);
//                                dbHelperForSmsSummary.insertSmsSummaryDetails(new SMSSummaryDbo(image,actualPhoneNumber,status,timeString,system_str_date,timestampData));
                            break;
                    }
//                        storeData(image, actualPhoneNumber, status, system_str_date, timeString,timestampData);
                }
            }, new IntentFilter(DELIVERED));
            Log.e("adapter","general status is: "+status);
//                Log.e("adapter","status is: "+status);
//            dbHelperForSmsSummary.insertSmsSummaryDetails(new SMSSummaryDbo(image,actualPhoneNumber,status,timeString,system_str_date));

//        PendingIntent pi = PendingIntent.getActivity(this, 0,
//                new Intent(context, SMS.class), 0);

            SmsManager sms = SmsManager.getDefault();
            sms.sendTextMessage(phoneNumber, null, message, sentPI, deliveredPI);

//            }


//            if (!Utilities.getNoSmsSendEachTime(context).equalsIgnoreCase("")) {
//
//                int gotSmsNumber = Integer.parseInt(Utilities.getNoSmsSendEachTime(context));
//                Log.e("adapter", "Got sms Number: " + gotSmsNumber);
//                for (int i = 0; i < gotSmsNumber; i++) {
//                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
////        Date currentFromApi = simpleDateFormat.parse();
////        Log.e("SMSAdapter", "Api Date: " + currentFromApi);
//                    Long system_timestamp = System.currentTimeMillis();
//                    final String system_str_date = getDate(system_timestamp);
//                    try {
//                        Date current_system_date = simpleDateFormat.parse(system_str_date);
//                    } catch (ParseException e) {
//                        e.printStackTrace();
//                    }
//
//                    DateFormat dateFormat = new SimpleDateFormat("hh.mm aa");
//                    final String timeString = dateFormat.format(new Date()).toString();
//                    System.out.println("Current time in AM/PM: " + timeString);
//                    Log.e("SMSAdapter", "Current Timestamp: " + system_timestamp);
//                    Log.e("SMSAdapter", "Current date: " + system_str_date);
//                    Log.e("SMSAdapter", "Current time: " + timeString);
////
////        differenceTime = printDifference(currentFromApi, current_system_date);
////
////        Log.e("SMSAdapter", "difference Time " + differenceTime);
//
//
//                    String SENT = "SMS_SENT";
//                    String DELIVERED = "SMS_DELIVERED";
//
//                    PendingIntent sentPI = PendingIntent.getBroadcast(context, 0,
//                            new Intent(SENT), 0);
//
//                    PendingIntent deliveredPI = PendingIntent.getBroadcast(context, 0,
//                            new Intent(DELIVERED), 0);
//
//                    //---when the SMS has been sent---
//                    context.registerReceiver(new BroadcastReceiver() {
//                        @Override
//                        public void onReceive(Context arg0, Intent arg1) {
//                            switch (getResultCode()) {
//                                case Activity.RESULT_OK:
//                                    Toast.makeText(context, "SMS sent",
//                                            Toast.LENGTH_SHORT).show();
//                                    break;
//                                case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
//                                    Toast.makeText(context, "Generic failure",
//                                            Toast.LENGTH_SHORT).show();
//                                    break;
//                                case SmsManager.RESULT_ERROR_NO_SERVICE:
//                                    Toast.makeText(context, "No service",
//                                            Toast.LENGTH_SHORT).show();
//                                    break;
//                                case SmsManager.RESULT_ERROR_NULL_PDU:
//                                    Toast.makeText(context, "Null PDU",
//                                            Toast.LENGTH_SHORT).show();
//                                    break;
//                                case SmsManager.RESULT_ERROR_RADIO_OFF:
//                                    Toast.makeText(context, "Radio off",
//                                            Toast.LENGTH_SHORT).show();
//                                    break;
//                            }
//                        }
//                    }, new IntentFilter(SENT));
//
//                    //---when the SMS has been delivered---
//                    context.registerReceiver(new BroadcastReceiver() {
//                        @Override
//                        public void onReceive(Context arg0, Intent arg1) {
//                            switch (getResultCode()) {
//                                case Activity.RESULT_OK:
//                                    Toast.makeText(context, "SMS delivered",
//                                            Toast.LENGTH_SHORT).show();
//                                    storeData(image, actualPhoneNumber, "delivered", system_str_date, timeString);
//
//                                    break;
//                                case Activity.RESULT_CANCELED:
//                                    Toast.makeText(context, "SMS not delivered",
//                                            Toast.LENGTH_SHORT).show();
//                                    storeData(image, actualPhoneNumber, "Undelivered", system_str_date, timeString);
//                                    break;
//                            }
//                        }
//                    }, new IntentFilter(DELIVERED));
//
////        PendingIntent pi = PendingIntent.getActivity(this, 0,
////                new Intent(context, SMS.class), 0);
//
//                    SmsManager sms = SmsManager.getDefault();
//                    sms.sendTextMessage(phoneNumber, null, message, sentPI, deliveredPI);
////                }
////
////            }
////            else
////            {
////                SmsManager sms = SmsManager.getDefault();
////                sms.sendTextMessage(phoneNumber, null, message, sentPI, deliveredPI);
//                }
//
//            } else {
//
////                int gotSmsNumber = Integer.parseInt(Utilities.getNoSmsSendEachTime(context));
////                Log.e("adapter", "Got sms Number: " + gotSmsNumber);
////                for (int i = 0; i < gotSmsNumber; i++) {
//                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
////        Date currentFromApi = simpleDateFormat.parse();
////        Log.e("SMSAdapter", "Api Date: " + currentFromApi);
//                    Long system_timestamp = System.currentTimeMillis();
//                    final String system_str_date = getDate(system_timestamp);
//                    try {
//                        Date current_system_date = simpleDateFormat.parse(system_str_date);
//                    } catch (ParseException e) {
//                        e.printStackTrace();
//                    }
//
//                    DateFormat dateFormat = new SimpleDateFormat("hh.mm aa");
//                    final String timeString = dateFormat.format(new Date()).toString();
//                    System.out.println("Current time in AM/PM: " + timeString);
//                    Log.e("SMSAdapter", "Current Timestamp: " + system_timestamp);
//                    Log.e("SMSAdapter", "Current date: " + system_str_date);
//                    Log.e("SMSAdapter", "Current time: " + timeString);
////
////        differenceTime = printDifference(currentFromApi, current_system_date);
////
////        Log.e("SMSAdapter", "difference Time " + differenceTime);
//
//
//                    String SENT = "SMS_SENT";
//                    String DELIVERED = "SMS_DELIVERED";
//
//                    PendingIntent sentPI = PendingIntent.getBroadcast(context, 0,
//                            new Intent(SENT), 0);
//
//                    PendingIntent deliveredPI = PendingIntent.getBroadcast(context, 0,
//                            new Intent(DELIVERED), 0);
//
//                    //---when the SMS has been sent---
//                    context.registerReceiver(new BroadcastReceiver() {
//                        @Override
//                        public void onReceive(Context arg0, Intent arg1) {
//                            switch (getResultCode()) {
//                                case Activity.RESULT_OK:
//                                    Toast.makeText(context, "SMS sent",
//                                            Toast.LENGTH_SHORT).show();
//                                    break;
//                                case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
//                                    Toast.makeText(context, "Generic failure",
//                                            Toast.LENGTH_SHORT).show();
//                                    break;
//                                case SmsManager.RESULT_ERROR_NO_SERVICE:
//                                    Toast.makeText(context, "No service",
//                                            Toast.LENGTH_SHORT).show();
//                                    break;
//                                case SmsManager.RESULT_ERROR_NULL_PDU:
//                                    Toast.makeText(context, "Null PDU",
//                                            Toast.LENGTH_SHORT).show();
//                                    break;
//                                case SmsManager.RESULT_ERROR_RADIO_OFF:
//                                    Toast.makeText(context, "Radio off",
//                                            Toast.LENGTH_SHORT).show();
//                                    break;
//                            }
//                        }
//                    }, new IntentFilter(SENT));
//
//                    //---when the SMS has been delivered---
//                    context.registerReceiver(new BroadcastReceiver() {
//                        @Override
//                        public void onReceive(Context arg0, Intent arg1) {
//                            switch (getResultCode()) {
//                                case Activity.RESULT_OK:
//                                    Toast.makeText(context, "SMS delivered",
//                                            Toast.LENGTH_SHORT).show();
//                                    storeData(image, actualPhoneNumber, "delivered", system_str_date, timeString);
//
//                                    break;
//                                case Activity.RESULT_CANCELED:
//                                    Toast.makeText(context, "SMS not delivered",
//                                            Toast.LENGTH_SHORT).show();
//                                    storeData(image, actualPhoneNumber, "Undelivered", system_str_date, timeString);
//                                    break;
//                            }
//                        }
//                    }, new IntentFilter(DELIVERED));
//
////        PendingIntent pi = PendingIntent.getActivity(this, 0,
////                new Intent(context, SMS.class), 0);
//
//                    SmsManager sms = SmsManager.getDefault();
//                    sms.sendTextMessage(phoneNumber, null, message, sentPI, deliveredPI);
////                }
//            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private void storeData(String image, String actualPhoneNumber, String status, String system_str_date, String timeString,Long timeStampDate,String timestampDataStr) {

//        int smsImage;
//    String smsNumber,status, smsTime, smsDate;

        dbHelperForSmsSummary.insertSmsSummaryDetails(new SMSSummaryDbo(image,actualPhoneNumber,status,timeString,system_str_date,timeStampDate,timestampDataStr));
    }



    private boolean allowPermissions() {
        boolean isGranted = isPermissionsGranted(getActivity(), new String[]{Manifest.permission.SEND_SMS
                });
        if (!isGranted) {
            askCompactPermissions(new String[]{Manifest.permission.SEND_SMS
                          },
                    new PermissionResult() {
                        @Override
                        public void permissionGranted() {
                        }

                        @Override
                        public void permissionDenied() {
                            Toast.makeText(getActivity(),
                                    getResources().getString(R.string.permission),
                                    Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void permissionForeverDenied() {
                            Toast.makeText(getActivity(),getResources().getString(R.string.permission),
                                    Toast.LENGTH_SHORT).show();
                        }
                    });
        }
        return isGranted;
    }

    private void setClickListener() {
        sp_sort_country.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            try{
                filterData(sp_sort_country.getSelectedItem().toString(),sp_sort_tier.getSelectedItem().toString());
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
//                if (!sp_sort_country.getSelectedItem().toString().equalsIgnoreCase("All Countries"))
//                {
//                    switch_select_all.setEnabled(false);
//                    newExpandeddapter.filterData(sp_sort_country.getSelectedItem().toString());
//                    expandAll();
////            filter(sp_sort_country.getSelectedItem().toString());
////            setUpList();
//                }
////        if (sp_sort_country.getSelectedItem().toString().equalsIgnoreCase("India"))
//                else
//                {
//                    switch_select_all.setEnabled(false);
//                    setUpList();
////            ArrayList<ContactCallModel> filteredList = new ArrayList<>();
////            contactListAdapter.filterList(data);
////            contactListAdapter.notifyDataSetChanged();
//                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        sp_sort_tier.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                try
                {
                filterData(sp_sort_country.getSelectedItem().toString(),sp_sort_tier.getSelectedItem().toString());
//                if (!sp_sort_tier.getSelectedItem().toString().equalsIgnoreCase("All Tier"))
//                {
//                    finalData = new ArrayList<ContactSmsFinalList>();
//                    finalData.add(new ContactSmsFinalList(sp_sort_tier.getSelectedItem().toString(),dbHelperForListForSms.getSmsDataListByTier(sp_sort_tier.getSelectedItem().toString())));
////
//                    newExpandeddapter = new MyExpandableSMSListAdapter(context,finalData,el_contact_list_new);
//
//                    el_contact_list_new.setAdapter(newExpandeddapter);
//                    expandAll();
//                }
//                else
//                {
//                    setUpList();
//                }
//                if (!sp_sort_tier.getSelectedItem().toString().equalsIgnoreCase("All Tier") && sp_sort_tier.getSelectedItem().toString().equalsIgnoreCase("Tier1"))
//                {
//                    ArrayList<CallAndSmsListDataDbo> newfirstTier;
//                    newfirstTier = new ArrayList<>();
//                    newfirstTier.add(new CallAndSmsListDataDbo(R.drawable.f1,"+91 7043169995","India","Tier1"));
//                    newfirstTier.add(new CallAndSmsListDataDbo(R.drawable.f2,"+91 8154936235","India","Tier1"));
//                    newfirstTier.add(new CallAndSmsListDataDbo(R.drawable.f3,"+91 8160058122","America","Tier1"));
//                    newfirstTier.add(new CallAndSmsListDataDbo(R.drawable.f4,"+91 8154936235","Ukraine","Tier1"));
//                    newfirstTier.add(new CallAndSmsListDataDbo(R.drawable.f5,"+91 8160058122","USA","Tier1"));
//
//                    newData = new ArrayList<>();
//                    newData.add(new ContactListCallSmsDboList("Tier1",newfirstTier));
//
//                    newExpandeddapter = new MyExpandableSMSListAdapter(context,newData);
//
//                    el_contact_list_new.setAdapter(newExpandeddapter);
//                    expandAll();
//                }
//                else if (!sp_sort_tier.getSelectedItem().toString().equalsIgnoreCase("All Tier") && sp_sort_tier.getSelectedItem().toString().equalsIgnoreCase("Tier2"))
//                {
//                    ArrayList<CallAndSmsListDataDbo> newSecondTier;
//                    newSecondTier = new ArrayList<>();
//                    newSecondTier.add(new CallAndSmsListDataDbo(R.drawable.f1,"+91 7043169995","India","Tier2"));
//                    newSecondTier.add(new CallAndSmsListDataDbo(R.drawable.f2,"+91 8154936235","India","Tier2"));
//                    newSecondTier.add(new CallAndSmsListDataDbo(R.drawable.f3,"+91 8160058122","America","Tier2"));
//                    newSecondTier.add(new CallAndSmsListDataDbo(R.drawable.f4,"+91 8154936235","Ukraine","Tier2"));
//                    newSecondTier.add(new CallAndSmsListDataDbo(R.drawable.f5,"+91 8160058122","USA","Tier2"));
//
//                    newData = new ArrayList<>();
//                    newData.add(new ContactListCallSmsDboList("Tier2",newSecondTier));
//
//                    newExpandeddapter = new MyExpandableSMSListAdapter(context,newData);
//
//                    el_contact_list_new.setAdapter(newExpandeddapter);
//                    expandAll();
//                }
//                else if  (!sp_sort_tier.getSelectedItem().toString().equalsIgnoreCase("All Tier") && sp_sort_tier.getSelectedItem().toString().equalsIgnoreCase("Tier3"))
//                {
//                    ArrayList<CallAndSmsListDataDbo> newThirdTier;
//                    newThirdTier = new ArrayList<>();
//                    newThirdTier.add(new CallAndSmsListDataDbo(R.drawable.f1,"+91 7043169995","India","Tier3"));
//                    newThirdTier.add(new CallAndSmsListDataDbo(R.drawable.f2,"+91 8154936235","India","Tier3"));
//                    newThirdTier.add(new CallAndSmsListDataDbo(R.drawable.f3,"+91 8160058122","America","Tier3"));
//                    newThirdTier.add(new CallAndSmsListDataDbo(R.drawable.f4,"+91 8154936235","Ukraine","Tier3"));
//                    newThirdTier.add(new CallAndSmsListDataDbo(R.drawable.f5,"+91 8160058122","USA","Tier3"));
//
//                    newData = new ArrayList<>();
//                    newData.add(new ContactListCallSmsDboList("Tier3",newThirdTier));
//
//                    newExpandeddapter = new MyExpandableSMSListAdapter(context,newData);
//
//                    el_contact_list_new.setAdapter(newExpandeddapter);
//                    expandAll();
//                }
//                else if  (!sp_sort_tier.getSelectedItem().toString().equalsIgnoreCase("All Tier") && sp_sort_tier.getSelectedItem().toString().equalsIgnoreCase("Tier4")) {
//                    ArrayList<CallAndSmsListDataDbo> newFourTier;
//                    newFourTier = new ArrayList<>();
//                    newFourTier.add(new CallAndSmsListDataDbo(R.drawable.f1, "+91 7043169995", "India", "Tier4"));
//                    newFourTier.add(new CallAndSmsListDataDbo(R.drawable.f2, "+91 8154936235", "India", "Tier4"));
//                    newFourTier.add(new CallAndSmsListDataDbo(R.drawable.f3, "+91 8160058122", "America", "Tier4"));
//                    newFourTier.add(new CallAndSmsListDataDbo(R.drawable.f4, "+91 8154936235", "Ukraine", "Tier4"));
//                    newFourTier.add(new CallAndSmsListDataDbo(R.drawable.f5, "+91 8160058122", "USA", "Tier4"));
//
//                    newData = new ArrayList<>();
//                    newData.add(new ContactListCallSmsDboList("Tier4",newFourTier));
//
//                    newExpandeddapter = new MyExpandableSMSListAdapter(context,newData);
//
//                    el_contact_list_new.setAdapter(newExpandeddapter);
//                    expandAll();
//                }
//                else
//                {
//                    setUpList();
//                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    private void Collect_phone_number() {
//        String image, String phoneNumber, String country, String tier
        selected_phone_num = new ArrayList<>();
        if (global_data_list_country_wise.size() != 0) {
            for (int i = 0; i < global_data_list_country_wise.size(); i++) {
                ContactSmsFinalList contactSmsFinalList = global_data_list_country_wise.get(i);
                if (contactSmsFinalList.getTier_list().size() != 0) {
                    for (int j = 0; j < contactSmsFinalList.getTier_list().size(); j++) {
                        ContactSmsFinalList contactSmsFinalList1 = contactSmsFinalList.getTier_list().get(j);
                        if (contactSmsFinalList1.getChildList().size() != 0) {
                            for (int k = 0; k < contactSmsFinalList1.getChildList().size(); k++) {
                                SmsListDataDbo smsListDataDbo = contactSmsFinalList1.getChildList().get(k);
                                if (smsListDataDbo.isSelected()) {
//                                    selected_phone_num.add(smsListDataDbo.getPhoneNumber());
                                    selected_phone_num.add(new SmsListDataDbo(smsListDataDbo.getImage(),smsListDataDbo.getPhoneNumber(),smsListDataDbo.getCountry(),smsListDataDbo.getTier()));
                                }
                            }
                        }
                    }
                }
            }
        }
    }


    private void setUpList() {
//        rv_contact_list.setLayoutManager(new LinearLayoutManager(context));
//        rv_contact_list_new.setLayoutManager(new LinearLayoutManager(context));

        hashMap = new HashMap<String, List<ContactCallModel>>();

        final List<String> header;
        header = new ArrayList<>();
        header.add("Tier1");
        header.add("Tier2");
        header.add("Tier3");
        header.add("Tier4");

        List<ContactCallModel> firstTier;
        firstTier = new ArrayList<>();
        firstTier.add(new ContactCallModel(1, R.drawable.f1,"+91 7043169995","India","Tier1"));
        firstTier.add(new ContactCallModel(2, R.drawable.f2,"+91 8154936235","India","Tier1"));
        firstTier.add(new ContactCallModel(3, R.drawable.f3,"+91 8160058122","America","Tier1"));
        firstTier.add(new ContactCallModel(4, R.drawable.f4,"+91 8154936235","Ukraine","Tier1"));
        firstTier.add(new ContactCallModel(5, R.drawable.f5,"+91 8160058122","USA","Tier1"));

        List<ContactCallModel> secondTier;
        secondTier = new ArrayList<>();
        secondTier.add(new ContactCallModel(6, R.drawable.f1,"+91 8154936235","India","Tier2"));
        secondTier.add(new ContactCallModel(7, R.drawable.f2,"+91 7043169995","India","Tier2"));
        secondTier.add(new ContactCallModel(8, R.drawable.f3,"+91 8154936235","Ukraine","Tier2"));
        secondTier.add(new ContactCallModel(9, R.drawable.f4,"+91 8160058122","USA","Tier2"));
        secondTier.add(new ContactCallModel(10, R.drawable.f5,"+91 8154936235","India","Tier2"));

        ArrayList<CallAndSmsListDataDbo> newfirstTier;
        newfirstTier = new ArrayList<>();
        newfirstTier.add(new CallAndSmsListDataDbo(R.drawable.f1,"+91 7043169995","India","Tier1"));
        newfirstTier.add(new CallAndSmsListDataDbo(R.drawable.f2,"+91 8154936235","India","Tier1"));
        newfirstTier.add(new CallAndSmsListDataDbo(R.drawable.f3,"+91 8160058122","America","Tier1"));
        newfirstTier.add(new CallAndSmsListDataDbo(R.drawable.f4,"+91 8154936235","Ukraine","Tier1"));
        newfirstTier.add(new CallAndSmsListDataDbo(R.drawable.f5,"+91 8160058122","USA","Tier1"));

        ArrayList<CallAndSmsListDataDbo> newSecondTier;
        newSecondTier = new ArrayList<>();
        newSecondTier.add(new CallAndSmsListDataDbo(R.drawable.f1,"+91 7043169995","India","Tier2"));
        newSecondTier.add(new CallAndSmsListDataDbo(R.drawable.f2,"+91 8154936235","India","Tier2"));
        newSecondTier.add(new CallAndSmsListDataDbo(R.drawable.f3,"+91 8160058122","America","Tier2"));
        newSecondTier.add(new CallAndSmsListDataDbo(R.drawable.f4,"+91 8154936235","Ukraine","Tier2"));
        newSecondTier.add(new CallAndSmsListDataDbo(R.drawable.f5,"+91 8160058122","USA","Tier2"));

        ArrayList<CallAndSmsListDataDbo> newThirdTier;
        newThirdTier = new ArrayList<>();
        newThirdTier.add(new CallAndSmsListDataDbo(R.drawable.f1,"+91 7043169995","India","Tier3"));
        newThirdTier.add(new CallAndSmsListDataDbo(R.drawable.f2,"+91 8154936235","India","Tier3"));
        newThirdTier.add(new CallAndSmsListDataDbo(R.drawable.f3,"+91 8160058122","America","Tier3"));
        newThirdTier.add(new CallAndSmsListDataDbo(R.drawable.f4,"+91 8154936235","Ukraine","Tier3"));
        newThirdTier.add(new CallAndSmsListDataDbo(R.drawable.f5,"+91 8160058122","USA","Tier3"));


        ArrayList<CallAndSmsListDataDbo> newFourTier;
        newFourTier = new ArrayList<>();
        newFourTier.add(new CallAndSmsListDataDbo(R.drawable.f1,"+91 7043169995","India","Tier4"));
        newFourTier.add(new CallAndSmsListDataDbo(R.drawable.f2,"+91 8154936235","India","Tier4"));
        newFourTier.add(new CallAndSmsListDataDbo(R.drawable.f3,"+91 8160058122","America","Tier4"));
        newFourTier.add(new CallAndSmsListDataDbo(R.drawable.f4,"+91 8154936235","Ukraine","Tier4"));
        newFourTier.add(new CallAndSmsListDataDbo(R.drawable.f5,"+91 8160058122","USA","Tier4"));

        newData = new ArrayList<>();
        newData.add(new ContactListCallSmsDboList("Tier1",newfirstTier));
        newData.add(new ContactListCallSmsDboList("Tier2",newSecondTier));
        newData.add(new ContactListCallSmsDboList("Tier3",newThirdTier));
        newData.add(new ContactListCallSmsDboList("Tier4",newFourTier));

        hashMap.put("Tier1", firstTier);
        hashMap.put("Tier2", secondTier);
        hashMap.put("Tier3", firstTier);
        hashMap.put("Tier4", secondTier);

        int count = dbHelperForListForSms.getSmsCount();
        Log.d(TAG,"Sms list count "+dbHelperForListForSms.getSmsCount());

        if (count > 0)
        {
            finalData = new ArrayList<>();
            finalHeaderData = new ArrayList<>();
            finalHeaderData = dbHelperForSmsTierNameDetails.getAllSmsTierName();

            for (int i=0;i < finalHeaderData.size();i++)
            {
                finalData.add(new ContactSmsFinalList(finalHeaderData.get(i),dbHelperForListForSms.getSmsDataListByTier(finalHeaderData.get(i))));
            }
            Log.e(TAG,"final data size: "+finalData.size());
            pos = null;
            newExpandeddapter = new MyExpandableSMSListAdapter(context, finalData, el_contact_list_new, SMSFragment.this);
            el_contact_list_new.setAdapter(newExpandeddapter);

            Set_all_data_of_tiers(finalData);
        }
        else {

        }

        el_contact_list_new.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView expandableListView, View view, int groupPosition, int childPosition, long l) {
//                int index = view.getFlatListPosition(ExpandableListView.getPackedPositionForChild(groupPosition, childPosition));
//                view.setItemChecked(index, true);
//                int index = parent.getFlatListPosition(ExpandableListView.getPackedPositionForChild(groupPosition, childPosition));
//                parent.setItemChecked(index, true);
                view.setSelected(true);
                return false;
            }
        });


        el_contact_list_new.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView expandableListView, View view, int groupPosition, long l) {
                if (expandableListView.isGroupExpanded(groupPosition)) {
                    expandableListView.collapseGroup(groupPosition);
                } else {
                    expandableListView.expandGroup(groupPosition);
                }
                return false;
            }
        });

        el_contact_list_new.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                if (pos != null) {
                    if (pos == position) {
                        pos = null;
                        Toast.makeText(context, "All contacts are unselect of this Tier", Toast.LENGTH_SHORT).show();
                    } else {
                        pos = position;
                        Toast.makeText(context, "All contacts are select of this Tier", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    pos = position;
                    Toast.makeText(context, "All contacts are select of this Tier", Toast.LENGTH_SHORT).show();
                }
                return false;
            }
        });

//        newExpandeddapter = new MyExpandableSMSListAdapter(context,newData);
//
//        el_contact_list_new.setAdapter(newExpandeddapter);


//        hashMap.put(header.get(1), child2);
//        hashMap.put(header.get(2), child3);
//        hashMap.put(header.get(3), child4);

//        expandableSMSListAdapter = new ExpandableSMSListAdapter(context, header, hashMap);
//        // Setting adpater for expandablelistview
//        el_contact_list_new.setAdapter(expandableSMSListAdapter);
//
//          /*
//        You can add listeners for the item clicks
//         */
//        el_contact_list_new.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
//
//            @Override
//            public boolean onGroupClick(ExpandableListView parent, View v,
//                                        int groupPosition, long id) {
//                return false;
//            }
//        });
//
//        // Listview Group expanded listener
//        el_contact_list_new.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
//
//            @Override
//            public void onGroupExpand(int groupPosition) {
////                Toast.makeText(context,
////                        header.get(groupPosition) + " Expanded",
////                        Toast.LENGTH_SHORT).show();
//            }
//        });
//
//        // Listview Group collasped listener
//        el_contact_list_new.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {
//
//            @Override
//            public void onGroupCollapse(int groupPosition) {
////                Toast.makeText(context,
////                        header.get(groupPosition) + " Collapsed",
////                        Toast.LENGTH_SHORT).show();
//
//            }
//        });
//
//        // Listview on child click listener
//        el_contact_list_new.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
//
//            @Override
//            public boolean onChildClick(ExpandableListView parent, View v,
//                                        int groupPosition, int childPosition, long id) {
//
////                Toast.makeText(
////                        context,
////                        header.get(groupPosition)
////                                + " : "
////                                + hashMap.get(
////                                header.get(groupPosition)).get(
////                                childPosition), Toast.LENGTH_SHORT)
////                        .show();
//                return false;
//            }
//        });

//        data = new ArrayList<ContactCallModel>();
//        data.add(new ContactCallModel(1,"+91 7043169995","India"));
//        data.add(new ContactCallModel(2,"+91 8154936235","India"));
//        data.add(new ContactCallModel(3,"+91 8160058122","America"));
//        data.add(new ContactCallModel(4,"+91 8154936235","Ukraine"));
//        data.add(new ContactCallModel(5,"+91 8160058122","USA"));
//        data.add(new ContactCallModel(6,"+91 8154936235","India"));
//        data.add(new ContactCallModel(7,"+91 7043169995","India"));
//        data.add(new ContactCallModel(8,"+91 8154936235","Ukraine"));
//        data.add(new ContactCallModel(9,"+91 8160058122","USA"));
//        data.add(new ContactCallModel(10,"+91 8154936235","India"));
//
//        contactListAdapter = new CallFragment.ContactListAdapter(context,data);
//
//        Log.e("Before Sorting:",data.get(0).getContactNumber());
//
//        rv_contact_list.setAdapter(contactListAdapter);

//        Collections.sort(callModelList, new Comparator<CallModel>() {
//            public int compare(CallModel v1, CallModel v2) {
//                return v1.getCountry().compareTo(v2.getCountry());
//            }
//        });
//
//        Log.e("Sorting:",callModelList.get(0).getCountry());


//        callModelList.sort();
    }



    private void setUpSpinners() {
        try {
            List<String> countryNames = new ArrayList<>();
            countryNames = dbHelperForSMSCountryNameDetails.getAllSmsCountryName();
            List<String> spinnerChooseSortBy = new ArrayList<>();
            spinnerChooseSortBy.add("All Countries");
            spinnerChooseSortBy.addAll(countryNames);
            adapterChooseSortBy = new ArrayAdapter<String>((BaseActivity) getActivity(), android.R.layout.simple_spinner_item, spinnerChooseSortBy);

            adapterChooseSortBy.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//       Spinner sItems = (Spinner) findViewById(R.id.spinner1);
            sp_sort_country.setAdapter(adapterChooseSortBy);

//        sp_sort_country.setOnItemSelectedListener(this);

            Log.e(TAG, "size of database tier list: " + finalHeaderData.size());
            List<String> spinnerChooseSortByTier = new ArrayList<>();
            spinnerChooseSortByTier.add("All Tier");
//        spinnerChooseSortByTier = dbHelperForCallTierNameDetails.getAllCallTierName();
//        for (int j=0;j < finalHeaderData.size(); j++)
//        {
            spinnerChooseSortByTier.addAll(finalHeaderData);
//        }
            Log.e(TAG, "size of tier list: " + spinnerChooseSortByTier.size());


            adapterChooseSortByTier = new ArrayAdapter<String>((BaseActivity) getActivity(), android.R.layout.simple_spinner_item, spinnerChooseSortByTier);

            adapterChooseSortByTier.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//       Spinner sItems = (Spinner) findViewById(R.id.spinner1);
            sp_sort_tier.setAdapter(adapterChooseSortByTier);

//        sp_sort_tier.setOnItemSelectedListener(this);

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

//    @Override
//    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//
//    }
//    @Override
//    public void onNothingSelected(AdapterView<?> adapterView) {
//
//    }

    private void expandAll() {
        int count = newExpandeddapter.getGroupCount();
        for (int i = 0; i < count; i++) {
            el_contact_list_new.expandGroup(i);
        } //end for (int i = 0; i < count; i++)
    }

    private void filterData(String countryData, String tierData) {

        current_selected_country = countryData;
        current_selected_tier = tierData;
        Set_filtered_data(countryData, tierData);
//        try {
//
//            if (countryData.equalsIgnoreCase("All Countries") && tierData.equalsIgnoreCase("All Tier")) {
//                setUpList();
//            } else if (!(countryData.equalsIgnoreCase("All Countries")) && tierData.equalsIgnoreCase("All Tier")) {
//                setUpList();
////                filter(sp_sort_country.getSelectedItem().toString());
//                newExpandeddapter.filterData(sp_sort_country.getSelectedItem().toString());
//                expandAll();
//            } else if (!(countryData.equalsIgnoreCase("All Countries")) && !(tierData.equalsIgnoreCase("All Tier"))) {
//                finalData = new ArrayList<>();
//                finalData.add(new ContactSmsFinalList(sp_sort_tier.getSelectedItem().toString(), dbHelperForListForSms.getSmsDataListByTier(sp_sort_tier.getSelectedItem().toString())));
////
//                newExpandeddapter = new MyExpandableSMSListAdapter(context, finalData, el_contact_list_new);
////                filter(sp_sort_country.getSelectedItem().toString());
//                newExpandeddapter.filterData(sp_sort_country.getSelectedItem().toString());
//                el_contact_list_new.setAdapter(newExpandeddapter);
//
//                expandAll();
//            }else if ((countryData.equalsIgnoreCase("All Countries")) && !(tierData.equalsIgnoreCase("All Tier"))) {
//                finalData = new ArrayList<>();
//                finalData.add(new ContactSmsFinalList(sp_sort_tier.getSelectedItem().toString(), dbHelperForListForSms.getSmsDataListByTier(sp_sort_tier.getSelectedItem().toString())));
////
//                newExpandeddapter = new MyExpandableSMSListAdapter(context, finalData, el_contact_list_new);
//
//                el_contact_list_new.setAdapter(newExpandeddapter);
////                filter(sp_sort_country.getSelectedItem().toString());
////                newExpandeddapter.filterData(sp_sort_country.getSelectedItem().toString());
//                expandAll();
//            }
//        }
//        catch (Exception e)
//        {
//            e.printStackTrace();
//        }
    }

    @Subscribe
    public void onEventMainThread(ApiErrorEvent event) {
        switch (event.getRequestTag()) {
            case SMS_LIST_TAG:
                Log.e(":::","Error"+event.getRequestTag());
                dismissProgress();
                break;
            default:
                break;
        }
    }


    @Subscribe
    public void onEventMainThread(ApiErrorWithMessageEvent event) {
        switch (event.getRequestTag()) {
            case SMS_LIST_TAG:
                dismissProgress();
                Log.e(":::",""+event.getResultMsgUser());
                break;
            default:
                break;
        }
    }

    @Subscribe
    public void onEventMainThread(SmsListResponse response) {
        switch (response.getRequestTag()) {
            case SMS_LIST_TAG:
                Log.e(TAG, "Response is: " + response);
                Log.e(TAG, "Response status: " + response.getStatus());
                storeDataInDatabase(response.getData(),response.getPath());
                break;
            default:
                break;
        }
    }

    private void Set_filtered_data(String countryData, String tierData) {
        if (countryData.equals("All Countries") && tierData.equals("All Tier")) {
            el_contact_list_new.setAdapter(new MyExpandableSMSListAdapter(context, global_data_list_all_country.get(0).getTier_list(), el_contact_list_new, SMSFragment.this));
        } else {
            ArrayList<ContactSmsFinalList> temp_data = new ArrayList<>();

            if (countryData.equals("All Countries") && !tierData.equals("All Tier")) {
                if (global_data_list_all_country.size() != 0) {
                    for (int i = 0; i < global_data_list_all_country.size(); i++) {
                        ContactSmsFinalList contactSmsFinalList = global_data_list_all_country.get(i);
                        if (contactSmsFinalList.getTier_list().size() != 0) {
                            for (int j = 0; j < contactSmsFinalList.getTier_list().size(); j++) {
                                ContactSmsFinalList contactSmsFinalList1 = contactSmsFinalList.getTier_list().get(j);
                                if (contactSmsFinalList1.getName().equals(tierData)) {
                                    temp_data.add(contactSmsFinalList1);
                                    break;
                                }
                            }
                        }
                    }
                }
            }

            if (!countryData.equals("All Countries") && tierData.equals("All Tier")) {
                if (global_data_list_country_wise.size() != 0) {
                    for (int i = 0; i < global_data_list_country_wise.size(); i++) {
                        ContactSmsFinalList contactSmsFinalList = global_data_list_country_wise.get(i);
                        if (contactSmsFinalList.getCountry_name().equals(countryData)) {
                            temp_data.addAll(contactSmsFinalList.getTier_list());
                        }
                    }
                }
            }

            if (!countryData.equals("All Countries") && !tierData.equals("All Tier")) {
                if (global_data_list_country_wise.size() != 0) {
                    for (int i = 0; i < global_data_list_country_wise.size(); i++) {
                        ContactSmsFinalList contactSmsFinalList = global_data_list_country_wise.get(i);
                        if (contactSmsFinalList.getCountry_name().equals(countryData)) {
                            if (contactSmsFinalList.getTier_list().size() != 0) {
                                for (int j = 0; j < contactSmsFinalList.getTier_list().size(); j++) {
                                    ContactSmsFinalList contactSmsFinalList1 = contactSmsFinalList.getTier_list().get(j);
                                    if (contactSmsFinalList1.getName().equals(tierData)) {
                                        temp_data.add(contactSmsFinalList1);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }


            if (temp_data.size() != 0) {
                newExpandeddapter = new MyExpandableSMSListAdapter(context, temp_data, el_contact_list_new, SMSFragment.this);
//                newExpandeddapter.filterData(sp_sort_country.getSelectedItem().toString());
                el_contact_list_new.setAdapter(newExpandeddapter);
            } else {
                temp_data = new ArrayList<>();
                el_contact_list_new.setAdapter(new MyExpandableSMSListAdapter(context, temp_data, el_contact_list_new, SMSFragment.this));
            }
//            expandAll();
        }
    }

//    private void filter(String text) {
//
//        try{
//            ArrayList<ContactSmsFinalList> filteredList = new ArrayList<>();
//
//            for (ContactSmsFinalList item : finalData)
//            {
//                if (item.getContactCountry().toLowerCase().contains(text.toLowerCase()))
//                {
//                    filteredList.add(item);
//                }
//            }
//
//            contactListAdapter.filterList(filteredList);
//        }
//        catch (Exception e)
//        {
//            e.printStackTrace();
//        }
//
//    }

    private void storeDataInDatabase(List<SmsListResponseData> data, String path) {
        Log.e(TAG,"list data size: "+data.size());
        dbHelperForListForSms.deleteSMSList();
        dbHelperForSmsTierNameDetails.deleteSmsTierNames();
        dbHelperForSMSCountryNameDetails.deleteSmsCountryNames();
        for(int i=0;i < data.size();i++)
        {
            Log.e(TAG,"list data size: "+data.size());
//            int image, String phoneNumber, String country, String tier
            Log.e(TAG,"sms list database size: "+dbHelperForListForSms.getSmsCount());
            String countryImage = path + data.get(i).getCountryImage();
            Log.e(TAG,"CountryImage: "+countryImage);
            dbHelperForListForSms.insertSmsListDetails(new SmsListDataDbo(countryImage,data.get(i).getNumber(),data.get(i).getCountry(),data.get(i).getTier(),false));
            dbHelperForSMSCountryNameDetails.insertSmsCountryName(data.get(i).getCountry());
            dbHelperForSmsTierNameDetails.insertSmsTierName(data.get(i).getTier());
            Log.e(TAG,"Size of Country names list: "+dbHelperForSMSCountryNameDetails.getSmsCountryNameCount());
            Log.e(TAG,"Size of tier names list: "+dbHelperForSmsTierNameDetails.getSmsTierNameCount());
            Log.e(TAG,"call list database size: "+dbHelperForListForSms.getSmsCount());
            Log.e(TAG,"Size of "+data.get(0).getTier() + " is "+ dbHelperForListForSms.getSmsTierCount(data.get(0).getTier()));
        }
        setUpList();
        setUpSpinners();
        dismissProgress();
    }

    private void Set_all_data_of_tiers(ArrayList<ContactSmsFinalList> finalData) {
        if (finalData.size() != 0) {
            global_data_list_country_wise = new ArrayList<>();
            global_data_list_all_country = new ArrayList<>();
            for (int i = 0; i < finalData.size(); i++) {
                ContactSmsFinalList contactSmsFinalList = finalData.get(i);
                if (contactSmsFinalList.getChildList().size() != 0) {
                    for (int j = 0; j < contactSmsFinalList.getChildList().size(); j++) {
                        SmsListDataDbo child_object = contactSmsFinalList.getChildList().get(j);
                        Add_to_list(child_object);
                        Add_to_all_data_list(child_object);
                    }
                }
            }
        }
    }

    private void Add_to_list(SmsListDataDbo child_object) {
        if (global_data_list_country_wise.size() != 0) {
            boolean match_country = false;
            for (int i = 0; i < global_data_list_country_wise.size(); i++) {
                ContactSmsFinalList contactSmsFinalList = global_data_list_country_wise.get(i);
                if (contactSmsFinalList.getCountry_name().equals(child_object.getCountry())) {
                    if (contactSmsFinalList.getTier_list().size() != 0) {
                        boolean match_tier = false;
                        for (int j = 0; j < contactSmsFinalList.getTier_list().size(); j++) {
                            ContactSmsFinalList contactCallFinalList1 = contactSmsFinalList.getTier_list().get(j);
                            if (contactCallFinalList1.getName().equals(child_object.getTier())) {
                                if (contactCallFinalList1.getChildList().size() != 0) {
                                    boolean match_child_objejct = false;
                                    for (int k = 0; k < contactCallFinalList1.getChildList().size(); k++) {
                                        SmsListDataDbo callListDataDbo = contactCallFinalList1.getChildList().get(k);
                                        if (callListDataDbo.getPhoneNumber().equals(child_object.getPhoneNumber())) {
                                            match_child_objejct = true;
                                            break;
                                        }
                                    }
                                    if (!match_child_objejct) {
                                        child_object.setSelected(false);
                                        contactCallFinalList1.getChildList().add(child_object);
                                    }
                                }
                                match_tier = true;
                                break;
                            }
                        }
                        if (!match_tier) {
                            ArrayList<SmsListDataDbo> childList = new ArrayList<>();
                            child_object.setSelected(false);
                            childList.add(child_object);
                            ContactSmsFinalList contactCallFinalList1 = new ContactSmsFinalList(child_object.getTier(), childList, false);
                            contactSmsFinalList.getTier_list().add(contactCallFinalList1);
                        }
                    }
                    match_country = true;
                    break;
                }
            }
            if (!match_country) {
                ArrayList<SmsListDataDbo> childList = new ArrayList<>();
                child_object.setSelected(false);
                childList.add(child_object);
                ContactSmsFinalList contactSmsFinalList = new ContactSmsFinalList(child_object.getTier(), childList, false);
                ArrayList<ContactSmsFinalList> tier_list = new ArrayList<>();
                tier_list.add(contactSmsFinalList);
                global_data_list_country_wise.add(new ContactSmsFinalList(tier_list, child_object.getCountry()));
            }
        } else {
            ArrayList<SmsListDataDbo> childList = new ArrayList<>();
            child_object.setSelected(false);
            childList.add(child_object);
            ContactSmsFinalList contactSmsFinalList = new ContactSmsFinalList(child_object.getTier(), childList, false);
            ArrayList<ContactSmsFinalList> tier_list = new ArrayList<>();
            tier_list.add(contactSmsFinalList);
            global_data_list_country_wise.add(new ContactSmsFinalList(tier_list, child_object.getCountry()));
        }
    }

    private void Add_to_all_data_list(SmsListDataDbo child_object) {
        if (global_data_list_all_country.size() != 0) {
            for (int i = 0; i < global_data_list_all_country.size(); i++) {
                ContactSmsFinalList contactSmsFinalList = global_data_list_all_country.get(i);
                if (contactSmsFinalList.getTier_list().size() != 0) {
                    boolean match_tier = false;
                    for (int j = 0; j < contactSmsFinalList.getTier_list().size(); j++) {
                        ContactSmsFinalList contactCallFinalList1 = contactSmsFinalList.getTier_list().get(j);
                        if (contactCallFinalList1.getName().equals(child_object.getTier())) {
                            if (contactCallFinalList1.getChildList().size() != 0) {
                                boolean match_child_objejct = false;
                                for (int k = 0; k < contactCallFinalList1.getChildList().size(); k++) {
                                    SmsListDataDbo callListDataDbo = contactCallFinalList1.getChildList().get(k);
                                    if (callListDataDbo.getPhoneNumber().equals(child_object.getPhoneNumber())) {
                                        match_child_objejct = true;
                                        break;
                                    }
                                }
                                if (!match_child_objejct) {
                                    child_object.setSelected(false);
                                    contactCallFinalList1.getChildList().add(child_object);
                                }
                            }
                            match_tier = true;
                            break;
                        }
                    }
                    if (!match_tier) {
                        ArrayList<SmsListDataDbo> childList = new ArrayList<>();
                        child_object.setSelected(false);
                        childList.add(child_object);
                        ContactSmsFinalList contactCallFinalList1 = new ContactSmsFinalList(child_object.getTier(), childList, false);
                        contactSmsFinalList.getTier_list().add(contactCallFinalList1);
                    }
                }
            }
        } else {
            ArrayList<SmsListDataDbo> childList = new ArrayList<>();
            child_object.setSelected(false);
            childList.add(child_object);
            ContactSmsFinalList contactCallFinalList = new ContactSmsFinalList(child_object.getTier(), childList, false);
            ArrayList<ContactSmsFinalList> tier_list = new ArrayList<>();
            tier_list.add(contactCallFinalList);
            global_data_list_all_country.add(new ContactSmsFinalList(tier_list, "All Countries"));
        }
    }

    @Override
    public void Add_remove(String current_selected_country, String tier_name, String
            action, String phoneNumber) {
        if (current_selected_country.equals("All Countries")) {
            if (countryNames_all.size() != 0) {
                Set_selected_in_country_data_array(current_selected_country, tier_name, action, phoneNumber, global_data_list_all_country, "ALL");
                for (int i = 0; i < countryNames_all.size(); i++) {
                    Set_selected_in_country_data_array(countryNames_all.get(i), tier_name, action, phoneNumber, global_data_list_country_wise, "COUNTRY");
                }
            }
        } else {
            Set_selected_in_country_data_array(current_selected_country, tier_name, action, phoneNumber, global_data_list_country_wise, "COUNTRY");
        }
    }

    private void Set_selected_in_country_data_array(String current_selected_country, String tier_name, String action, String phoneNumber, ArrayList<ContactSmsFinalList> finalData, String from) {
        if (finalData.size() != 0) {
            for (int i = 0; i < finalData.size(); i++) {
                ContactSmsFinalList contactCallFinalList = finalData.get(i);
                if (contactCallFinalList.getCountry_name().equals(current_selected_country)) {
                    if (contactCallFinalList.getTier_list().size() != 0) {
                        for (int j = 0; j < contactCallFinalList.getTier_list().size(); j++) {
                            ContactSmsFinalList contactCallFinalList1 = contactCallFinalList.getTier_list().get(j);
                            if (contactCallFinalList1.getName().equals(tier_name)) {
                                if (contactCallFinalList1.getChildList().size() != 0) {
                                    if (!phoneNumber.equals("")) {
                                        for (int k = 0; k < contactCallFinalList1.getChildList().size(); k++) {
                                            SmsListDataDbo callListDataDbo = contactCallFinalList1.getChildList().get(k);
                                            if (callListDataDbo.getPhoneNumber().equals(phoneNumber)) {
                                                if (action.equals("ADD")) {
                                                    callListDataDbo.setSelected(true);
                                                } else {
                                                    callListDataDbo.setSelected(false);
                                                }
                                                contactCallFinalList1.getChildList().remove(k);
                                                contactCallFinalList1.getChildList().add(k, callListDataDbo);
                                                break;
                                            }
                                        }
                                    } else {
                                        for (int k = 0; k < contactCallFinalList1.getChildList().size(); k++) {
                                            SmsListDataDbo callListDataDbo = contactCallFinalList1.getChildList().get(k);
                                            if (action.equals("ADD")) {
                                                callListDataDbo.setSelected(true);
                                            } else {
                                                callListDataDbo.setSelected(false);
                                            }
                                            contactCallFinalList1.getChildList().remove(k);
                                            contactCallFinalList1.getChildList().add(k, callListDataDbo);
                                        }

                                        if (action.equals("ADD")) {
                                            contactCallFinalList1.setSelected_tier(true);
                                        } else {
                                            contactCallFinalList1.setSelected_tier(false);
                                        }
                                    }
                                }
                                break;
                            }
                        }
                    }
                    break;
                }
            }

            if (from.equals("ALL")) {
                global_data_list_all_country = finalData;
            } else {
                global_data_list_country_wise = finalData;
            }

            selected_phone_num = new ArrayList<>();
//            selected_phone_num.add("asda");
            newExpandeddapter.notifyDataSetChanged();
            Collect_phone_number();
        }
    }

    private String getDate(long time) {
        Date date = new Date(time); // *1000 is to convert seconds to milliseconds
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH); // the format of your date
        return sdf.format(date);
    }

    private String getTimeStampDate(long time) {
        Date date = new Date(time); // *1000 is to convert seconds to milliseconds
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH); // the format of your date
        return sdf.format(date);
    }
}
