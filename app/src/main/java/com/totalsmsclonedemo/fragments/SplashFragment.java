package com.totalsmsclonedemo.fragments;


import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.totalsmsclonedemo.R;
import com.totalsmsclonedemo.activities.BaseActivity;
import com.totalsmsclonedemo.events.ApiErrorEvent;
import com.totalsmsclonedemo.events.ApiErrorWithMessageEvent;

import org.greenrobot.eventbus.Subscribe;

/**
 * A simple {@link Fragment} subclass.
 */
public class SplashFragment extends BaseFragment {

    View v;
    Context context;

    @Override
    public void setToolbarForFragment() {
        ((BaseActivity) getActivity()).getToolbar().setVisibility(View.GONE);
        ((BaseActivity) getActivity()).getLl_toolbar().setVisibility(View.GONE);
        ((BaseActivity) getActivity()).getToolbartext().setVisibility(View.GONE);
    }

    @Override
    public void setTabLayout() {
        ((BaseActivity) getActivity()).getLl_view_tab().setVisibility(View.GONE);
//        ((BaseActivity) getActivity()).getTabLayout().setVisibility(View.GONE);
    }

    public SplashFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v =  inflater.inflate(R.layout.fragment_splash, container, false);
        context = (BaseActivity)getActivity();
//        Intent i = new Intent(context, CallDataStoreService.class);
//        context.startService(i);
//        Intent i2 = new Intent(context, SmsDataStoreService.class);
//        context.startService(i2);

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                try {



                    mainInterfaces.OpenDashBoard();

//                    if (Utilities.getIsTermsAndConditions((BaseActivity)getActivity())) {
//                        try
//                        {
//                            mainInterfaces.OpenDashBoard();
//                        }
//                        catch (Exception e)
//                        {
//                            e.printStackTrace();
//                        }
//                    }
//                    else if (!(Utilities.getLicenceKey((BaseActivity)getActivity()) == null) && !Utilities.getIsTermsAndConditions((BaseActivity)getActivity()))
//                    {
//                        try
//                        {
//                            mainInterfaces.OpenTermsAndConditions();
//                        }
//                        catch (Exception e)
//                        {
//                            e.printStackTrace();
//                        }
//                    }
//                    else
//                    {
//                        mainInterfaces.OpenLogin_Screen();
//                    }
                }catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, 3000);

        return v;
    }

    @Subscribe
    public void onEventMainThread(ApiErrorEvent event) {
        switch (event.getRequestTag()) {
//            case LOGTAG:
//                Log.e(":::","Error"+event.getRequestTag());
//                dismissProgress();
//                break;
            default:
                break;
        }
    }


    @Subscribe
    public void onEventMainThread(ApiErrorWithMessageEvent event) {
        switch (event.getRequestTag()) {
//            case LOGTAG:
//                dismissProgress();
//                Log.e(":::",""+event.getResultMsgUser());
//                break;
            default:
                break;
        }
    }

}
