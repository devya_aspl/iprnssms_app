package com.totalsmsclonedemo.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.totalsmsclonedemo.R;
import com.totalsmsclonedemo.activities.BaseActivity;
import com.totalsmsclonedemo.adapters.SmsSummaryAdapter;
import com.totalsmsclonedemo.databases.DBHelperForSmsSummary;
import com.totalsmsclonedemo.dbo.SMSSummaryDbo;
import com.totalsmsclonedemo.events.ApiErrorEvent;
import com.totalsmsclonedemo.events.ApiErrorWithMessageEvent;

import org.greenrobot.eventbus.Subscribe;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class SmsSummaryFragment extends BaseFragment {

    View v;
    Unbinder unbinder;
    Context context;

    @BindView(R.id.rv_sms_summary_list)
    RecyclerView rv_sms_summary_list;

    int count = 0;

    SmsSummaryAdapter smsSummaryAdapter;
    DBHelperForSmsSummary dbHelperForSmsSummary;
    public static final String TAG = SmsSummaryFragment.class.getSimpleName();

    List<SMSSummaryDbo> data;

    @Override
    public void setToolbarForFragment() {
        ((BaseActivity) getActivity()).getToolbar().setVisibility(View.VISIBLE);
        ((BaseActivity) getActivity()).getLl_toolbar().setVisibility(View.VISIBLE);
        ((BaseActivity) getActivity()).getToolbartext().setVisibility(View.VISIBLE);
        ((BaseActivity) getActivity()).getToolbartext().setText("SMS Test Summary");

        ((BaseActivity)getActivity()).getImgToolBarBack().setVisibility(View.VISIBLE);
        ((BaseActivity)getActivity()).getImgToolBarBack().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mainInterfaces.OpenDashBoard();
            }
        });
    }

    @Override
    public void setTabLayout() {

    }

    public SmsSummaryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v =  inflater.inflate(R.layout.fragment_sms_summary, container, false);
        context = (BaseActivity)getActivity();
        unbinder = ButterKnife.bind(this, v);
        dbHelperForSmsSummary = new DBHelperForSmsSummary((BaseActivity)getContext());
        count = dbHelperForSmsSummary.getSmsSummaryCount();
        Log.e(TAG,"SMS Summary Count is: "+count);

        if (count > 0) {
            data = dbHelperForSmsSummary.getSmsSummaryData();

            try {
//                for (int i = 0; i < data.size(); i++) {
//
//                }
                rv_sms_summary_list.setLayoutManager(new LinearLayoutManager(context));
                smsSummaryAdapter = new SmsSummaryAdapter(context,data);
                rv_sms_summary_list.setAdapter(smsSummaryAdapter);
            }
                catch(Exception e)
                {

                }
        }

        return v;
    }

    @Subscribe
    public void onEventMainThread(ApiErrorEvent event) {
        switch (event.getRequestTag()) {
//            case LOGTAG:
//                Log.e(":::","Error"+event.getRequestTag());
//                dismissProgress();
//                break;
            default:
                break;
        }
    }


    @Subscribe
    public void onEventMainThread(ApiErrorWithMessageEvent event) {
        switch (event.getRequestTag()) {
//            case LOGTAG:
//                dismissProgress();
//                Log.e(":::",""+event.getResultMsgUser());
//                break;
            default:
                break;
        }
    }

}
