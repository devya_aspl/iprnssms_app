package com.totalsmsclonedemo.fragments;


import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.CallLog;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.totalsmsclonedemo.R;
import com.totalsmsclonedemo.activities.BaseActivity;
import com.totalsmsclonedemo.adapters.CallSummaryAdapter;
import com.totalsmsclonedemo.databases.DBHelperForCallSummary;
import com.totalsmsclonedemo.databases.DBHelperForCallSummaryForPhoneTime;
import com.totalsmsclonedemo.dbo.CallSummaryDbo;
import com.totalsmsclonedemo.events.ApiErrorEvent;
import com.totalsmsclonedemo.events.ApiErrorWithMessageEvent;

import org.greenrobot.eventbus.Subscribe;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class CallSummaryFragment extends BaseFragment {

    View v;
    Unbinder unbinder;
    Context context;

    @BindView(R.id.rv_call_summary)
    RecyclerView rv_call_summary;

    int count = 0;

    CallSummaryAdapter callSummaryAdapter;
    DBHelperForCallSummary dbHelperForCallSummary;
    DBHelperForCallSummaryForPhoneTime dbHelperForCallSummaryForPhoneTime;
    public static final String TAG = CallSummaryFragment.class.getSimpleName();

    List<CallSummaryDbo> data;


    @Override
    public void setToolbarForFragment() {
        ((BaseActivity) getActivity()).getToolbar().setVisibility(View.VISIBLE);
        ((BaseActivity) getActivity()).getLl_toolbar().setVisibility(View.VISIBLE);
        ((BaseActivity) getActivity()).getToolbartext().setVisibility(View.VISIBLE);
        ((BaseActivity) getActivity()).getToolbartext().setText("Call Test Summary");

        ((BaseActivity)getActivity()).getImgToolBarBack().setVisibility(View.VISIBLE);
        ((BaseActivity)getActivity()).getImgToolBarBack().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mainInterfaces.OpenDashBoard();
            }
        });
    }

    @Override
    public void setTabLayout() {

    }

    public CallSummaryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_call_summary, container, false);
        context = (BaseActivity)getActivity();
        initDialog();
        unbinder = ButterKnife.bind(this, v);
        dbHelperForCallSummary = new DBHelperForCallSummary((BaseActivity)getContext());
        dbHelperForCallSummaryForPhoneTime = new DBHelperForCallSummaryForPhoneTime((BaseActivity)getContext());

        int countCallSummary = dbHelperForCallSummaryForPhoneTime.getCallSummaryCountDemo();
        Log.e(TAG, "Size of call Summary Demo: " + dbHelperForCallSummaryForPhoneTime.getCallSummaryCountDemo());

//        new callListAsync().execute();

        try {
            showProgress();

//        showProgress();
            if (countCallSummary > 0) {
                List<CallSummaryDbo> callSummaryDboList = new ArrayList<CallSummaryDbo>();
                callSummaryDboList = dbHelperForCallSummaryForPhoneTime.getCallSummaryDataDemo();

                for (int i = 0; i < callSummaryDboList.size(); i++) {

                    String phoneNumberCompare = callSummaryDboList.get(i).getCallNumber();
                    phoneNumberCompare = phoneNumberCompare.replace(" ", "");

                    String timeCompareStr = getDateTime(callSummaryDboList.get(i).getCallTimeStamp());
                    Log.e(TAG, "timeCompareStr: " + timeCompareStr);


                    StringBuffer sb = new StringBuffer();
                    Cursor managedCursor = ((BaseActivity) getActivity()).managedQuery(CallLog.Calls.CONTENT_URI, null, null, null, null);
                    int number = managedCursor.getColumnIndex(CallLog.Calls.NUMBER);
                    int type = managedCursor.getColumnIndex(CallLog.Calls.TYPE);
                    int date = managedCursor.getColumnIndex(CallLog.Calls.DATE);
                    int duration = managedCursor.getColumnIndex(CallLog.Calls.DURATION);

                    while (managedCursor.moveToNext()) {
                        String phNumber = managedCursor.getString(number);

                        String callDate = managedCursor.getString(date);
                        Date callDayTime = new Date(Long.valueOf(callDate));
                        SimpleDateFormat sdf2 = new SimpleDateFormat("HH:mm:ss", Locale.ENGLISH); // the format of your date
                        String originalTimeString = sdf2.format(callDayTime);
                        Log.e(TAG, "originalTimeString: " + originalTimeString);

                        if (managedCursor.getString(number).equalsIgnoreCase(phoneNumberCompare) && originalTimeString.equalsIgnoreCase(timeCompareStr)) {
                            String callType = managedCursor.getString(type);
//                String callDate = managedCursor.getString(date);
//                Date callDayTime = new Date(Long.valueOf(callDate));
                            String callDuration = managedCursor.getString(duration);
                            String dir = null;
                            int dircode = Integer.parseInt(callType);
                            switch (dircode) {
                                case CallLog.Calls.OUTGOING_TYPE:
                                    dir = "OUTGOING";
                                    break;
                                case CallLog.Calls.INCOMING_TYPE:
                                    dir = "INCOMING";
                                    break;
                                case CallLog.Calls.MISSED_TYPE:
                                    dir = "MISSED";
                                    break;
                                case CallLog.Calls.REJECTED_TYPE:
                                    dir = "REJECTED";
                                    break;
                            }
                            sb.append("\nPhone Number:--- " + phNumber + " \nCall Type:--- " + dir + " \nCall Date:--- " + callDayTime + " \nCall duration in sec :--- " + callDuration);
                            sb.append("\n----------------------------------");

                            dbHelperForCallSummary.insertCallSummaryDetails(new CallSummaryDbo(callSummaryDboList.get(i).getCallImage(), callSummaryDboList.get(i).getCallNumber(), dir, callSummaryDboList.get(i).getCallTime(), callSummaryDboList.get(i).getCallDate(), callSummaryDboList.get(i).getCallTimeStamp(), callSummaryDboList.get(i).getCallStrTimeStamp()));
                        }
                    }
                    Log.e(TAG, "Call Log: " + sb);
                }
            }



            count = dbHelperForCallSummary.getCallSummaryCount();
            Log.e(TAG, "Call Summary Count is: " + count);

            if (count > 0) {
                data = dbHelperForCallSummary.getCallSummaryData();

                try {
//                for (int i = 0; i < data.size(); i++) {
//
//                }
                    rv_call_summary.setLayoutManager(new LinearLayoutManager(context));
                    callSummaryAdapter = new CallSummaryAdapter(context, data);
                    rv_call_summary.setAdapter(callSummaryAdapter);
                    dismissProgress();
                } catch (Exception e) {
                    dismissProgress();
                }
            } else {
                dismissProgress();
            }
            dismissProgress();

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return v;
    }

    @Subscribe
    public void onEventMainThread(ApiErrorEvent event) {
        switch (event.getRequestTag()) {
//            case LOGTAG:
//                Log.e(":::","Error"+event.getRequestTag());
//                dismissProgress();
//                break;
            default:
                break;
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Subscribe
    public void onEventMainThread(ApiErrorWithMessageEvent event) {
        switch (event.getRequestTag()) {
//            case LOGTAG:
//                dismissProgress();
//                Log.e(":::",""+event.getResultMsgUser());
//                break;
            default:
                break;
        }
    }

    private String getDate(long time) {
        Date date = new Date(time); // *1000 is to convert seconds to milliseconds
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH); // the format of your date
        return sdf.format(date);
    }

    private String getDateTime(long time) {
        Date date = new Date(time); // *1000 is to convert seconds to milliseconds
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss", Locale.ENGLISH); // the format of your date
        return sdf.format(date);
    }

    private String getTimeStampDate(long time) {
        Date date = new Date(time); // *1000 is to convert seconds to milliseconds
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH); // the format of your date
        return sdf.format(date);
    }

    private class callListAsync extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            showProgress();
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try {

            int countCallSummary = dbHelperForCallSummaryForPhoneTime.getCallSummaryCountDemo();
        Log.e(TAG,"Size of call Summary Demo: "+dbHelperForCallSummaryForPhoneTime.getCallSummaryCountDemo());
//        showProgress();
        if (countCallSummary > 0)
        {
            List<CallSummaryDbo> callSummaryDboList = new ArrayList<CallSummaryDbo>();
            callSummaryDboList = dbHelperForCallSummaryForPhoneTime.getCallSummaryDataDemo();

            for (int i=0; i < callSummaryDboList.size();i++) {

                String phoneNumberCompare = callSummaryDboList.get(i).getCallNumber();
                phoneNumberCompare = phoneNumberCompare.replace(" ", "");

                String timeCompareStr =  getDateTime(callSummaryDboList.get(i).getCallTimeStamp());
                Log.e(TAG,"timeCompareStr: "+timeCompareStr);


                StringBuffer sb = new StringBuffer();
                Cursor managedCursor = ((BaseActivity) getActivity()).managedQuery(CallLog.Calls.CONTENT_URI, null, null, null, null);
                int number = managedCursor.getColumnIndex(CallLog.Calls.NUMBER);
                int type = managedCursor.getColumnIndex(CallLog.Calls.TYPE);
                int date = managedCursor.getColumnIndex(CallLog.Calls.DATE);
                int duration = managedCursor.getColumnIndex(CallLog.Calls.DURATION);

                while (managedCursor.moveToNext()) {
                    String phNumber = managedCursor.getString(number);

                    String callDate = managedCursor.getString(date);
                    Date callDayTime = new Date(Long.valueOf(callDate));
                    SimpleDateFormat sdf2 = new SimpleDateFormat("HH:mm:ss", Locale.ENGLISH); // the format of your date
                    String originalTimeString = sdf2.format(callDayTime);
                    Log.e(TAG, "originalTimeString: " + originalTimeString);

                    if (managedCursor.getString(number).equalsIgnoreCase(phoneNumberCompare) && originalTimeString.equalsIgnoreCase(timeCompareStr)) {
                        String callType = managedCursor.getString(type);
//                String callDate = managedCursor.getString(date);
//                Date callDayTime = new Date(Long.valueOf(callDate));
                        String callDuration = managedCursor.getString(duration);
                        String dir = null;
                        int dircode = Integer.parseInt(callType);
                        switch (dircode) {
                            case CallLog.Calls.OUTGOING_TYPE:
                                dir = "OUTGOING";
                                break;
                            case CallLog.Calls.INCOMING_TYPE:
                                dir = "INCOMING";
                                break;
                            case CallLog.Calls.MISSED_TYPE:
                                dir = "MISSED";
                                break;
                            case CallLog.Calls.REJECTED_TYPE:
                                dir = "REJECTED";
                                break;
                        }
                        sb.append("\nPhone Number:--- " + phNumber + " \nCall Type:--- " + dir + " \nCall Date:--- " + callDayTime + " \nCall duration in sec :--- " + callDuration);
                        sb.append("\n----------------------------------");

                        dbHelperForCallSummary.insertCallSummaryDetails(new CallSummaryDbo(callSummaryDboList.get(i).getCallImage(), callSummaryDboList.get(i).getCallNumber(), dir, callSummaryDboList.get(i).getCallTime(), callSummaryDboList.get(i).getCallDate(), callSummaryDboList.get(i).getCallTimeStamp(), callSummaryDboList.get(i).getCallStrTimeStamp()));
                    }
                }
                Log.e(TAG, "Call Log: " + sb);
            }


        }


            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            dismissProgress();
            super.onPostExecute(aVoid);
        }
    }


}
