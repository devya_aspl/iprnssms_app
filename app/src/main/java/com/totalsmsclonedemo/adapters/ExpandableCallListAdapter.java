package com.totalsmsclonedemo.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.icu.text.UnicodeSetSpanner;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.totalsmsclonedemo.Model.ContactCallModel;
import com.totalsmsclonedemo.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import androidx.recyclerview.selection.SelectionTracker;
import de.hdodenhof.circleimageview.CircleImageView;


public class ExpandableCallListAdapter extends BaseExpandableListAdapter {

    private Context _context;
    private List<String> headerOriginal; // headerOriginal titles
    private List<String> headerRows; // headerOriginal titles
    // Child data in format of headerOriginal title, child title
    private HashMap<String, List<ContactCallModel>> child;

    private SelectionTracker mSelectionTracker;

    public ExpandableCallListAdapter(Context context, List<String> listDataHeader, HashMap<String, List<ContactCallModel>> listChildData) {
        this._context = context;
        this.headerOriginal = listDataHeader;
        this.headerRows = listDataHeader;
        this.child = listChildData;
    }

    @Override
    public int getGroupCount() {
        // Get headerOriginal size
        return this.headerOriginal.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        // return children count
        return this.child.get(this.headerOriginal.get(groupPosition)).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        // Get headerOriginal position
        return this.headerOriginal.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        // This will return the child
        return this.child.get(this.headerOriginal.get(groupPosition)).get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        // TODO Auto-generated method stub
        return false;
    }

    public void setSelectionTracker(SelectionTracker mSelectionTracker) {
        this.mSelectionTracker = mSelectionTracker;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        // Getting headerOriginal title
        String headerTitle = (String) getGroup(groupPosition);

        // Inflating headerOriginal layout and setting text
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_group, parent, false);
        }

        //set content for the parent views
        TextView header_text = (TextView) convertView.findViewById(R.id.header);
        header_text.setText(headerTitle);

        // If group is expanded then change the text into bold and change the
        // icon
        if (isExpanded) {
            header_text.setTypeface(null, Typeface.BOLD);
            header_text.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.arrow_down, 0);
        } else {
            // If group is not expanded then change the text back into normal
            // and change the icon

            header_text.setTypeface(null, Typeface.NORMAL);
            header_text.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.arrow_up, 0);
        }

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        // Getting child text
        final ContactCallModel contactChild = (ContactCallModel) getChild(groupPosition, childPosition);
        // Inflating child layout and setting textview

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.call_list_item, parent, false);
        }



        //set content in the child views
        TextView text_view_country = (TextView) convertView.findViewById(R.id.text_view_country);
        text_view_country.setText(contactChild.getContactCountry());
         TextView tv_number = (TextView) convertView.findViewById(R.id.tv_number);
        tv_number.setText(contactChild.getContactNumber());
         CircleImageView iv_image = (CircleImageView) convertView.findViewById(R.id.iv_image);
        iv_image.setImageResource(contactChild.getImage());

        LinearLayout ll_call_list_item = (LinearLayout)convertView.findViewById(R.id.ll_call_list_item);
        ll_call_list_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Toast.makeText(_context,contactChild.getContactCountry(),Toast.LENGTH_SHORT).show();
            }
        });


        return convertView;
    }

//    public void filterData(String query){
//
//        query = query.toLowerCase();
//        Log.v("Call Adapter", String.valueOf(headerRows.size()));
////        headerRows.clear();
//        headerRows.clear();
//
//
//        if(query.isEmpty()){
//            headerRows.addAll(headerOriginal);
//        }
//        else {
//            for(String headerRow: headerOriginal){
//                ArrayList<ContactCallModel> accomodationChildRows = headerRow.getChildRow();
//                ArrayList<AccomodationChildRow> newList = new ArrayList<AccomodationChildRow>();
//                for(AccomodationChildRow accomodationChildRow: accomodationChildRows){
//                    if(accomodationChildRow.getName().toLowerCase().contains(query)){
//                        newList.add(accomodationChildRow);
//                    }
//                }
//                if(newList.size() > 0){
//                    AccomodationHeaderRow nAccomodationHeaderRows =
//                            new AccomodationHeaderRow(accomodationHeaderRow.getName(),newList);
//                    headerRows.add(nAccomodationHeaderRows);
//                }
//            }
//        }
//        Log.v("Accomodation_Adapter", String.valueOf(headerRows.size()));
//        notifyDataSetChanged();
//    }


    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        // TODO Auto-generated method stub
        return true;
    }
}
