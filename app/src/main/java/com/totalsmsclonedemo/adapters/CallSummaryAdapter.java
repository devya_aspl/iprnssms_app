package com.totalsmsclonedemo.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.totalsmsclonedemo.R;
import com.totalsmsclonedemo.dbo.CallSummaryDbo;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class CallSummaryAdapter extends RecyclerView.Adapter<CallSummaryAdapter.ViewHolder> {


        private Context context;
        private List<CallSummaryDbo> callSummaryData = new ArrayList<>();
//        private List<ProjectListdbo> filteredData = new ArrayList<>();
//        String difference_time;

        public CallSummaryAdapter(Context context, List<CallSummaryDbo> callSummaryData) {
            this.context = context;
            this.callSummaryData = callSummaryData;
        }

        @NonNull
        @Override
        public CallSummaryAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.call_summary_list_item, parent, false);
            return new CallSummaryAdapter.ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(@NonNull CallSummaryAdapter.ViewHolder holder, final int position) {

            if (callSummaryData.get(position).getStatus().equalsIgnoreCase("outgoing"))
            {
                holder.tv_call_status.setTextColor(context.getResources().getColor(R.color.green));
            }
            else
            {
                holder.tv_call_status.setTextColor(context.getResources().getColor(R.color.red));
            }

            holder.tv_number.setText(callSummaryData.get(position).getCallNumber());
            holder.tv_call_summary_date.setText(callSummaryData.get(position).getCallDate());
            holder.tv_call_summary_time.setText(callSummaryData.get(position).getCallTime());
            holder.tv_call_status.setText(callSummaryData.get(position).getStatus());
            Picasso.with(context).load(callSummaryData.get(position).getCallImage())
                    .placeholder(R.drawable.f1).into(holder.iv_call_summary);
//            holder.iv_sms_summary.setText(callSummaryData.get(position).getCallTime());

        }


        @Override
        public int getItemCount() {
            try {
                return callSummaryData.size();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            return 0;
        }


        public class ViewHolder extends RecyclerView.ViewHolder {

            @BindView(R.id.iv_call_summary)
            CircleImageView iv_call_summary;
            @BindView(R.id.tv_number)
            TextView tv_number;
            @BindView(R.id.tv_call_summary_date)
            TextView tv_call_summary_date;
            @BindView(R.id.tv_call_status)
            TextView tv_call_status;
            @BindView(R.id.tv_call_summary_time)
            TextView tv_call_summary_time;
            @BindView(R.id.ll_call_summary_list_item)
            LinearLayout ll_call_summary_list_item;


            public ViewHolder(View v) {
                super(v);
                ButterKnife.bind(this, v);
            }


        }



}
