package com.totalsmsclonedemo.adapters;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.CountDownTimer;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.totalsmsclonedemo.Model.ContactCallFinalList;
import com.totalsmsclonedemo.Model.ContactSmsFinalList;
import com.totalsmsclonedemo.R;
import com.totalsmsclonedemo.Utilities;
import com.totalsmsclonedemo.dbo.CallListDataDbo;
import com.totalsmsclonedemo.dbo.SmsListDataDbo;
import com.totalsmsclonedemo.fragments.CallFragment;
import com.totalsmsclonedemo.fragments.SMSFragment;

import java.lang.reflect.Method;
import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class MyExpandableSMSListAdapter extends BaseExpandableListAdapter {

    private Context context;
    private ArrayList<ContactSmsFinalList> parentRowList;
    private ArrayList<ContactSmsFinalList> originalList;
    public int lastExpandedGroupPosition;
    ExpandableListView expandableListView;
    CountDownTimer timer;
    Add_remove_child add_remove_child;
    Integer selected_pos;

    public MyExpandableSMSListAdapter(Context context, ArrayList<ContactSmsFinalList> originalList, ExpandableListView expandableListView, Fragment fragment) {
        this.context = context;
        this.parentRowList = new ArrayList<>();
        this.parentRowList.addAll(originalList);
        this.originalList = new ArrayList<>();
        this.originalList.addAll(originalList);
        this.expandableListView = expandableListView;
        add_remove_child = (Add_remove_child) fragment;
    }

    @Override
    public int getGroupCount() {
        return parentRowList.size();
    }


    @Override
    public int getChildrenCount(int groupPosition) {
        return parentRowList.get(groupPosition).getChildList().size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return parentRowList.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return parentRowList.get(groupPosition).getChildList().get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(final int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        final ContactSmsFinalList parentRow = (ContactSmsFinalList) getGroup(groupPosition);

        convertView = LayoutInflater.from(context).inflate(R.layout.list_group, parent, false);

        final LinearLayout ll_call_list_item = convertView.findViewById(R.id.ll_group);

        if (parentRow.isSelected_tier()) {
            ll_call_list_item.setBackgroundResource(R.color.selectedItem);
        } else {
            ll_call_list_item.setBackgroundResource(R.color.white);
        }

        convertView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (expandableListView.isGroupExpanded(groupPosition)) {
                    expandableListView.collapseGroup(groupPosition);
                    expandableListView.expandGroup(groupPosition);
                } else {
                    expandableListView.expandGroup(groupPosition);
                }
                int viewColor = ((ColorDrawable) ll_call_list_item.getBackground()).getColor();
                if (viewColor == (context.getResources().getColor(R.color.selectedItem))) {
                    ll_call_list_item.setBackgroundResource(R.color.white);
                    add_remove_child.Add_remove(SMSFragment.current_selected_country, parentRow.getName(), "REMOVE", "");
                    Toast.makeText(context, "All contact of this tier removed from selection list", Toast.LENGTH_SHORT).show();
                } else {
                    ll_call_list_item.setBackgroundResource(R.color.selectedItem);
                    add_remove_child.Add_remove(SMSFragment.current_selected_country, parentRow.getName(), "ADD", "");
                    Toast.makeText(context, "All contact of this tier added to selection list", Toast.LENGTH_SHORT).show();
                }
                return true;
            }
        });

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (expandableListView.isGroupExpanded(groupPosition)) {
                    expandableListView.collapseGroup(groupPosition);
                } else {
                    expandableListView.expandGroup(groupPosition);
                }
            }
        });

        if (SMSFragment.current_selected_tier.equals(parentRow.getName())) {
//            expandableListView.expandGroup(groupPosition);
        }

        TextView header_text = convertView.findViewById(R.id.header);

        header_text.setText(parentRow.getName().trim());

        if (isExpanded) {
            header_text.setTypeface(null, Typeface.BOLD);
            header_text.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.arrow_down, 0);
        } else {
            header_text.setTypeface(null, Typeface.NORMAL);
            header_text.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.arrow_up, 0);
        }

        return convertView;
    }

    @Override
    public View getChildView(final int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        final SmsListDataDbo contactChild = (SmsListDataDbo) getChild(groupPosition, childPosition);

        convertView = LayoutInflater.from(context).inflate(R.layout.sms_list_item, parent, false);

        TextView text_view_country = (TextView) convertView.findViewById(R.id.text_view_country);
        text_view_country.setText(contactChild.getCountry());
        TextView tv_number = (TextView) convertView.findViewById(R.id.tv_number);
        tv_number.setText(contactChild.getPhoneNumber());
        CircleImageView iv_image = (CircleImageView) convertView.findViewById(R.id.iv_image);
        Picasso.with(context).load(contactChild.getImage()).placeholder(R.drawable.f1).into(iv_image);
//        iv_image.setImageResource(contactChild.getImage());

        final LinearLayout ll_call_list_item = convertView.findViewById(R.id.ll_sms_list_item);

        if (contactChild.isSelected()) {
            ll_call_list_item.setBackgroundResource(R.color.selectedItem);
        } else {
            ll_call_list_item.setBackgroundResource(R.color.white);
        }

        ll_call_list_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (SMSFragment.selected_phone_num != null && SMSFragment.selected_phone_num.size() != 0) {
//                    CallFragment.current_selected_tier = contactChild.getTier();
                    int viewColor = ((ColorDrawable) ll_call_list_item.getBackground()).getColor();
                    if (viewColor == (context.getResources().getColor(R.color.selectedItem))) {
                        ll_call_list_item.setBackgroundResource(R.color.white);
                        add_remove_child.Add_remove(SMSFragment.current_selected_country, contactChild.getTier(), "REMOVE", contactChild.getPhoneNumber());
                        Toast.makeText(context, "Contact removed from selection list", Toast.LENGTH_SHORT).show();
                    } else {
                        ll_call_list_item.setBackgroundResource(R.color.selectedItem);
                        add_remove_child.Add_remove(SMSFragment.current_selected_country, contactChild.getTier(), "ADD", contactChild.getPhoneNumber());
                        Toast.makeText(context, "Contact added to selection list", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    String phoneNumber = contactChild.getPhoneNumber();
                    phoneNumber = phoneNumber.replace(" ", "");
                    phoneNumber = phoneNumber.replace("+", "");
                    phoneNumber =  phoneNumber.substring(2);
                    Log.e("SmsAdapter","Phone Number is: "+phoneNumber);
//                    if (!Utilities.getNoSmsSendEachTime(context).equalsIgnoreCase("0")) {
                    Log.e("smsAdapter","no of times sms send: "+ Utilities.getNoSmsSendEachTime(context));
//                    confirmSmsDialog(phoneNumber, contactChild.getPhoneNumber(), contactChild.getImage());
//                    String phoneNumber = contactChild.getPhoneNumber();
//                    phoneNumber = phoneNumber.replace(" ", "");
//                    phoneNumber = phoneNumber.replace("+", "");
//                    phoneNumber = phoneNumber.substring(2);
//                    Log.e("callAdapter", "Phone Number is: " + phoneNumber);
//                    confirmCallDialog(phoneNumber);
                }
            }
        });

        convertView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                int viewColor = ((ColorDrawable) ll_call_list_item.getBackground()).getColor();
                if (viewColor == (context.getResources().getColor(R.color.selectedItem))) {
                    ll_call_list_item.setBackgroundResource(R.color.white);
                    add_remove_child.Add_remove(SMSFragment.current_selected_country, contactChild.getTier(), "REMOVE", contactChild.getPhoneNumber());
                    Toast.makeText(context, "Contact removed from selection list", Toast.LENGTH_SHORT).show();
                } else {
                    ll_call_list_item.setBackgroundResource(R.color.selectedItem);
                    add_remove_child.Add_remove(SMSFragment.current_selected_country, contactChild.getTier(), "ADD", contactChild.getPhoneNumber());
                    Toast.makeText(context, "Contact added to selection list", Toast.LENGTH_SHORT).show();
                }
                return true;
            }
        });

        return convertView;
    }

    private void confirmCallDialog(final String phoneNumber) {
        try {
            final Dialog dialog = new Dialog(context);
            dialog.setCancelable(true);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        dialog.getWindow().setGravity(Gravity.BOTTOM);
            dialog.setContentView(R.layout.dialog_call_confirm);
            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            lp.copyFrom(dialog.getWindow().getAttributes());
            lp.width = WindowManager.LayoutParams.MATCH_PARENT;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

            if (!dialog.isShowing()) {
//            if (!(()).isFinishing()) {
                dialog.show();
//            }
            }
//        dialog.show();
            dialog.getWindow().setAttributes(lp);

            Button btn_yes = dialog.findViewById(R.id.btn_yes);
            btn_yes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    makeCall(phoneNumber);
                    dialog.dismiss();
                }
            });

            Button btn_no = dialog.findViewById(R.id.btn_no);
            btn_no.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void makeCall(String phoneNumber) {
        try {
//        Intent intent = new Intent(Intent.ACTION_DIAL);
//        intent.setData(Uri.parse("tel:"+phoneNumber));
//        context.startActivity(intent);

            //performs call
            if (!phoneNumber.equals("")) {

                if (!Utilities.getDisconnectDurationTime(context).equalsIgnoreCase("")) {
                    long durationTime = Integer.parseInt(Utilities.getDisconnectDurationTime(context)) * 60000;
                    Log.e("adapter", "config duration: " + durationTime);

                    Intent intent2 = new Intent(Intent.ACTION_CALL);
                    intent2.setData(Uri.parse("tel:" + phoneNumber));
                    if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                    context.startActivity(intent2);


                    if (timer != null) {
                        timer.cancel();
                        timer = null;
                    }

                    timer = new CountDownTimer(durationTime, 1000) {
                        @Override
                        public void onTick(long l) {
                            Log.e("adapter", "seconds remaining : " + l / 1000);
                        }

                        @Override
                        public void onFinish() {
                            Log.e("done!", "done!");
                            endCall(context);
                        }
                    }.start();
                } else {
                    Intent intent2 = new Intent(Intent.ACTION_CALL);
                    intent2.setData(Uri.parse("tel:" + phoneNumber));
                    if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                    context.startActivity(intent2);
//                    Toast.makeText(context,"Please Select Disconnect time from Configuration",Toast.LENGTH_SHORT).show();
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void endCall(Context context) {
        TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        try {
            Class c = Class.forName(tm.getClass().getName());
            Method m = c.getDeclaredMethod("getITelephony");
            m.setAccessible(true);
            Object telephonyService = m.invoke(tm);

            c = Class.forName(telephonyService.getClass().getName());
            m = c.getDeclaredMethod("endCall");
            m.setAccessible(true);
            m.invoke(telephonyService);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    public void filterData(String query) {
        query = query.toLowerCase();
        parentRowList.clear();

        if (query.isEmpty()) {
            parentRowList.addAll(originalList);
        } else {
            for (ContactSmsFinalList parentRow : originalList) {
                ArrayList<SmsListDataDbo> childList = parentRow.getChildList();
                ArrayList<SmsListDataDbo> newList = new ArrayList<SmsListDataDbo>();

                for (SmsListDataDbo childRow : childList) {
                    if (childRow.getCountry().toLowerCase().contains(query)) {
                        newList.add(childRow);
                    }
                } // end for (com.example.user.searchviewexpandablelistview.ChildRow childRow: childList)
                if (newList.size() > 0) {
                    ContactSmsFinalList nParentRow = new ContactSmsFinalList(parentRow.getName(), newList, false);
                    parentRowList.add(nParentRow);
                }


            } // end or (com.example.user.searchviewexpandablelistview.ParentRow parentRow : originalList)
        } // end else

        notifyDataSetChanged();
    }

    public void filterList(ArrayList<ContactSmsFinalList> filteredData) {
        parentRowList = filteredData;
        notifyDataSetChanged();
    }

    public void onGroupExpanded(int groupPosition) {
        /*if (groupPosition != lastExpandedGroupPosition) {
            expandableListView.collapseGroup(lastExpandedGroupPosition);
        }

        super.onGroupExpanded(groupPosition);

        lastExpandedGroupPosition = groupPosition;*/

    }

    public interface Add_remove_child {

        void Add_remove(String current_selected_country, String name, String action, String phoneNumber);
    }

}