package com.totalsmsclonedemo.adapters;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.net.Uri;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.totalsmsclonedemo.Model.ContactCallModel;
import com.totalsmsclonedemo.R;
import com.totalsmsclonedemo.Utilities;
import com.totalsmsclonedemo.activities.BaseActivity;
import com.totalsmsclonedemo.databases.DBHelperForSmsSummary;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;


public class ExpandableSMSListAdapter extends BaseExpandableListAdapter {

    private Context _context;
    private List<String> header; // header titles
    // Child data in format of header title, child title
    private HashMap<String, List<ContactCallModel>> child;

    DBHelperForSmsSummary dbHelperForSmsSummary;

    public ExpandableSMSListAdapter(Context context, List<String> listDataHeader, HashMap<String, List<ContactCallModel>> listChildData) {
        dbHelperForSmsSummary = new DBHelperForSmsSummary(context);
        this._context = context;
        this.header = listDataHeader;
        this.child = listChildData;
    }

    @Override
    public int getGroupCount() {
        // Get header size
        return this.header.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        // return children count
        return this.child.get(this.header.get(groupPosition)).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        // Get header position
        return this.header.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        // This will return the child
        return this.child.get(this.header.get(groupPosition)).get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        // Getting header title
        String headerTitle = (String) getGroup(groupPosition);

        // Inflating header layout and setting text
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_group, parent, false);
        }

        //set content for the parent views
        TextView header_text = (TextView) convertView.findViewById(R.id.header);
        header_text.setText(headerTitle);

        // If group is expanded then change the text into bold and change the
        // icon
        if (isExpanded) {
            header_text.setTypeface(null, Typeface.BOLD);
            header_text.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.arrow_down, 0);
        } else {
            // If group is not expanded then change the text back into normal
            // and change the icon

            header_text.setTypeface(null, Typeface.NORMAL);
            header_text.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.arrow_up, 0);
        }

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        // Getting child text
        final ContactCallModel contactChild = (ContactCallModel) getChild(groupPosition, childPosition);
        // Inflating child layout and setting textview
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.sms_list_item, parent, false);
        }

        //set content in the child views
        TextView text_view_country = (TextView) convertView.findViewById(R.id.text_view_country);
        text_view_country.setText(contactChild.getContactCountry());
         TextView tv_number = (TextView) convertView.findViewById(R.id.tv_number);
        tv_number.setText(contactChild.getContactNumber());
         CircleImageView iv_image = (CircleImageView) convertView.findViewById(R.id.iv_image);
        iv_image.setImageResource(contactChild.getImage());

        LinearLayout ll_sms_list_item = (LinearLayout)convertView.findViewById(R.id.ll_sms_list_item);
        ll_sms_list_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    String phoneNumber = contactChild.getContactNumber();
                    phoneNumber = phoneNumber.replace(" ", "");
                    phoneNumber = phoneNumber.replace("+", "");
                    phoneNumber =  phoneNumber.substring(2);
                    Log.e("callAdapter","Phone Number is: "+phoneNumber);
//                    Uri uri = Uri.parse("smsto:"+phoneNumber);
//                    Intent it = new Intent(Intent.ACTION_SENDTO, uri);
//                    it.putExtra("sms_body", "Hello");
//                    _context.startActivity(it);

                    sendSMS(phoneNumber,"Hello",contactChild.getContactNumber(),contactChild.getImage());


                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
//                Toast.makeText(_context,contactChild.getContactCountry(),Toast.LENGTH_SHORT).show();
            }
        });

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        // TODO Auto-generated method stub
        return true;
    }

    //---sends an SMS message to another device---
    private void sendSMS(String phoneNumber, String message, final String actualPhoneNumber, final int image)
    {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
//        Date currentFromApi = simpleDateFormat.parse();
//        Log.e("SMSAdapter", "Api Date: " + currentFromApi);
        Long system_timestamp = System.currentTimeMillis();
        final String system_str_date = getDate(system_timestamp);
        try {
            Date current_system_date = simpleDateFormat.parse(system_str_date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        DateFormat dateFormat = new SimpleDateFormat("hh.mm aa");
        final String timeString = dateFormat.format(new Date()).toString();
        System.out.println("Current time in AM/PM: "+timeString);
        Log.e("SMSAdapter", "Current Timestamp: " + system_timestamp);
        Log.e("SMSAdapter", "Current date: " + system_str_date);
        Log.e("SMSAdapter", "Current time: " + timeString);
//
//        differenceTime = printDifference(currentFromApi, current_system_date);
//
//        Log.e("SMSAdapter", "difference Time " + differenceTime);


        String SENT = "SMS_SENT";
        String DELIVERED = "SMS_DELIVERED";

        PendingIntent sentPI = PendingIntent.getBroadcast(_context, 0,
                new Intent(SENT), 0);

        PendingIntent deliveredPI = PendingIntent.getBroadcast(_context, 0,
                new Intent(DELIVERED), 0);

        //---when the SMS has been sent---
        _context.registerReceiver(new BroadcastReceiver(){
            @Override
            public void onReceive(Context arg0, Intent arg1) {
                switch (getResultCode())
                {
                    case Activity.RESULT_OK:
                        Toast.makeText(_context, "SMS sent",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                        Toast.makeText(_context, "Generic failure",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_NO_SERVICE:
                        Toast.makeText(_context, "No service",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_NULL_PDU:
                        Toast.makeText(_context, "Null PDU",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_RADIO_OFF:
                        Toast.makeText(_context, "Radio off",
                                Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        }, new IntentFilter(SENT));

        //---when the SMS has been delivered---
        _context.registerReceiver(new BroadcastReceiver(){
            @Override
            public void onReceive(Context arg0, Intent arg1) {
                switch (getResultCode())
                {
                    case Activity.RESULT_OK:
                        Toast.makeText(_context, "SMS delivered",
                                Toast.LENGTH_SHORT).show();
                        storeData(image,actualPhoneNumber,"delivered",system_str_date,timeString);

                        break;
                    case Activity.RESULT_CANCELED:
                        Toast.makeText(_context, "SMS not delivered",
                                Toast.LENGTH_SHORT).show();
                        storeData(image,actualPhoneNumber,"Undelivered",system_str_date,timeString);
                        break;
                }
            }
        }, new IntentFilter(DELIVERED));

//        PendingIntent pi = PendingIntent.getActivity(this, 0,
//                new Intent(_context, SMS.class), 0);
        SmsManager sms = SmsManager.getDefault();
        sms.sendTextMessage(phoneNumber, null, message, sentPI, deliveredPI);
    }

    private void storeData(int image, String actualPhoneNumber, String delivered, String system_str_date, String timeString) {
    }

    public String printDifference(Date startDate, Date endDate) {
        //milliseconds
        long different = endDate.getTime() - startDate.getTime();

        System.out.println("startDate : " + startDate);
        System.out.println("endDate : "+ endDate);
        System.out.println("different : " + different);

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        long elapsedSeconds = different / secondsInMilli;

        System.out.printf(
                "%d days, %d hours, %d minutes, %d seconds%n",
                elapsedDays, elapsedHours, elapsedMinutes, elapsedSeconds);

        Log.e("SmsAdapter","%d days " + elapsedDays
                + ", %d hours," + elapsedHours
                + " %d minutes," + elapsedMinutes
                + " %d seconds%n" + elapsedSeconds);

        String difference=null;

        if ( elapsedDays != 0)
        {
            difference = elapsedDays + " days" + " ago";
        }
        else if (elapsedHours != 0)
        {
            difference = elapsedHours + " hours" + " ago";
        }
        else if (elapsedMinutes != 0)
        {
            difference = elapsedMinutes + " minutes" + " ago";
        }  else if (elapsedSeconds != 0 || elapsedSeconds < 0)
        {
            difference = Math.abs(elapsedSeconds) + " seconds" + " ago";
        }
        else if(elapsedSeconds < 1000)
        {
            difference = "0 seconds" + " ago";
        }
        return difference;
    }

    private String getDate(long time) {
        Date date = new Date(time); // *1000 is to convert seconds to milliseconds
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH); // the format of your date
        return sdf.format(date);
    }
}
