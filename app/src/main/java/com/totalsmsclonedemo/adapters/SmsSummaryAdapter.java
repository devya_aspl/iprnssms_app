package com.totalsmsclonedemo.adapters;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.totalsmsclonedemo.PhoneListener;
import com.totalsmsclonedemo.R;
import com.totalsmsclonedemo.activities.BaseActivity;
import com.totalsmsclonedemo.dbo.SMSSummaryDbo;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class SmsSummaryAdapter extends RecyclerView.Adapter<SmsSummaryAdapter.ViewHolder> {


        private Context context;
        private List<SMSSummaryDbo> AdapterData = new ArrayList<>();
//        private List<ProjectListdbo> filteredData = new ArrayList<>();
//        String difference_time;

        public SmsSummaryAdapter(Context context, List<SMSSummaryDbo> AdapterData) {
            this.context = context;
            this.AdapterData = AdapterData;
        }

        @NonNull
        @Override
        public SmsSummaryAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.sms_summary_list_item, parent, false);
            return new SmsSummaryAdapter.ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(@NonNull SmsSummaryAdapter.ViewHolder holder, final int position) {

            if (AdapterData.get(position).getStatus().equalsIgnoreCase("delivered"))
            {
                holder.tv_status.setTextColor(context.getResources().getColor(R.color.green));
            }
            else
            {
                holder.tv_status.setTextColor(context.getResources().getColor(R.color.red));
            }

            holder.tv_number.setText(AdapterData.get(position).getSmsNumber());
            holder.tv_sms_summary_date.setText(AdapterData.get(position).getSmsDate());
            holder.tv_sms_summary_time.setText(AdapterData.get(position).getSmsTime());
            holder.tv_status.setText(AdapterData.get(position).getStatus());
            Picasso.with(context).load(AdapterData.get(position).getSmsImage())
                    .placeholder(R.drawable.f1).into(holder.iv_sms_summary);
//            holder.iv_sms_summary.setText(AdapterData.get(position).getCallTime());

        }


        @Override
        public int getItemCount() {
            try {
                return AdapterData.size();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            return 0;
        }


        public class ViewHolder extends RecyclerView.ViewHolder {

            @BindView(R.id.iv_sms_summary)
            CircleImageView iv_sms_summary;
            @BindView(R.id.tv_number)
            TextView tv_number;
            @BindView(R.id.tv_sms_summary_date)
            TextView tv_sms_summary_date;
            @BindView(R.id.tv_status)
            TextView tv_status;

            @BindView(R.id.tv_sms_summary_time)
            TextView tv_sms_summary_time;



            @BindView(R.id.ll_sms_summary_list_item)
            LinearLayout ll_sms_summary_list_item;


            public ViewHolder(View v) {
                super(v);
                ButterKnife.bind(this, v);
            }


        }



}
