package com.totalsmsclonedemo.databases;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class DBHelperForCallTierNameDetails extends SQLiteOpenHelper {


    // Database Version
    private static final int DATABASE_VERSION = 10678;

    Context context;

    // Database Name
    private static final String DATABASE_NAME = "call_tierName_db";
    private static final String TABLE_NAME = "appCallTierName";

    //    ProjectConfigurationDbo configurationDbo;
    String tierName;

    //    timerId,syncWithMobileData,autoSync,periodicity,chooseLanguage
    public static final String CREATE_TABLE =
            "CREATE TABLE " + TABLE_NAME + "(tierName TEXT UNIQUE);";

    public DBHelperForCallTierNameDetails(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }


    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        Log.e("database", "call tier list table created");
        sqLiteDatabase.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);

        // Create tables again
        onCreate(db);
    }

    //    tierName
    public void insertCallTierName(String tierName) {


        try {
            SQLiteDatabase db = this.getWritableDatabase();
            Log.e("insert call tier:",
                    " tierName is:  " + tierName
            );
            ContentValues values = new ContentValues();
            values.put("tierName", tierName);
            long rowInserted = db.insertWithOnConflict(TABLE_NAME, null, values, SQLiteDatabase.CONFLICT_REPLACE);
//            if(rowInserted != -1)
//                Toast.makeText(context, "Data Inserted Successfully", Toast.LENGTH_SHORT).show();
//            else
//                Toast.makeText(context, "Something Wrong", Toast.LENGTH_SHORT).show();

            db.close();
//            Toast.makeText();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//    public int getConfigurationCount() {
//        String countQuery = "SELECT  * FROM " + TABLE_NAME;
//        SQLiteDatabase db = this.getReadableDatabase();
//        Cursor cursor = db.rawQuery(countQuery, null);
//
//        int count = cursor.getCount();
//        cursor.close();
//
//
//        // return count
//        return count;
//    }

    //
    public ArrayList<String> getAllCallTierName() {
        ArrayList<String> callTierNameList = new ArrayList<>();

        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_NAME;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                String tierName = cursor.getString(cursor.getColumnIndex("tierName"));
                callTierNameList.add(tierName);
            } while (cursor.moveToNext());
        }

        // close db connection
        db.close();

        // return call list
        return callTierNameList;
    }

    public int getCallTierNameCount() {
        String countQuery = "SELECT  * FROM " + TABLE_NAME;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);

        int count = cursor.getCount();
        cursor.close();


        // return count
        return count;
    }

    public int deleteCallTierNames() {
        try {
            SQLiteDatabase db = this.getWritableDatabase();

//            return db.delete(TABLE_NAME, "status" + " = ?", new String[]{"Pending"});

//            return db.delete(TABLE_NAME,
//                    KEY_DATE + "='date' AND " + KEY_GRADE + "='style2' AND " +
//                            KEY_STYLE + "='style' AND " + KEY_PUMPLEVEL + "='pumpLevel'",
//                    null);

            return db.delete(TABLE_NAME,
                    null,
                    null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }
}
