package com.totalsmsclonedemo.databases;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.totalsmsclonedemo.dbo.SMSSummaryDbo;

import java.util.ArrayList;
import java.util.List;

public class DBHelperForSmsSummary extends SQLiteOpenHelper {


    // Database Version
    private static final int DATABASE_VERSION = 10678;

    Context context;

    // Database Name
    private static final String DATABASE_NAME = "tsc_sms_summary_db";
    private static final String TABLE_NAME = "tscSmsSummary";

    SMSSummaryDbo smsSummaryDbo;

//   int smsImage;
//    String smsNumber,status, smsTime, smsDate,smsTimeStamp;
    public static final String CREATE_TABLE =
            "CREATE TABLE " + TABLE_NAME + "(smsImage TEXT,smsNumber TEXT,status TEXT,smsTime TEXT,smsDate DATE,smsTimeStamp REAL UNIQUE,smsStrTimeStamp TEXT);";

    public DBHelperForSmsSummary(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }


    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        Log.e("database","sms summary table created");
        sqLiteDatabase.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);

        // Create tables again
        onCreate(db);
    }

//    int smsImage;
////    String smsNumber,status, smsTime, smsDate;
    public void insertSmsSummaryDetails(SMSSummaryDbo smsSummaryDbo) {


        try {
//      timerId,syncWithMobileData,autoSync,periodicity,chooseLanguage
            SQLiteDatabase db = this.getWritableDatabase();


            Log.e("insert sms summary :",
                    " image is:  "+ smsSummaryDbo.getSmsImage() +
                    " SmsNumber is:  "+ smsSummaryDbo.getSmsNumber() +
                    " SmsDate is:  "+ smsSummaryDbo.getSmsDate() +
                    " Status is:  "+ smsSummaryDbo.getStatus() +
                    " SmsTime is:  "+ smsSummaryDbo.getSmsTime() +
                    " smsTimeStamp is:  "+ smsSummaryDbo.getSmsTimeStamp() +
                    " smsStrTimeStamp is:  "+ smsSummaryDbo.getSmsStrTimeStamp()

            );

//         int smsImage;
//////    String smsNumber,status, smsTime, smsDate;

            ContentValues values = new ContentValues();
//            timeId,call_disconnect_time,no_sms_send_each_time
            values.put("smsImage", smsSummaryDbo.getSmsImage());
            values.put("smsNumber", smsSummaryDbo.getSmsNumber());
            values.put("status", smsSummaryDbo.getStatus());
            values.put("smsTime", smsSummaryDbo.getSmsTime());
            values.put("smsDate", smsSummaryDbo.getSmsDate());
            values.put("smsTimeStamp", smsSummaryDbo.getSmsTimeStamp());
            values.put("smsStrTimeStamp", smsSummaryDbo.getSmsStrTimeStamp());

            long rowInserted =   db.insertWithOnConflict(TABLE_NAME, null, values,SQLiteDatabase.CONFLICT_REPLACE);
//            if(rowInserted != -1)
//                Toast.makeText(context, "Data Inserted Successfully", Toast.LENGTH_SHORT).show();
//            else
//                Toast.makeText(context, "Something Wrong", Toast.LENGTH_SHORT).show();

            db.close();
//            Toast.makeText();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

//    public int getConfigurationCount() {
//        String countQuery = "SELECT  * FROM " + TABLE_NAME;
//        SQLiteDatabase db = this.getReadableDatabase();
//        Cursor cursor = db.rawQuery(countQuery, null);
//
//        int count = cursor.getCount();
//        cursor.close();
//
//
//        // return count
//        return count;
//    }

//
    public List<SMSSummaryDbo> getSmsSummaryData() {
        List<SMSSummaryDbo> smsSummaryDboList = new ArrayList<>();

        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_NAME + " ORDER BY  smsStrTimeStamp DESC";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);


        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                SMSSummaryDbo smsSummaryDbo = new SMSSummaryDbo();
//               int smsImage;
//////////    String smsNumber,status, smsTime, smsDate;
                smsSummaryDbo.setSmsImage(cursor.getString(cursor.getColumnIndex("smsImage")));
                smsSummaryDbo.setSmsNumber(cursor.getString(cursor.getColumnIndex("smsNumber")));
                smsSummaryDbo.setStatus(cursor.getString(cursor.getColumnIndex("status")));
                smsSummaryDbo.setSmsTime(cursor.getString(cursor.getColumnIndex("smsTime")));
                smsSummaryDbo.setSmsDate(cursor.getString(cursor.getColumnIndex("smsDate")));
                smsSummaryDbo.setSmsTimeStamp(cursor.getLong(cursor.getColumnIndex("smsTimeStamp")));
                smsSummaryDbo.setSmsStrTimeStamp(cursor.getString(cursor.getColumnIndex("smsStrTimeStamp")));
                smsSummaryDboList.add(smsSummaryDbo);
            } while (cursor.moveToNext());
        }

        // close db connection
        db.close();

        // return notes list
        return smsSummaryDboList;
    }

    public int getSmsSummaryCount() {
        String countQuery = "SELECT  * FROM " + TABLE_NAME;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);

        int count = cursor.getCount();
        cursor.close();


        // return count
        return count;
    }

//    public int getProjectIdCount(String projectId) {
//
////        String countQuery = "SELECT  * FROM " + TABLE_NAME + "WHERE guiId= " + guiId;
//        SQLiteDatabase db = this.getReadableDatabase();
////        Cursor cursor = db.rawQuery(countQuery, null);
//        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_NAME + " WHERE projectId LIKE ?", new String[] { "%" + projectId + "%" });
//
//        int count = cursor.getCount();
//        cursor.close();
//
//
//        // return count
//        return count;
//    }
}
