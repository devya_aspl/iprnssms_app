package com.totalsmsclonedemo.databases;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.totalsmsclonedemo.dbo.CallSummaryDbo;
import com.totalsmsclonedemo.dbo.SMSSummaryDbo;

import java.util.ArrayList;
import java.util.List;

public class DBHelperForCallSummary extends SQLiteOpenHelper {


    // Database Version
    private static final int DATABASE_VERSION = 10678;

    Context context;

    // Database Name
    private static final String DATABASE_NAME = "tsc_call_summary_db";
    private static final String TABLE_NAME = "tscCallSummary";

    CallSummaryDbo callSummaryDbo;

//    callImage,callNumber, status,callTime, callDate,callTimeStamp,callStrTimeStamp
    public static final String CREATE_TABLE =
            "CREATE TABLE " + TABLE_NAME + "(callImage TEXT,callNumber TEXT,status TEXT,callTime TEXT,callDate DATE,callTimeStamp REAL ,callStrTimeStamp TEXT UNIQUE);";

    public DBHelperForCallSummary(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }


    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        Log.e("database","call summary table created");
        sqLiteDatabase.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);

        // Create tables again
        onCreate(db);
    }

//    callImage,callNumber,status,callTime,callDate,callTimeStamp,callStrTimeStamp
    public void insertCallSummaryDetails(CallSummaryDbo callSummaryDbo) {


        try {
//      callImage,callNumber, status,callTime, callDate,callTimeStamp,callStrTimeStamp
            SQLiteDatabase db = this.getWritableDatabase();


            Log.e("insert call summary :",
                    " image is:  "+ callSummaryDbo.getCallImage() +
                    " CallNumber is:  "+ callSummaryDbo.getCallNumber() +
                    " CallDate is:  "+ callSummaryDbo.getCallDate() +
                    " Status is:  "+ callSummaryDbo.getStatus() +
                    " CallTime is:  "+ callSummaryDbo.getCallTime() +
                    " CallTimeStamp is:  "+ callSummaryDbo.getCallTimeStamp() +
                    " callStrTimeStamp is:  "+ callSummaryDbo.getCallStrTimeStamp()
            );

//            callImage,callNumber, status,callTime, callDate,callTimeStamp,callStrTimeStamp
            ContentValues values = new ContentValues();
//            callImage,callNumber, status,callTime, callDate,callTimeStamp,callStrTimeStamp
            values.put("callImage", callSummaryDbo.getCallImage());
            values.put("callNumber", callSummaryDbo.getCallNumber());
            values.put("status", callSummaryDbo.getStatus());
            values.put("callTime", callSummaryDbo.getCallTime());
            values.put("callDate", callSummaryDbo.getCallDate());
            values.put("callTimeStamp", callSummaryDbo.getCallTimeStamp());
            values.put("callStrTimeStamp", callSummaryDbo.getCallStrTimeStamp());

            long rowInserted =   db.insertWithOnConflict(TABLE_NAME, null, values,SQLiteDatabase.CONFLICT_REPLACE);
//            if(rowInserted != -1)
//                Toast.makeText(context, "Data Inserted Successfully", Toast.LENGTH_SHORT).show();
//            else
//                Toast.makeText(context, "Something Wrong", Toast.LENGTH_SHORT).show();

            db.close();
//            Toast.makeText();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

//    public int getConfigurationCount() {
//        String countQuery = "SELECT  * FROM " + TABLE_NAME;
//        SQLiteDatabase db = this.getReadableDatabase();
//        Cursor cursor = db.rawQuery(countQuery, null);
//
//        int count = cursor.getCount();
//        cursor.close();
//
//
//        // return count
//        return count;
//    }

//
    public List<CallSummaryDbo> getCallSummaryData() {
        List<CallSummaryDbo> callSummaryDboList = new ArrayList<>();

        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_NAME + " ORDER BY callTimeStamp DESC";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);


        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                CallSummaryDbo callSummaryDbo = new CallSummaryDbo();
//                callImage,callNumber, status,callTime, callDate,callTimeStamp,callStrTimeStamp
                callSummaryDbo.setCallImage(cursor.getString(cursor.getColumnIndex("callImage")));
                callSummaryDbo.setCallNumber(cursor.getString(cursor.getColumnIndex("callNumber")));
                callSummaryDbo.setStatus(cursor.getString(cursor.getColumnIndex("status")));
                callSummaryDbo.setCallTime(cursor.getString(cursor.getColumnIndex("callTime")));
                callSummaryDbo.setCallDate(cursor.getString(cursor.getColumnIndex("callDate")));
                callSummaryDbo.setCallTimeStamp(cursor.getLong(cursor.getColumnIndex("callTimeStamp")));
                callSummaryDbo.setCallStrTimeStamp(cursor.getString(cursor.getColumnIndex("callStrTimeStamp")));
                callSummaryDboList.add(callSummaryDbo);
            } while (cursor.moveToNext());
        }

        // close db connection
        db.close();

        // return notes list
        return callSummaryDboList;
    }

    public int getCallSummaryCount() {
        String countQuery = "SELECT  * FROM " + TABLE_NAME;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);

        int count = cursor.getCount();
        cursor.close();


        // return count
        return count;
    }

//    public int getProjectIdCount(String projectId) {
//
////        String countQuery = "SELECT  * FROM " + TABLE_NAME + "WHERE guiId= " + guiId;
//        SQLiteDatabase db = this.getReadableDatabase();
////        Cursor cursor = db.rawQuery(countQuery, null);
//        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_NAME + " WHERE projectId LIKE ?", new String[] { "%" + projectId + "%" });
//
//        int count = cursor.getCount();
//        cursor.close();
//
//
//        // return count
//        return count;
//    }
}
