package com.totalsmsclonedemo.databases;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.telecom.Call;
import android.util.Log;
import android.widget.Toast;

import com.totalsmsclonedemo.dbo.CallAndSmsListDataDbo;
import com.totalsmsclonedemo.dbo.SMSSummaryDbo;

import java.util.ArrayList;
import java.util.List;

public class DBHelperForListForCallAndSms extends SQLiteOpenHelper {


    // Database Version
    private static final int DATABASE_VERSION = 10678;

    Context context;

    // Database Name
    private static final String DATABASE_NAME = "tsc_call_sms_list_db";
    private static final String TABLE_NAME = "tscCallSmsList";

    CallAndSmsListDataDbo callAndSmsListDataDbo;

//    int image;
//    String phoneNumber,country,tier;
        public static final String CREATE_TABLE =
            "CREATE TABLE " + TABLE_NAME + "(image TEXT,phoneNumber TEXT,country TEXT,tier TEXT);";

    public DBHelperForListForCallAndSms(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }


    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        Log.e("database","sms summary table created");
        sqLiteDatabase.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);

        // Create tables again
        onCreate(db);
    }

//    int image;
//    String phoneNumber,country,tier;
    public void insertCallSmsListDetails(CallAndSmsListDataDbo callAndSmsListDataDbo) {


        try {
//      int image;
////    String phoneNumber,country,tier;
            SQLiteDatabase db = this.getWritableDatabase();


            Log.e("insert call and sms :",
                    " image is:  "+ callAndSmsListDataDbo.getImage() +
                    " phoneNumber is:  "+ callAndSmsListDataDbo.getPhoneNumber() +
                    " country is:  "+ callAndSmsListDataDbo.getCountry() +
                    " tier is:  "+ callAndSmsListDataDbo.getTier()

            );

//       int image;
//////    String phoneNumber,country,tier;

            ContentValues values = new ContentValues();
            values.put("image", callAndSmsListDataDbo.getImage());
            values.put("phoneNumber", callAndSmsListDataDbo.getPhoneNumber());
            values.put("country", callAndSmsListDataDbo.getCountry());
            values.put("tier", callAndSmsListDataDbo.getTier());
//            values.put("smsDate", smsSummaryDbo.getCallDate());

            long rowInserted =   db.insertWithOnConflict(TABLE_NAME, null, values, SQLiteDatabase.CONFLICT_REPLACE);
//            if(rowInserted != -1)
//                Toast.makeText(context, "Data Inserted Successfully", Toast.LENGTH_SHORT).show();
//            else
//                Toast.makeText(context, "Something Wrong", Toast.LENGTH_SHORT).show();

            db.close();
//            Toast.makeText();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

//    public int getConfigurationCount() {
//        String countQuery = "SELECT  * FROM " + TABLE_NAME;
//        SQLiteDatabase db = this.getReadableDatabase();
//        Cursor cursor = db.rawQuery(countQuery, null);
//
//        int count = cursor.getCount();
//        cursor.close();
//
//
//        // return count
//        return count;
//    }

//
    public List<CallAndSmsListDataDbo> getCallSmsDataList() {
        List<CallAndSmsListDataDbo> callAndSmsListDataDboList = new ArrayList<>();

        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_NAME;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);


        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                CallAndSmsListDataDbo callAndSmsListDataDbo = new CallAndSmsListDataDbo();
//           int image;
////    String phoneNumber,country,tier;
                callAndSmsListDataDbo.setImage(cursor.getInt(cursor.getColumnIndex("image")));
                callAndSmsListDataDbo.setPhoneNumber(cursor.getString(cursor.getColumnIndex("phoneNumber")));
                callAndSmsListDataDbo.setCountry(cursor.getString(cursor.getColumnIndex("country")));
                callAndSmsListDataDbo.setTier(cursor.getString(cursor.getColumnIndex("tier")));
//                smsSummaryDbo.setCallDate(cursor.getString(cursor.getColumnIndex("smsDate")));
                callAndSmsListDataDboList.add(callAndSmsListDataDbo);
            } while (cursor.moveToNext());
        }

        // close db connection
        db.close();

        // return notes list
        return callAndSmsListDataDboList;
    }

    public ArrayList<CallAndSmsListDataDbo> getCallSmsDataListByTier(String tier) {
        ArrayList<CallAndSmsListDataDbo> callAndSmsListDataDboList = new ArrayList<>();

        // Select All Query
//        String selectQuery = "SELECT  * FROM " + TABLE_NAME;
        String selectQuery = "SELECT  * FROM " + TABLE_NAME + " where tier=?";
//            String selectQuery = "SELECT * FROM  " + TABLE_NAME + " WHERE projectId='" + projectId;


        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, new String[]{tier});

//        SQLiteDatabase db = this.getWritableDatabase();
//        Cursor cursor = db.rawQuery(selectQuery, null);


        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                CallAndSmsListDataDbo callAndSmsListDataDbo = new CallAndSmsListDataDbo();
//           int image;
////    String phoneNumber,country,tier;
                callAndSmsListDataDbo.setImage(cursor.getInt(cursor.getColumnIndex("image")));
                callAndSmsListDataDbo.setPhoneNumber(cursor.getString(cursor.getColumnIndex("phoneNumber")));
                callAndSmsListDataDbo.setCountry(cursor.getString(cursor.getColumnIndex("country")));
                callAndSmsListDataDbo.setTier(cursor.getString(cursor.getColumnIndex("tier")));
//                smsSummaryDbo.setCallDate(cursor.getString(cursor.getColumnIndex("smsDate")));
                callAndSmsListDataDboList.add(callAndSmsListDataDbo);
            } while (cursor.moveToNext());
        }

        // close db connection
        db.close();

        // return notes list
        return callAndSmsListDataDboList;
    }

//    public List<ProjectListFileData> getSpecificProjectDetails(String projectId) {
//        try {
//            List<ProjectListFileData> projects = new ArrayList<>();
//
//            // Select All Query
//            String selectQuery = "SELECT  * FROM " + TABLE_NAME + " where projectId=?";
////            String selectQuery = "SELECT * FROM  " + TABLE_NAME + " WHERE projectId='" + projectId;
//
//
//            SQLiteDatabase db = this.getWritableDatabase();
//            Cursor cursor = db.rawQuery(selectQuery, new String[]{projectId});
//
////        String project_id, String guid_id, String licence_id, String project_title, String created_date, String sync_date, String calculated_time
//
//
//            // looping through all rows and adding to list
//            if (cursor.moveToFirst()) {
//                do {
//
//                    Log.e("select file detail :",
//                            "ProjectD id is:  "+ cursor.getString(cursor.getColumnIndex("projectId"))
//                                    + "ProjectD guid_id is: "+ cursor.getString(cursor.getColumnIndex("projectDetailsId"))
//                                    + "ProjectD details id is: "+ cursor.getString(cursor.getColumnIndex("detailsGuidId"))
//                                    + "ProjectD deviceId is: "+ cursor.getString(cursor.getColumnIndex("deviceId"))
//                                    + "ProjectD fileType is: "+ cursor.getString(cursor.getColumnIndex("fileType"))
//                                    + "ProjectD description is: "+ cursor.getString(cursor.getColumnIndex("description"))
//                                    + "ProjectD filePath is: "+ cursor.getString(cursor.getColumnIndex("filePath"))
//                                    + "ProjectD fileName is: "+ cursor.getString(cursor.getColumnIndex("fileName"))
//                                    + "ProjectD cdate is: "+ cursor.getString(cursor.getColumnIndex("created_date"))
//                                    + "ProjectD difftime is: "+ cursor.getString(cursor.getColumnIndex("differenceTime"))
//                                    + "ProjectD status is: "+ cursor.getString(cursor.getColumnIndex("status"))
//                                    + "ProjectD code is: "+ cursor.getString(cursor.getColumnIndex("code"))
////                                    + "ProjectD chapterGuidId is: "+ cursor.getString(cursor.getColumnIndex("chapterGuidId"))
////                                    + "ProjectD subchapterGuidId is: "+ cursor.getString(cursor.getColumnIndex("subchapterGuidId"))
////                                    + "ProjectD workItemGuidId is: "+ cursor.getString(cursor.getColumnIndex("workitemGuidId"))
//                    );
//
////                 projectId,chapterGuidId,subchapterGuidId,workitemGuidId,projectDetailsId,detailsGuidId,deviceId,fileType,description,filePath,fileName,created_date,differenceTime;
//                    ProjectListFileData projectListFileData = new ProjectListFileData();
////                pid,guiId,lid,ptitle,cdate,sdate,caltime
//                    projectListFileData.setProjectGuidId(cursor.getString(cursor.getColumnIndex("projectId")));
////                    projectListFileData.setChapterGuidId(cursor.getString(cursor.getColumnIndex("chapterGuidId")));
////                    projectListFileData.setSubchapterGuidId(cursor.getString(cursor.getColumnIndex("subchapterGuidId")));
////                    projectListFileData.setWorkitemGuidId(cursor.getString(cursor.getColumnIndex("workitemGuidId")));
//                    projectListFileData.setSelectedDetailsGuidId(cursor.getString(cursor.getColumnIndex("projectDetailsId")));
//                    projectListFileData.setDetailsGuidId(cursor.getString(cursor.getColumnIndex("detailsGuidId")));
//                    projectListFileData.setDeviceId(cursor.getString(cursor.getColumnIndex("deviceId")));
//                    projectListFileData.setFileType(cursor.getString(cursor.getColumnIndex("fileType")));
//                    projectListFileData.setDescription(cursor.getString(cursor.getColumnIndex("description")));
//                    projectListFileData.setFilePath(cursor.getString(cursor.getColumnIndex("filePath")));
//                    projectListFileData.setFileName(cursor.getString(cursor.getColumnIndex("fileName")));
//                    projectListFileData.setCreated_date(cursor.getString(cursor.getColumnIndex("created_date")));
//                    projectListFileData.setDifferenceTime(cursor.getString(cursor.getColumnIndex("differenceTime")));
//                    projectListFileData.setStatus(cursor.getString(cursor.getColumnIndex("status")));
//                    projectListFileData.setCode(cursor.getString(cursor.getColumnIndex("code")));
//
//                    projects.add(projectListFileData);
//                } while (cursor.moveToNext());
//            }
//
//            // close db connection
//            db.close();
//
//            // return notes list
//            return projects;
//        }
//        catch (Exception e)
//        {
//            e.printStackTrace();
//        }
//        return null;
//    }

    public int getCallSmsCount() {
        String countQuery = "SELECT  * FROM " + TABLE_NAME;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);

        int count = cursor.getCount();
        cursor.close();


        // return count
        return count;
    }



//    public int getProjectIdCount(String projectId) {
//
////        String countQuery = "SELECT  * FROM " + TABLE_NAME + "WHERE guiId= " + guiId;
//        SQLiteDatabase db = this.getReadableDatabase();
////        Cursor cursor = db.rawQuery(countQuery, null);
//        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_NAME + " WHERE projectId LIKE ?", new String[] { "%" + projectId + "%" });
//
//        int count = cursor.getCount();
//        cursor.close();
//
//
//        // return count
//        return count;
//    }
}
