package com.totalsmsclonedemo;

import android.content.Context;
import android.content.Intent;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;


public class PhoneListener extends PhoneStateListener {

    private static final String TAG = PhoneListener.class.getSimpleName();


    private Context context;
    Intent callRecordingServiceIntent,contactIntent;
    boolean isServiceRunning = false;
    private static final int REQUEST_RECORD_AUDIO = 0;
    public static int currentStatusOfPhone;
    //UpdateCallLogCallback updateCallLogCallback;

    PhoneListener(Context context){
        this.context = context;
        //updateCallLogCallback = (UpdateCallLogCallback) context;

    }

//    RecordStopInterface recordStopInterface;
    public interface UpdateCallLogCallback {
        void calllogUpdated();
    }
    @Override
    public void onCallStateChanged(int state, String incomingNumber) {
        Log.d(TAG, "PhoneListener::onCallStateChanged state:" + state + " incomingNumber:" + incomingNumber);

//        if (AudioRecorderActivity.recorderActivity != null) {
//            recordStopInterface = (RecordStopInterface) AudioRecorderActivity.recorderActivity;
//        }

//        if (recordStopInterface != null) {
//            recordStopInterface.onStopRecordWhileCallTime();
//        }

        switch (state) {

            case TelephonyManager.CALL_STATE_IDLE:

                Log.d(TAG, "CALL_STATE_IDLE");
                currentStatusOfPhone = 0;
                break;

            case TelephonyManager.CALL_STATE_RINGING:

                Log.d(TAG, "CALL_STATE_RINGING");
                currentStatusOfPhone = 1;
                Log.d(TAG, "setPhoneState is called while call state is ringing");
                break;

            case TelephonyManager.CALL_STATE_OFFHOOK:
                Log.d(TAG, "CALL_STATE_OFFHOOK starting recording");
                currentStatusOfPhone = 2;

//                Log.e(TAG, "Package Status: " + Utilities.getSubStatus(context)+"");
//
//                if(myPreferences.getVelue_fromSharedPreferences(Constant.AUTO_CALL_RECORD_TYPE).equalsIgnoreCase(Constant.ON)) {
//                    Log.e(TAG, "onCallStateChanged: " +"Recrding Started after checking the prefs"
//                    +"value in shared prefs"+ myPreferences.getVelue_fromSharedPreferences(Constant.AUTO_CALL_RECORD_TYPE));
//
//                    if ((!isServiceRunning) && Utilities.getSubStatus(context)) {
//
//                        try {
//
//                            //callRecordingServiceIntent = new Intent(context, CallRecordingService.class);
//                            callRecordingServiceIntent = new Intent(context, CallRecService.class);
//
//                            String strContactName = mConstant.getContactName(incomingNumber, context);
//
//                            if (strContactName.equals("")) {
//
//                                incomingNumber = incomingNumber.replace('+', '0');
//                                Log.e(TAG, "after change + sign: " + incomingNumber);
//                                Utilities.storeValue(context, Utilities.OPPENT_MOBILE_NO, incomingNumber);
//                                callRecordingServiceIntent.putExtra(ProcessingBase.IntentKey.INSTANCE.getPHONE_NUMBER(), incomingNumber);
//
//                            } else {
//                                Utilities.storeValue(context, Utilities.OPPENT_MOBILE_NO, strContactName);
//                                callRecordingServiceIntent.putExtra(ProcessingBase.IntentKey.INSTANCE.getPHONE_NUMBER(), strContactName);
//                            }
//
//                            //callRecordingServiceIntent.putExtra(ProcessingBase.IntentKey.INSTANCE.getPHONE_NUMBER(), "+79202162032");
//                            callRecordingServiceIntent.putExtra(ProcessingBase.IntentKey.INSTANCE.getTYPE_CALL(),
//                                    ProcessingBase.TypeCall.INSTANCE.getINC());
//
//                            context.startService(callRecordingServiceIntent);
//
//                            //createNotification();       //  Create Notification...
//
//                        } catch (Exception e) {
//                            System.out.println("Error At createNotification: " + e);
//                            e.printStackTrace();
//                        }
//                    } else {
//                        Log.d("PhoneListener:", "callRecordingServiceIntent is not running...");
//                    }
//
//                    isServiceRunning = true;
//                }
                break;
        }
    }


//    private void createNotification() {
//        Log.d(TAG, "Notification: Create Notification....");
//
//        NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
//
//        PendingIntent contentIntent = PendingIntent.getActivity(context, 0, new Intent(), 0);
//        Bitmap bitmap= BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_launcher);
//        Notification.Builder mBuilder =
//                new Notification.Builder(context)
//                        .setLargeIcon(bitmap)
//                        .setSmallIcon(R.drawable.ic_launcher)
//                        .setContentTitle("RECall")
//                        .setStyle(new Notification.BigTextStyle().bigText("call recording..."));
//        mBuilder.setOngoing(true);
//        mBuilder.setContentIntent(contentIntent);
//        mNotificationManager.notify(9999, mBuilder.build());
//    }
}