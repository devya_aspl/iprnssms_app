package com.totalsmsclonedemo.dbo;

public class CallListDataDbo {

    String image;
    String phoneNumber, country, tier;
    boolean selected;

    public CallListDataDbo(String image, String phoneNumber, String country, String tier, boolean selected) {
        this.image = image;
        this.phoneNumber = phoneNumber;
        this.country = country;
        this.tier = tier;
        this.selected = selected;
    }

    public CallListDataDbo(String image, String phoneNumber, String country, String tier) {
        this.image = image;
        this.phoneNumber = phoneNumber;
        this.country = country;
        this.tier = tier;
    }

    public CallListDataDbo() {

    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getTier() {
        return tier;
    }

    public void setTier(String tier) {
        this.tier = tier;
    }
}
