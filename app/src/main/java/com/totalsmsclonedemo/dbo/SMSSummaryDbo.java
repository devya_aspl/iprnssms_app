package com.totalsmsclonedemo.dbo;

public class SMSSummaryDbo {

    String smsImage;
    String smsNumber,status, smsTime, smsDate,smsStrTimeStamp;
    long smsTimeStamp;

    public SMSSummaryDbo(String smsImage, String smsNumber, String status, String smsTime, String smsDate) {
        this.smsImage = smsImage;
        this.smsNumber = smsNumber;
        this.status = status;
        this.smsTime = smsTime;
        this.smsDate = smsDate;
    }

    public SMSSummaryDbo(String smsImage, String smsNumber, String status, String smsTime, String smsDate, long smsTimeStamp) {
        this.smsImage = smsImage;
        this.smsNumber = smsNumber;
        this.status = status;
        this.smsTime = smsTime;
        this.smsDate = smsDate;
        this.smsTimeStamp = smsTimeStamp;
    }
    public SMSSummaryDbo(String smsImage, String smsNumber, String status, String smsTime, String smsDate, long smsTimeStamp, String smsStrTimeStamp) {
        this.smsImage = smsImage;
        this.smsNumber = smsNumber;
        this.status = status;
        this.smsTime = smsTime;
        this.smsDate = smsDate;
        this.smsTimeStamp = smsTimeStamp;
        this.smsStrTimeStamp = smsStrTimeStamp;
    }



    public String getSmsStrTimeStamp() {
        return smsStrTimeStamp;
    }

    public void setSmsStrTimeStamp(String smsStrTimeStamp) {
        this.smsStrTimeStamp = smsStrTimeStamp;
    }

    public long getSmsTimeStamp() {
        return smsTimeStamp;
    }

    public void setSmsTimeStamp(long smsTimeStamp) {
        this.smsTimeStamp = smsTimeStamp;
    }

    public SMSSummaryDbo() {

    }

    public String getSmsImage() {
        return smsImage;
    }

    public void setSmsImage(String smsImage) {
        this.smsImage = smsImage;
    }

    public String getSmsNumber() {
        return smsNumber;
    }

    public void setSmsNumber(String smsNumber) {
        this.smsNumber = smsNumber;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSmsTime() {
        return smsTime;
    }

    public void setSmsTime(String smsTime) {
        this.smsTime = smsTime;
    }

    public String getSmsDate() {
        return smsDate;
    }

    public void setSmsDate(String smsDate) {
        this.smsDate = smsDate;
    }
}
