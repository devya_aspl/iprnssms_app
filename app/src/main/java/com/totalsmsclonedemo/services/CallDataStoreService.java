package com.totalsmsclonedemo.services;

import android.app.IntentService;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import com.totalsmsclonedemo.Utilities;
import com.totalsmsclonedemo.databases.DBHelperForCallCountryNameDetails;
import com.totalsmsclonedemo.databases.DBHelperForCallTierNameDetails;
import com.totalsmsclonedemo.databases.DBHelperForListForCall;
import com.totalsmsclonedemo.dbo.CallListDataDbo;
import com.totalsmsclonedemo.dialog.CustomDialog;
import com.totalsmsclonedemo.network.ApIServiceNew;
import com.totalsmsclonedemo.network.response.CallListResponseData;
import com.totalsmsclonedemo.network.response.CallListResponseNew;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class CallDataStoreService extends IntentService {

    public static final String TAG = "callDataIntent";
    DBHelperForListForCall dbHelperForListForCall;
    DBHelperForCallTierNameDetails dbHelperForCallTierNameDetails;
    DBHelperForCallCountryNameDetails dbHelperForCallCountryNameDetails;
    ApIServiceNew newsAPI;
    private CustomDialog customDialog;
    private Handler handler;

    public CallDataStoreService() {
        super("CallDataStoreService");
    }

    @Override
    public void onCreate() {
        super.onCreate();


    }

    @Override
    protected void onHandleIntent(Intent intent) {
//        try {
////            customDialog = new CustomDialog(this);
////            handler = new Handler();
////            showProgress();
//            dbHelperForListForCall = new DBHelperForListForCall(this);
//            dbHelperForCallTierNameDetails = new DBHelperForCallTierNameDetails(this);
//            dbHelperForCallCountryNameDetails = new DBHelperForCallCountryNameDetails(this);
//
//            Retrofit retrofit = new Retrofit.Builder()
//                    .baseUrl("http://tapsglobalsolutions.com/iprnsms/ws/webservice/testnumbers/")
//                    .addConverterFactory(GsonConverterFactory.create())
//                    .build();
//            newsAPI = retrofit.create(ApIServiceNew.class);
//
//            if (Utilities.isNetworkAvailable(getApplicationContext())) {
//                Log.e(TAG, "check internet............");
//                new callListAsync().execute();
//            }
//
////            dismissProgress();
//
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }

    private class callListAsync extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try {

                Call<CallListResponseNew> call_list = newsAPI.get_call_list();

                call_list.enqueue(new Callback<CallListResponseNew>() {
                    @Override
                    public void onResponse(Call<CallListResponseNew> call, Response<CallListResponseNew> response) {
                        try {
                            if (response.isSuccessful()) {
                                Log.e(TAG, "response: " + response.body().getMsg());

                                List<CallListResponseData> data = new ArrayList<>();
                                data = response.body().getData();

                                Log.e(TAG, "list data size: " + data.size());
                                dbHelperForListForCall.deleteCallList();
                                dbHelperForCallTierNameDetails.deleteCallTierNames();
                                dbHelperForCallCountryNameDetails.deleteCallCountryNames();
                                for (int i = 0; i < data.size(); i++) {
                                    Log.e(TAG, "list data size: " + data.size());
//            int image, String phoneNumber, String country, String tier
                                    Log.e(TAG, "call list database size: " + dbHelperForListForCall.getCallCount());
                                    String countryImage = response.body().getPath() + data.get(i).getCountryImage();
                                    Log.e(TAG, "CountryImage: " + countryImage);
                                    dbHelperForListForCall.insertCallListDetails(new CallListDataDbo(countryImage, data.get(i).getNumber(), data.get(i).getCountry(), data.get(i).getTier(), false));
                                    dbHelperForCallCountryNameDetails.insertCallCountryName(data.get(i).getCountry());
                                    dbHelperForCallTierNameDetails.insertCallTierName(data.get(i).getTier());
                                    Log.e(TAG, "Size of Country names list: " + dbHelperForCallCountryNameDetails.getCallCountryNameCount());
                                    Log.e(TAG, "Size of tier names list: " + dbHelperForCallTierNameDetails.getCallTierNameCount());
                                    Log.e(TAG, "call list database size: " + dbHelperForListForCall.getCallCount());
                                    Log.e(TAG, "Size of " + data.get(0).getTier() + " is " + dbHelperForListForCall.getCallTierCount(data.get(0).getTier()));
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Call<CallListResponseNew> call, Throwable t) {
                        Log.e(TAG, "response: " + "failure");
                    }
                });


            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }
    }


    protected void dismissProgress() {
        if (handler != null && customDialog != null) {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    customDialog.hide();
                }
            });
        }
    }

    protected void showProgress() {
        if (handler != null && customDialog != null) {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    if (!customDialog.isDialogShowing()) {
                        customDialog.show();
                    }
                }// hideKeyboard(edt);}});}}
            });
        }
    }

    @Override
    public int onStartCommand(@Nullable Intent intent, int flags, int startId) {
        Log.e(TAG, "call service starting");
//        Toast.makeText(this, "call service starting", Toast.LENGTH_SHORT).show();
        try {
//            customDialog = new CustomDialog(this);
//            handler = new Handler();
//            showProgress();
            dbHelperForListForCall = new DBHelperForListForCall(this);
            dbHelperForCallTierNameDetails = new DBHelperForCallTierNameDetails(this);
            dbHelperForCallCountryNameDetails = new DBHelperForCallCountryNameDetails(this);

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl("http://tapsglobalsolutions.com/iprnsms/ws/webservice/testnumbers/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            newsAPI = retrofit.create(ApIServiceNew.class);

            if (Utilities.isNetworkAvailable(getApplicationContext())) {
                Log.e(TAG, "check internet............");
                new callListAsync().execute();
            }

//            dismissProgress();


        } catch (Exception e) {
            e.printStackTrace();
        }
        return super.onStartCommand(intent, flags, startId);
    }
}
