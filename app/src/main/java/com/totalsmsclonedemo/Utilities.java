package com.totalsmsclonedemo;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;


//import vc908.stickerfactory.StickersManager;

public class Utilities extends Application
{

    private static final String TAG = Utilities.class.getSimpleName();

    public static String DISCONNECT_DURATION_TIME = "disconnect_duration_time";
    public static String NO_SMS_SEND_EACH_TIME = "no_sms_send_each_time";
//    public static String PROJECT_DETAILS_ID = "p_details_id";
//    public static String isTermsAndConditions = "is_t&c";
//    public static String LICENCE_CODE = "licence_code";
//    public static String GOT_API_CURRENT_DATE = "current_api_date";
//    public static String STORE_CAPTURE_IMAGE_PATH = "store_capture_image_path";
//    public static String GUI_ID_INSERT_DATA = "gui_id_insert_data";
//    public static String DETAILS_GUI_ID_INSERT_DATA = "details_gui_id_insert_data";


    public int getAppVersion()
    {
        try
        {
            PackageInfo packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e)
        {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    public static String getDeviceId(Context context)
    {
        String androidId = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        return androidId;
    }




    public static void showAlertMessage(Context context, int resId)
    {
        Toast.makeText(context, resId, Toast.LENGTH_SHORT).show();
    }

    public static void showAlertMessage(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }


    public static void storeValue(Context context, String key, Object value) {

        SharedPreferences preferences = getAppPreferences(context);
        Editor editor = preferences.edit();

        System.out.println("store_key = " + key);
        System.out.println("store_value = " + value);

        if (value instanceof String)
        {
            editor.putString(key, (String) value);
        }
        else if (value instanceof Integer)
        {
            editor.putInt(key, (Integer) value);
        }
        else if (value instanceof Float)
        {
            editor.putFloat(key, (Float) value);
        }
        else if (value instanceof Boolean)
        {
            editor.putBoolean(key, (Boolean) value);
        }

        editor.commit();
    }

//    public static String getOppentMobileNo(Context context) {
//        SharedPreferences preferences = Utilities.getAppPreferences(context);
//        return preferences.getString(OPPENT_MOBILE_NO, "");
//    }



    @SuppressLint("SdCardPath")
    public static void checkDB(Context context)
    {
        try
        {

            // android default database location is :
            // /data/data/com.example.appname/databases/
            String packageName = context.getPackageName();
            System.out.println("packageName = " + packageName);
            String destPath = "/data/data/" + packageName + "/databases";
//            String fullPath = "/data/data/" + packageName + "/databases/" + Database.DATABASE_NAME;

            // this database folder location
            File f = new File(destPath);

            // this database file location
//            File obj = new File(fullPath);

            // check if databases folder exists or not. if not create it
            if (!f.exists())
            {
                f.mkdirs();
                f.createNewFile();
            }

            // check database file exists or not, if not copy database from
            // assets
//            if (!obj.exists())
//            {
//                CopyDB(fullPath, context);
//            }

        } catch (FileNotFoundException e)
        {
            Log.i("fish", "befor sys log FileNotFoundException");
            e.printStackTrace();

        } catch (IOException e)
        {
            Log.i("fish", "befor sys log IOException");
            e.printStackTrace();

        }

    }

    public static void CopyDB(String path, Context context) throws IOException
    {

        InputStream databaseInput = null;
        String outFileName = path;
        OutputStream databaseOutput = new FileOutputStream(outFileName);

        byte[] buffer = new byte[1024];
        int length;

        // open database file from asset folder
//        databaseInput = context.getAssets().open(Database.DATABASE_NAME);
        while ((length = databaseInput.read(buffer)) > 0)
        {
            databaseOutput.write(buffer, 0, length);
            databaseOutput.flush();
        }
        databaseInput.close();

        databaseOutput.flush();
        databaseOutput.close();

    }


    @SuppressLint("SimpleDateFormat")
    public static String ConvertMilliToDate(Long currentDateTime)
    {
        //creating Date from millisecond
        Date currentDate = new Date(currentDateTime);

        //printing value of Date
        System.out.println("current Date: " + currentDate);
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm aa");
        //formatted value of current Date
        System.out.println("Milliseconds to Date: " + df.format(currentDate));

        //Converting milliseconds to Date using Calendar
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(currentDateTime);
        System.out.println("Milliseconds to Date using Calendar:"
                + df.format(cal.getTime()));

        String formattedDate = df.format(cal.getTime());

        return formattedDate;

    }

    @SuppressLint("SimpleDateFormat")
    public static Long ConvertDateToMilli(String currentDateTime)
    {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm aa");
        Long date_time = null;
        try
        {
            Date date = df.parse(currentDateTime);
            System.out.println(currentDateTime);
            System.out.println("Date - Time in milliseconds : " + date.getTime());
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            date_time = calendar.getTimeInMillis();
            System.out.println("Calender - Time in milliseconds : " + calendar.getTimeInMillis());
        } catch (ParseException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return date_time;
    }

    public static void hideSoftKeyboard(Activity activity)
    {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }


    public static String toCommaSeparatedString(ArrayList<String> strings)
    {
        String result = "";
        if (strings != null && !strings.isEmpty())
        {
            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 0; i < strings.size(); i++)
            {
                String string = strings.get(i);
                if (string != null)
                {
                    stringBuilder.append(string);
                    stringBuilder.append(",");
                }
            }
            stringBuilder.deleteCharAt(stringBuilder.length() - 1);
            result = stringBuilder.toString();
            //System.out.println("result = "+result);
        }
        return result;
    }

    public static String toCommaSeparatedString(String[] strings)
    {
        String result = "";
        if (strings != null && strings.length > 0)
        {

            for (int i = 0; i < strings.length; i++)
            {
                result = result.concat(strings[i]);
                if(i!=(strings.length-1))
                    result = result.concat(",");
            }

        }
        return result;
    }

    public static SharedPreferences getAppPreferences(Context context) {
        return context.getSharedPreferences("Presto", Context.MODE_PRIVATE);
    }


    public static void clearPreferences(Context mContext)
    {
        SharedPreferences preferences = getAppPreferences(mContext);
        Editor editor = preferences.edit();
        editor.clear();
        editor.commit();

    }
    public static boolean isNetworkAvailable(Context context)
    {
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        if (connectivity != null)
        {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();

            if (info != null)
            {
                for (int i = 0; i < info.length; i++)
                {
                    Log.i("Class", info[i].getState().toString());
                    if (info[i].getState() == NetworkInfo.State.CONNECTED)
                    {
                        return true;
                    }
                }
            }
        }
        return false;
    }


    private static final long K = 1024;
    private static final long M = K * K;
    private static final long G = M * K;
    private static final long T = G * K;

    public static String convertToStringRepresentation(final long value){
        final long[] dividers = new long[] { T, G, M, K, 1 };
        final String[] units = new String[] { "TB", "GB", "MB", "KB", "B" };
        if(value < 1)
            throw new IllegalArgumentException("Invalid file size: " + value);
        String result = null;
        for(int i = 0; i < dividers.length; i++){
            final long divider = dividers[i];
            if(value >= divider){
                result = format(value, divider, units[i]);
                break;
            }
        }
        return result;
    }

    private static String format(final long value,
                                 final long divider,
                                 final String unit){
        final double result =
                divider > 1 ? (double) value / (double) divider : (double) value;
        return new DecimalFormat("#,##0.#").format(result) + " " + unit;
    }

    public static void showSnackBarToast(View view, String strMessage) {
        try {
            Snackbar.make(view, strMessage, Snackbar.LENGTH_LONG).show();
        } catch (Exception e) {

        }
    }


    public String milliSecondsToTimer(long milliseconds) {
        String finalTimerString = "";
        String secondsString = "", minuteString = "";

        // Convert total duration into time
        int hours = (int) (milliseconds / (1000 * 60 * 60));
        int minutes = (int) (milliseconds % (1000 * 60 * 60)) / (1000 * 60);
        int seconds = (int) ((milliseconds % (1000 * 60 * 60)) % (1000 * 60) / 1000);
        // Add hours if there
        if (hours > 0) {
            finalTimerString = hours + ":";
        }

        // Prepending 0 to seconds if it is one digit
        if (seconds < 10) {
            secondsString = "0" + seconds;
        } else {
            secondsString = "" + seconds;
        }

        // Prepending 0 to minutes if it is one digit
        if (minutes < 10) {
            minuteString = "0" + minutes;
        } else {
            minuteString = "" + minutes;
        }

        finalTimerString = finalTimerString + minuteString + ":" + secondsString;

        // return timer string
        return finalTimerString;
    }

    public static String getDisconnectDurationTime(Context context){
        SharedPreferences preferences = Utilities.getAppPreferences(context);
        return preferences.getString(DISCONNECT_DURATION_TIME, "");
    }
//
    public static String getNoSmsSendEachTime(Context context){
        SharedPreferences preferences = Utilities.getAppPreferences(context);
        return preferences.getString(NO_SMS_SEND_EACH_TIME, "");
    }
//
//    public static String getGotCurrentDate(Context context){
//        SharedPreferences preferences = Utilities.getAppPreferences(context);
//        return preferences.getString(GOT_API_CURRENT_DATE, null);
//    }
//
//    public static String getStoreCaptureImagePath(Context context){
//        SharedPreferences preferences = Utilities.getAppPreferences(context);
//        return preferences.getString(STORE_CAPTURE_IMAGE_PATH, null);
//    }
//
// public static String getGuiIdInsertData(Context context){
//        SharedPreferences preferences = Utilities.getAppPreferences(context);
//        return preferences.getString(GUI_ID_INSERT_DATA, null);
//    }
//
//    public static String getProjectDetailsId(Context context){
//        SharedPreferences preferences = Utilities.getAppPreferences(context);
//        return preferences.getString(PROJECT_DETAILS_ID, null);
//    }
//
//    public static String getDetailsGuiIdInsertData(Context context){
//        SharedPreferences preferences = Utilities.getAppPreferences(context);
//        return preferences.getString(DETAILS_GUI_ID_INSERT_DATA, null);
//    }
//
//    public static Boolean getIsTermsAndConditions(Context context)
//    {
//        SharedPreferences preferences = Utilities.getAppPreferences(context);
//        return preferences.getBoolean(isTermsAndConditions, false);
//    }
//
//    public static boolean checkForCamera(Context context)
//    {
//        return context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA);
//    }


}
