package com.totalsmsclonedemo.network.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CallListResponseData extends AbstractApiResponse
{
    @SerializedName("tier")
    @Expose
    private String tier;
    @SerializedName("number")
    @Expose
    private String number;
    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("country_image")
    @Expose
    private String countryImage;

    public String getTier() {
        return tier;
    }

    public void setTier(String tier) {
        this.tier = tier;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCountryImage() {
        return countryImage;
    }

    public void setCountryImage(String countryImage) {
        this.countryImage = countryImage;
    }
}
