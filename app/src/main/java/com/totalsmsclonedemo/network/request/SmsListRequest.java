package com.totalsmsclonedemo.network.request;

import android.content.Context;

import com.totalsmsclonedemo.Helper;
import com.totalsmsclonedemo.R;
import com.totalsmsclonedemo.TotalSMSCloneApp;
import com.totalsmsclonedemo.network.APIService;
import com.totalsmsclonedemo.network.response.AbstractApiCallback;
import com.totalsmsclonedemo.network.response.SmsListResponse;



public class SmsListRequest extends AbstractApiRequest {
    private static final String LOGTAG = SmsListRequest.class.getSimpleName();

    private static Context mContext;


    private AbstractApiCallback<SmsListResponse> smsListCallback;




    /**
     * Initialize the request with the passed values.
     *
     * @param APIService The {@link APIService} used for executing the calls.
     * @param tag        Identifies the request.
     */

    public SmsListRequest(APIService APIService, String tag) {
        super(APIService, tag);
        mContext= TotalSMSCloneApp.getAppContext();
    }


    public void execute() {
        smsListCallback = new AbstractApiCallback(tag);

        if (!isInternetActive()) {
            smsListCallback.postUnexpectedError(mContext.getString(R.string.error_no_internet));
            return;
        }


        apiService.get_sms_list().enqueue(smsListCallback);

    }

    @Override
    public void cancel() {
        smsListCallback.invalidate();
    }

    @Override
    public boolean isInternetActive() {
        return Helper.isInternetActive(mContext);
    }
}
