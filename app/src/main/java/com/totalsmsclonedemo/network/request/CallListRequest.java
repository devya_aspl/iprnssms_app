package com.totalsmsclonedemo.network.request;

import android.content.Context;

import com.totalsmsclonedemo.Helper;
import com.totalsmsclonedemo.R;
import com.totalsmsclonedemo.TotalSMSCloneApp;
import com.totalsmsclonedemo.network.APIService;
import com.totalsmsclonedemo.network.response.AbstractApiCallback;
import com.totalsmsclonedemo.network.response.CallListResponse;


/**
 * Created by android2 on 3/3/17.
 */

public class CallListRequest extends AbstractApiRequest {
    private static final String LOGTAG = CallListRequest.class.getSimpleName();

    private static Context mContext;
    private String licence_key, licence_code,uuid,email,api_from;


    private AbstractApiCallback<CallListResponse> callListCallback;




    /**
     * Initialize the request with the passed values.
     *
     * @param APIService The {@link APIService} used for executing the calls.
     * @param tag        Identifies the request.
     */

    public CallListRequest(APIService APIService, String tag) {
        super(APIService, tag);
        mContext= TotalSMSCloneApp.getAppContext();
    }


    public void execute() {
        callListCallback = new AbstractApiCallback(tag);

        if (!isInternetActive()) {
            callListCallback.postUnexpectedError(mContext.getString(R.string.error_no_internet));
            return;
        }


        apiService.get_call_list().enqueue(callListCallback);

    }

    @Override
    public void cancel() {
        callListCallback.invalidate();
    }

    @Override
    public boolean isInternetActive() {
        return Helper.isInternetActive(mContext);
    }
}
