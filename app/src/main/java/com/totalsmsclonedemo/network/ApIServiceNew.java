package com.totalsmsclonedemo.network;

import com.totalsmsclonedemo.network.response.CallListResponse;
import com.totalsmsclonedemo.network.response.CallListResponseNew;
import com.totalsmsclonedemo.network.response.SmsListResponse;
import com.totalsmsclonedemo.network.response.SmsListResponseNew;

import retrofit2.Call;
import retrofit2.http.POST;

public interface ApIServiceNew {
    @POST("CALL")
    Call<CallListResponseNew> get_call_list();

    @POST("SMS")
    Call<SmsListResponseNew> get_sms_list();

}
